﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using ChessboardGamesCore;
using ChessboardGamesCore.IO.FEN;
using ChessBox.ChessGameCoreExtensions;

namespace ChessBox
{
    public partial class FENGeneratorForm : Form
    {
        public FENGeneratorForm(string FENString)
        {
            InitializeComponent();

            try
            {
                this.chessboard = new Chessboard();
                FENData fenData = new FENParser().GetExistingFENDataFromString(FENString);

                if (fenData.ActiveColor == PieceColor.White)
                    this.whiteColor_radioButton.Checked = true;
                else
                    this.blackColor_radioButton.Checked = true;

                this.chessboard.LoadStateByFENPiecePlacement(fenData.PiecePlacement);
            }
            catch
            {
                this.whiteColor_radioButton.Checked = true;
            }
        }

        Chessboard chessboard;
        ChessboardCoordinate activeCoordinate;
        Piece selectedPiece = null;

        public string FENStringResult { get; protected set; }

        private void FENGeneratorForm_Load(object sender, EventArgs e)
        {
            RedrawChessboard();
        }

        private void chessboard_pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            int mouseX = e.X - 15;
            int mouseY = e.Y - 15;
            Rectangle chessboardRectangle = new Rectangle(0, 0, chessboard_pictureBox.Width - 15, chessboard_pictureBox.Height - 15);
            if (chessboardRectangle.Contains(new Point(mouseX, mouseY)))
            {
                int file = (mouseX - mouseX % 55) / 55;
                int rank = 7 - (mouseY - mouseY % 55) / 55;
                ChessboardCoordinate newCoordinate = new ChessboardCoordinate(file > 7 ? 7 : file, rank < 0 ? 0 : rank);
                if (newCoordinate != activeCoordinate && file <= 7 && rank >= 0)
                {
                    activeCoordinate = newCoordinate;
                    RedrawChessboard();
                }
            }
        }

        private void chessboard_pictureBox_MouseLeave(object sender, EventArgs e)
        {
            activeCoordinate = null;
            RedrawChessboard();
        }

        protected void RedrawChessboard()
        {
            if (activeCoordinate == null)
                this.chessboard_pictureBox.Image = this.chessboard.GetImage();
            else
                this.chessboard_pictureBox.Image = this.chessboard.GetImage(new List<ChessboardCoordinate>() { activeCoordinate });
        }

        private void chessboard_pictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            if (this.activeCoordinate != null && e.Button == MouseButtons.Left)
            {
                if (this.chessboard[this.activeCoordinate] != null)
                    this.chessboard.RemovePiece(this.activeCoordinate);
                if (this.selectedPiece != null)
                    this.chessboard.SetPiece(this.selectedPiece, this.activeCoordinate);
                RedrawChessboard();;
            }
        }

        private void clearChessboard_button_Click(object sender, EventArgs e)
        {
            for (int file = 0; file < 8; file++)
                for (int rank = 0; rank < 8; rank++)
                    if (this.chessboard[file, rank] != null)
                        this.chessboard.RemovePiece(new ChessboardCoordinate(file, rank));

            RedrawChessboard();
        }

        private void cancel_button_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ok_button_Click(object sender, EventArgs e)
        {
            FENData fenData = new FENData();
            fenData.PiecePlacement = chessboard.GetFENPiecePlacement();
            fenData.ActiveColor = whiteColor_radioButton.Checked ? PieceColor.White : PieceColor.Black;

            this.FENStringResult = fenData.ToString();
            this.DialogResult = DialogResult.OK;
        }

        protected void ProcessPieceSelection(object sender, Piece piece)
        {
            this.selectedPiece = piece;
            this.selectPiece_pictureBox.Top = ((PictureBox)sender).Top - 2;
            this.selectPiece_pictureBox.Left = ((PictureBox)sender).Left - 2;
        }

        private void whitePawn_pictureBox_Click(object sender, EventArgs e)
        {
            ProcessPieceSelection(sender, new Piece(PieceType.Pawn, PieceColor.White));
        }

        private void whiteRook_pictureBox_Click(object sender, EventArgs e)
        {
            ProcessPieceSelection(sender, new Piece(PieceType.Rook, PieceColor.White));
        }

        private void whiteKnight_pictureBox_Click(object sender, EventArgs e)
        {
            ProcessPieceSelection(sender, new Piece(PieceType.Knight, PieceColor.White));
        }

        private void blackPawn_pictureBox_Click(object sender, EventArgs e)
        {
            ProcessPieceSelection(sender, new Piece(PieceType.Pawn, PieceColor.Black));
        }

        private void blackRook_pictureBox_Click(object sender, EventArgs e)
        {
            ProcessPieceSelection(sender, new Piece(PieceType.Rook, PieceColor.Black));
        }

        private void blackKnight_pictureBox_Click(object sender, EventArgs e)
        {
            ProcessPieceSelection(sender, new Piece(PieceType.Knight, PieceColor.Black));
        }

        private void whiteBishop_pictureBox_Click(object sender, EventArgs e)
        {
            ProcessPieceSelection(sender, new Piece(PieceType.Bishop, PieceColor.White));
        }

        private void whiteQueen_pictureBox_Click(object sender, EventArgs e)
        {
            ProcessPieceSelection(sender, new Piece(PieceType.Queen, PieceColor.White));
        }

        private void whiteKing_pictureBox_Click(object sender, EventArgs e)
        {
            ProcessPieceSelection(sender, new Piece(PieceType.King, PieceColor.White));
        }

        private void blackBishop_pictureBox_Click(object sender, EventArgs e)
        {
            ProcessPieceSelection(sender, new Piece(PieceType.Bishop, PieceColor.Black));
        }

        private void blackQueen_pictureBox_Click(object sender, EventArgs e)
        {
            ProcessPieceSelection(sender, new Piece(PieceType.Queen, PieceColor.Black));
        }

        private void blackKing_pictureBox_Click(object sender, EventArgs e)
        {
            ProcessPieceSelection(sender, new Piece(PieceType.King, PieceColor.Black));
        }

        private void clearCell_pictureBox_Click(object sender, EventArgs e)
        {
            ProcessPieceSelection(sender, null);
        }
    }
}
