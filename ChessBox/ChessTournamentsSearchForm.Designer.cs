﻿namespace ChessBox
{
    partial class ChessTournamentsSearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.statusLabel_toolStrip = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ImportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.exportFiltered_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportSelected_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exit_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.event_toolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.clearEvent_toolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel4 = new System.Windows.Forms.ToolStripLabel();
            this.site_toolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.clearSite_toolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.whitePlayer_toolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.clearWhitePlayer_toolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.blackPlayer_toolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.clearBlackPlayer_toolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.groupBox_seachFilter = new System.Windows.Forms.GroupBox();
            this.piecePlacementFilter_checkBox = new System.Windows.Forms.CheckBox();
            this.fenGenerator_button = new System.Windows.Forms.Button();
            this.fenChessboardState_textBox = new System.Windows.Forms.TextBox();
            this.fen_label = new System.Windows.Forms.Label();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.chessGames_dataGridView = new System.Windows.Forms.DataGridView();
            this.idColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eventColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.siteColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.roundColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.whiteColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.blackColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.resultColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.movetextColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chessGamesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.chessTournamentsDataSet = new ChessBox.Database.ChessTournamentsDatabaseDataSet();
            this.tagPairs_groupBox = new System.Windows.Forms.GroupBox();
            this.showSelectedChessGame_button = new System.Windows.Forms.Button();
            this.roundValue_label = new System.Windows.Forms.Label();
            this.round_label = new System.Windows.Forms.Label();
            this.eventValue_label = new System.Windows.Forms.Label();
            this.resultValue_label = new System.Windows.Forms.Label();
            this.blackValue_label = new System.Windows.Forms.Label();
            this.whiteValue_label = new System.Windows.Forms.Label();
            this.dateValue_label = new System.Windows.Forms.Label();
            this.siteValue_label = new System.Windows.Forms.Label();
            this.result_label = new System.Windows.Forms.Label();
            this.black_label = new System.Windows.Forms.Label();
            this.white_label = new System.Windows.Forms.Label();
            this.date_label = new System.Windows.Forms.Label();
            this.site_label = new System.Windows.Forms.Label();
            this.event_label = new System.Windows.Forms.Label();
            this.chessGamesTableAdapter = new ChessBox.Database.ChessTournamentsDatabaseDataSetTableAdapters.ChessGamesTableAdapter();
            this.statusStrip.SuspendLayout();
            this.menuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.toolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.groupBox_seachFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chessGames_dataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chessGamesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chessTournamentsDataSet)).BeginInit();
            this.tagPairs_groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel_toolStrip});
            this.statusStrip.Location = new System.Drawing.Point(0, 590);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(1008, 22);
            this.statusStrip.TabIndex = 0;
            this.statusStrip.Text = "statusStrip";
            // 
            // statusLabel_toolStrip
            // 
            this.statusLabel_toolStrip.Name = "statusLabel_toolStrip";
            this.statusLabel_toolStrip.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.statusLabel_toolStrip.Size = new System.Drawing.Size(77, 17);
            this.statusLabel_toolStrip.Text = "StatusLabel";
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1008, 24);
            this.menuStrip.TabIndex = 1;
            this.menuStrip.Text = "menuStrip";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ImportToolStripMenuItem,
            this.toolStripSeparator6,
            this.exportFiltered_ToolStripMenuItem,
            this.exportSelected_ToolStripMenuItem,
            this.toolStripSeparator1,
            this.exit_ToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // ImportToolStripMenuItem
            // 
            this.ImportToolStripMenuItem.Name = "ImportToolStripMenuItem";
            this.ImportToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.ImportToolStripMenuItem.Text = "Импорт...";
            this.ImportToolStripMenuItem.Click += new System.EventHandler(this.ImportToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(201, 6);
            // 
            // exportFiltered_ToolStripMenuItem
            // 
            this.exportFiltered_ToolStripMenuItem.Name = "exportFiltered_ToolStripMenuItem";
            this.exportFiltered_ToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.exportFiltered_ToolStripMenuItem.Text = "Экспорт найденного...";
            this.exportFiltered_ToolStripMenuItem.Click += new System.EventHandler(this.exportFiltered_ToolStripMenuItem_Click);
            // 
            // exportSelected_ToolStripMenuItem
            // 
            this.exportSelected_ToolStripMenuItem.Name = "exportSelected_ToolStripMenuItem";
            this.exportSelected_ToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.exportSelected_ToolStripMenuItem.Text = "Экспорт выделенного...";
            this.exportSelected_ToolStripMenuItem.Click += new System.EventHandler(this.export_ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(201, 6);
            // 
            // exit_ToolStripMenuItem
            // 
            this.exit_ToolStripMenuItem.Name = "exit_ToolStripMenuItem";
            this.exit_ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.exit_ToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.exit_ToolStripMenuItem.Text = "Выход";
            this.exit_ToolStripMenuItem.Click += new System.EventHandler(this.exit_ToolStripMenuItem_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.toolStrip);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1008, 566);
            this.splitContainer1.SplitterDistance = 25;
            this.splitContainer1.SplitterWidth = 2;
            this.splitContainer1.TabIndex = 2;
            // 
            // toolStrip
            // 
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.event_toolStripComboBox,
            this.clearEvent_toolStripButton,
            this.toolStripSeparator2,
            this.toolStripLabel4,
            this.site_toolStripComboBox,
            this.clearSite_toolStripButton,
            this.toolStripSeparator5,
            this.toolStripLabel2,
            this.whitePlayer_toolStripComboBox,
            this.clearWhitePlayer_toolStripButton,
            this.toolStripSeparator3,
            this.toolStripLabel3,
            this.blackPlayer_toolStripComboBox,
            this.clearBlackPlayer_toolStripButton,
            this.toolStripSeparator4});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(1008, 25);
            this.toolStrip.TabIndex = 0;
            this.toolStrip.Text = "toolStrip";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(51, 22);
            this.toolStripLabel1.Text = "Турнир:";
            // 
            // event_toolStripComboBox
            // 
            this.event_toolStripComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.event_toolStripComboBox.Name = "event_toolStripComboBox";
            this.event_toolStripComboBox.Size = new System.Drawing.Size(150, 25);
            this.event_toolStripComboBox.Text = "Выберите турнир";
            this.event_toolStripComboBox.SelectedIndexChanged += new System.EventHandler(this.event_toolStripComboBox_SelectedIndexChanged);
            // 
            // clearEvent_toolStripButton
            // 
            this.clearEvent_toolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.clearEvent_toolStripButton.Enabled = false;
            this.clearEvent_toolStripButton.Image = global::ChessBox.Properties.Resources.remove;
            this.clearEvent_toolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.clearEvent_toolStripButton.Name = "clearEvent_toolStripButton";
            this.clearEvent_toolStripButton.Size = new System.Drawing.Size(23, 22);
            this.clearEvent_toolStripButton.Text = "Отменить фильтр по турнирам";
            this.clearEvent_toolStripButton.Click += new System.EventHandler(this.clearEvent_toolStripButton_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel4
            // 
            this.toolStripLabel4.Name = "toolStripLabel4";
            this.toolStripLabel4.Size = new System.Drawing.Size(45, 22);
            this.toolStripLabel4.Text = "Место:";
            // 
            // site_toolStripComboBox
            // 
            this.site_toolStripComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.site_toolStripComboBox.Name = "site_toolStripComboBox";
            this.site_toolStripComboBox.Size = new System.Drawing.Size(150, 25);
            this.site_toolStripComboBox.Text = "Выберите место";
            this.site_toolStripComboBox.SelectedIndexChanged += new System.EventHandler(this.site_toolStripComboBox_SelectedIndexChanged);
            // 
            // clearSite_toolStripButton
            // 
            this.clearSite_toolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.clearSite_toolStripButton.Enabled = false;
            this.clearSite_toolStripButton.Image = global::ChessBox.Properties.Resources.remove;
            this.clearSite_toolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.clearSite_toolStripButton.Name = "clearSite_toolStripButton";
            this.clearSite_toolStripButton.Size = new System.Drawing.Size(23, 22);
            this.clearSite_toolStripButton.Text = "Отменить фильтр по месту проведения турнира";
            this.clearSite_toolStripButton.Click += new System.EventHandler(this.clearSite_toolStripButton_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(45, 22);
            this.toolStripLabel2.Text = "Белые:";
            // 
            // whitePlayer_toolStripComboBox
            // 
            this.whitePlayer_toolStripComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.whitePlayer_toolStripComboBox.Name = "whitePlayer_toolStripComboBox";
            this.whitePlayer_toolStripComboBox.Size = new System.Drawing.Size(150, 25);
            this.whitePlayer_toolStripComboBox.Text = "Выберите шахматиста";
            this.whitePlayer_toolStripComboBox.SelectedIndexChanged += new System.EventHandler(this.whitePlayer_toolStripComboBox_SelectedIndexChanged);
            // 
            // clearWhitePlayer_toolStripButton
            // 
            this.clearWhitePlayer_toolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.clearWhitePlayer_toolStripButton.Enabled = false;
            this.clearWhitePlayer_toolStripButton.Image = global::ChessBox.Properties.Resources.remove;
            this.clearWhitePlayer_toolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.clearWhitePlayer_toolStripButton.Name = "clearWhitePlayer_toolStripButton";
            this.clearWhitePlayer_toolStripButton.Size = new System.Drawing.Size(23, 22);
            this.clearWhitePlayer_toolStripButton.Text = "Отменить фильтр по \"белым\" шахматистам";
            this.clearWhitePlayer_toolStripButton.Click += new System.EventHandler(this.clearWhitePlayer_toolStripButton_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(56, 22);
            this.toolStripLabel3.Text = "Черные: ";
            // 
            // blackPlayer_toolStripComboBox
            // 
            this.blackPlayer_toolStripComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.blackPlayer_toolStripComboBox.Name = "blackPlayer_toolStripComboBox";
            this.blackPlayer_toolStripComboBox.Size = new System.Drawing.Size(150, 25);
            this.blackPlayer_toolStripComboBox.Text = "Выберите шахматиста";
            this.blackPlayer_toolStripComboBox.SelectedIndexChanged += new System.EventHandler(this.blackPlayer_toolStripComboBox_SelectedIndexChanged);
            // 
            // clearBlackPlayer_toolStripButton
            // 
            this.clearBlackPlayer_toolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.clearBlackPlayer_toolStripButton.Enabled = false;
            this.clearBlackPlayer_toolStripButton.Image = global::ChessBox.Properties.Resources.remove;
            this.clearBlackPlayer_toolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.clearBlackPlayer_toolStripButton.Name = "clearBlackPlayer_toolStripButton";
            this.clearBlackPlayer_toolStripButton.Size = new System.Drawing.Size(23, 22);
            this.clearBlackPlayer_toolStripButton.Text = "Отменить фильтр по \"черным\" шахматистам";
            this.clearBlackPlayer_toolStripButton.Click += new System.EventHandler(this.clearBlackPlayer_toolStripButton_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.groupBox_seachFilter);
            this.splitContainer2.Panel1.Padding = new System.Windows.Forms.Padding(3);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer2.Size = new System.Drawing.Size(1008, 539);
            this.splitContainer2.SplitterWidth = 2;
            this.splitContainer2.TabIndex = 0;
            // 
            // groupBox_seachFilter
            // 
            this.groupBox_seachFilter.Controls.Add(this.piecePlacementFilter_checkBox);
            this.groupBox_seachFilter.Controls.Add(this.fenGenerator_button);
            this.groupBox_seachFilter.Controls.Add(this.fenChessboardState_textBox);
            this.groupBox_seachFilter.Controls.Add(this.fen_label);
            this.groupBox_seachFilter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox_seachFilter.Location = new System.Drawing.Point(3, 3);
            this.groupBox_seachFilter.Name = "groupBox_seachFilter";
            this.groupBox_seachFilter.Size = new System.Drawing.Size(1002, 44);
            this.groupBox_seachFilter.TabIndex = 0;
            this.groupBox_seachFilter.TabStop = false;
            this.groupBox_seachFilter.Text = " Поиск партии по заданной позиции ";
            // 
            // piecePlacementFilter_checkBox
            // 
            this.piecePlacementFilter_checkBox.AutoSize = true;
            this.piecePlacementFilter_checkBox.Location = new System.Drawing.Point(823, 20);
            this.piecePlacementFilter_checkBox.Name = "piecePlacementFilter_checkBox";
            this.piecePlacementFilter_checkBox.Size = new System.Drawing.Size(172, 17);
            this.piecePlacementFilter_checkBox.TabIndex = 3;
            this.piecePlacementFilter_checkBox.Text = "Учесть расположение фигур";
            this.piecePlacementFilter_checkBox.UseVisualStyleBackColor = true;
            this.piecePlacementFilter_checkBox.CheckedChanged += new System.EventHandler(this.piecePlacementFilter_checkBox_CheckedChanged);
            // 
            // fenGenerator_button
            // 
            this.fenGenerator_button.Location = new System.Drawing.Point(671, 16);
            this.fenGenerator_button.Name = "fenGenerator_button";
            this.fenGenerator_button.Size = new System.Drawing.Size(136, 23);
            this.fenGenerator_button.TabIndex = 2;
            this.fenGenerator_button.Text = "Генератор FEN";
            this.fenGenerator_button.UseVisualStyleBackColor = true;
            this.fenGenerator_button.Click += new System.EventHandler(this.fenGenerator_button_Click);
            // 
            // fenChessboardState_textBox
            // 
            this.fenChessboardState_textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fenChessboardState_textBox.Location = new System.Drawing.Point(46, 17);
            this.fenChessboardState_textBox.Name = "fenChessboardState_textBox";
            this.fenChessboardState_textBox.ReadOnly = true;
            this.fenChessboardState_textBox.Size = new System.Drawing.Size(619, 21);
            this.fenChessboardState_textBox.TabIndex = 1;
            // 
            // fen_label
            // 
            this.fen_label.AutoSize = true;
            this.fen_label.Location = new System.Drawing.Point(9, 21);
            this.fen_label.Name = "fen_label";
            this.fen_label.Size = new System.Drawing.Size(31, 13);
            this.fen_label.TabIndex = 0;
            this.fen_label.Text = "FEN:";
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer3.IsSplitterFixed = true;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.chessGames_dataGridView);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.tagPairs_groupBox);
            this.splitContainer3.Panel2.Padding = new System.Windows.Forms.Padding(3);
            this.splitContainer3.Size = new System.Drawing.Size(1008, 487);
            this.splitContainer3.SplitterDistance = 768;
            this.splitContainer3.SplitterWidth = 2;
            this.splitContainer3.TabIndex = 0;
            // 
            // chessGames_dataGridView
            // 
            this.chessGames_dataGridView.AllowUserToAddRows = false;
            this.chessGames_dataGridView.AllowUserToDeleteRows = false;
            this.chessGames_dataGridView.AllowUserToResizeRows = false;
            this.chessGames_dataGridView.AutoGenerateColumns = false;
            this.chessGames_dataGridView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.chessGames_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.chessGames_dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idColumn,
            this.eventColumn,
            this.siteColumn,
            this.dateColumn,
            this.roundColumn,
            this.whiteColumn,
            this.blackColumn,
            this.resultColumn,
            this.movetextColumn});
            this.chessGames_dataGridView.DataSource = this.chessGamesBindingSource;
            this.chessGames_dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chessGames_dataGridView.Location = new System.Drawing.Point(0, 0);
            this.chessGames_dataGridView.Name = "chessGames_dataGridView";
            this.chessGames_dataGridView.ReadOnly = true;
            this.chessGames_dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.chessGames_dataGridView.Size = new System.Drawing.Size(768, 487);
            this.chessGames_dataGridView.TabIndex = 0;
            this.chessGames_dataGridView.SelectionChanged += new System.EventHandler(this.chessGames_dataGridView_SelectionChanged);
            // 
            // idColumn
            // 
            this.idColumn.DataPropertyName = "id";
            this.idColumn.Frozen = true;
            this.idColumn.HeaderText = "id";
            this.idColumn.Name = "idColumn";
            this.idColumn.ReadOnly = true;
            this.idColumn.Visible = false;
            // 
            // eventColumn
            // 
            this.eventColumn.DataPropertyName = "event";
            this.eventColumn.Frozen = true;
            this.eventColumn.HeaderText = "Турнир";
            this.eventColumn.Name = "eventColumn";
            this.eventColumn.ReadOnly = true;
            this.eventColumn.Width = 125;
            // 
            // siteColumn
            // 
            this.siteColumn.DataPropertyName = "site";
            this.siteColumn.Frozen = true;
            this.siteColumn.HeaderText = "Место";
            this.siteColumn.Name = "siteColumn";
            this.siteColumn.ReadOnly = true;
            this.siteColumn.Width = 150;
            // 
            // dateColumn
            // 
            this.dateColumn.DataPropertyName = "date";
            this.dateColumn.Frozen = true;
            this.dateColumn.HeaderText = "date";
            this.dateColumn.Name = "dateColumn";
            this.dateColumn.ReadOnly = true;
            this.dateColumn.Visible = false;
            // 
            // roundColumn
            // 
            this.roundColumn.DataPropertyName = "round";
            this.roundColumn.Frozen = true;
            this.roundColumn.HeaderText = "round";
            this.roundColumn.Name = "roundColumn";
            this.roundColumn.ReadOnly = true;
            this.roundColumn.Visible = false;
            // 
            // whiteColumn
            // 
            this.whiteColumn.DataPropertyName = "white";
            this.whiteColumn.Frozen = true;
            this.whiteColumn.HeaderText = "Белые";
            this.whiteColumn.Name = "whiteColumn";
            this.whiteColumn.ReadOnly = true;
            this.whiteColumn.Width = 175;
            // 
            // blackColumn
            // 
            this.blackColumn.DataPropertyName = "black";
            this.blackColumn.Frozen = true;
            this.blackColumn.HeaderText = "Черные";
            this.blackColumn.Name = "blackColumn";
            this.blackColumn.ReadOnly = true;
            this.blackColumn.Width = 175;
            // 
            // resultColumn
            // 
            this.resultColumn.DataPropertyName = "result";
            this.resultColumn.Frozen = true;
            this.resultColumn.HeaderText = "Результат";
            this.resultColumn.Name = "resultColumn";
            this.resultColumn.ReadOnly = true;
            this.resultColumn.Width = 75;
            // 
            // movetextColumn
            // 
            this.movetextColumn.DataPropertyName = "movetext";
            this.movetextColumn.Frozen = true;
            this.movetextColumn.HeaderText = "movetext";
            this.movetextColumn.Name = "movetextColumn";
            this.movetextColumn.ReadOnly = true;
            this.movetextColumn.Visible = false;
            // 
            // chessGamesBindingSource
            // 
            this.chessGamesBindingSource.DataMember = "ChessGames";
            this.chessGamesBindingSource.DataSource = this.chessTournamentsDataSet;
            // 
            // chessTournamentsDataSet
            // 
            this.chessTournamentsDataSet.DataSetName = "ChessTournamentsDataSet";
            this.chessTournamentsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tagPairs_groupBox
            // 
            this.tagPairs_groupBox.Controls.Add(this.showSelectedChessGame_button);
            this.tagPairs_groupBox.Controls.Add(this.roundValue_label);
            this.tagPairs_groupBox.Controls.Add(this.round_label);
            this.tagPairs_groupBox.Controls.Add(this.eventValue_label);
            this.tagPairs_groupBox.Controls.Add(this.resultValue_label);
            this.tagPairs_groupBox.Controls.Add(this.blackValue_label);
            this.tagPairs_groupBox.Controls.Add(this.whiteValue_label);
            this.tagPairs_groupBox.Controls.Add(this.dateValue_label);
            this.tagPairs_groupBox.Controls.Add(this.siteValue_label);
            this.tagPairs_groupBox.Controls.Add(this.result_label);
            this.tagPairs_groupBox.Controls.Add(this.black_label);
            this.tagPairs_groupBox.Controls.Add(this.white_label);
            this.tagPairs_groupBox.Controls.Add(this.date_label);
            this.tagPairs_groupBox.Controls.Add(this.site_label);
            this.tagPairs_groupBox.Controls.Add(this.event_label);
            this.tagPairs_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tagPairs_groupBox.Location = new System.Drawing.Point(3, 3);
            this.tagPairs_groupBox.Name = "tagPairs_groupBox";
            this.tagPairs_groupBox.Size = new System.Drawing.Size(232, 481);
            this.tagPairs_groupBox.TabIndex = 1;
            this.tagPairs_groupBox.TabStop = false;
            this.tagPairs_groupBox.Text = "Информация о шахматной партии";
            // 
            // showSelectedChessGame_button
            // 
            this.showSelectedChessGame_button.Enabled = false;
            this.showSelectedChessGame_button.Location = new System.Drawing.Point(9, 200);
            this.showSelectedChessGame_button.Name = "showSelectedChessGame_button";
            this.showSelectedChessGame_button.Size = new System.Drawing.Size(220, 23);
            this.showSelectedChessGame_button.TabIndex = 8;
            this.showSelectedChessGame_button.Text = "Посмотреть выбранную игру";
            this.showSelectedChessGame_button.UseVisualStyleBackColor = true;
            this.showSelectedChessGame_button.Click += new System.EventHandler(this.showSelectedChessGame_button_Click);
            // 
            // roundValue_label
            // 
            this.roundValue_label.AutoSize = true;
            this.roundValue_label.Location = new System.Drawing.Point(117, 100);
            this.roundValue_label.Name = "roundValue_label";
            this.roundValue_label.Size = new System.Drawing.Size(10, 13);
            this.roundValue_label.TabIndex = 7;
            this.roundValue_label.Text = "-";
            // 
            // round_label
            // 
            this.round_label.AutoSize = true;
            this.round_label.Location = new System.Drawing.Point(6, 100);
            this.round_label.Name = "round_label";
            this.round_label.Size = new System.Drawing.Size(28, 13);
            this.round_label.TabIndex = 6;
            this.round_label.Text = "Тур:";
            // 
            // eventValue_label
            // 
            this.eventValue_label.AutoSize = true;
            this.eventValue_label.Location = new System.Drawing.Point(117, 25);
            this.eventValue_label.Name = "eventValue_label";
            this.eventValue_label.Size = new System.Drawing.Size(10, 13);
            this.eventValue_label.TabIndex = 5;
            this.eventValue_label.Text = "-";
            // 
            // resultValue_label
            // 
            this.resultValue_label.AutoSize = true;
            this.resultValue_label.Location = new System.Drawing.Point(117, 175);
            this.resultValue_label.Name = "resultValue_label";
            this.resultValue_label.Size = new System.Drawing.Size(10, 13);
            this.resultValue_label.TabIndex = 5;
            this.resultValue_label.Text = "-";
            // 
            // blackValue_label
            // 
            this.blackValue_label.AutoSize = true;
            this.blackValue_label.Location = new System.Drawing.Point(117, 150);
            this.blackValue_label.Name = "blackValue_label";
            this.blackValue_label.Size = new System.Drawing.Size(10, 13);
            this.blackValue_label.TabIndex = 5;
            this.blackValue_label.Text = "-";
            // 
            // whiteValue_label
            // 
            this.whiteValue_label.AutoSize = true;
            this.whiteValue_label.Location = new System.Drawing.Point(117, 125);
            this.whiteValue_label.Name = "whiteValue_label";
            this.whiteValue_label.Size = new System.Drawing.Size(10, 13);
            this.whiteValue_label.TabIndex = 5;
            this.whiteValue_label.Text = "-";
            // 
            // dateValue_label
            // 
            this.dateValue_label.AutoSize = true;
            this.dateValue_label.Location = new System.Drawing.Point(117, 75);
            this.dateValue_label.Name = "dateValue_label";
            this.dateValue_label.Size = new System.Drawing.Size(10, 13);
            this.dateValue_label.TabIndex = 5;
            this.dateValue_label.Text = "-";
            // 
            // siteValue_label
            // 
            this.siteValue_label.AutoSize = true;
            this.siteValue_label.Location = new System.Drawing.Point(117, 50);
            this.siteValue_label.Name = "siteValue_label";
            this.siteValue_label.Size = new System.Drawing.Size(10, 13);
            this.siteValue_label.TabIndex = 5;
            this.siteValue_label.Text = "-";
            // 
            // result_label
            // 
            this.result_label.AutoSize = true;
            this.result_label.Location = new System.Drawing.Point(6, 175);
            this.result_label.Name = "result_label";
            this.result_label.Size = new System.Drawing.Size(100, 13);
            this.result_label.TabIndex = 4;
            this.result_label.Text = "Результат партии:";
            // 
            // black_label
            // 
            this.black_label.AutoSize = true;
            this.black_label.Location = new System.Drawing.Point(6, 150);
            this.black_label.Name = "black_label";
            this.black_label.Size = new System.Drawing.Size(99, 13);
            this.black_label.TabIndex = 3;
            this.black_label.Text = "За черных играет:";
            // 
            // white_label
            // 
            this.white_label.AutoSize = true;
            this.white_label.Location = new System.Drawing.Point(6, 125);
            this.white_label.Name = "white_label";
            this.white_label.Size = new System.Drawing.Size(94, 13);
            this.white_label.TabIndex = 3;
            this.white_label.Text = "За белых играет:";
            // 
            // date_label
            // 
            this.date_label.AutoSize = true;
            this.date_label.Location = new System.Drawing.Point(6, 75);
            this.date_label.Name = "date_label";
            this.date_label.Size = new System.Drawing.Size(99, 13);
            this.date_label.TabIndex = 2;
            this.date_label.Text = "Дата проведения:";
            // 
            // site_label
            // 
            this.site_label.AutoSize = true;
            this.site_label.Location = new System.Drawing.Point(6, 50);
            this.site_label.Name = "site_label";
            this.site_label.Size = new System.Drawing.Size(105, 13);
            this.site_label.TabIndex = 1;
            this.site_label.Text = "Место проведения:";
            // 
            // event_label
            // 
            this.event_label.AutoSize = true;
            this.event_label.Location = new System.Drawing.Point(6, 25);
            this.event_label.Name = "event_label";
            this.event_label.Size = new System.Drawing.Size(103, 13);
            this.event_label.TabIndex = 0;
            this.event_label.Text = "Название турнира:";
            // 
            // chessGamesTableAdapter
            // 
            this.chessGamesTableAdapter.ClearBeforeFill = true;
            // 
            // ChessTournamentsSearchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 612);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip);
            this.MainMenuStrip = this.menuStrip;
            this.MinimumSize = new System.Drawing.Size(850, 600);
            this.Name = "ChessTournamentsSearchForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Шахматные турниры";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ChessTournamentsSearchForm_FormClosing);
            this.Load += new System.EventHandler(this.ChessTournamentsSearchForm_Load);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.groupBox_seachFilter.ResumeLayout(false);
            this.groupBox_seachFilter.PerformLayout();
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chessGames_dataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chessGamesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chessTournamentsDataSet)).EndInit();
            this.tagPairs_groupBox.ResumeLayout(false);
            this.tagPairs_groupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.GroupBox groupBox_seachFilter;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ImportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exit_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportSelected_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.GroupBox tagPairs_groupBox;
        private System.Windows.Forms.Label roundValue_label;
        private System.Windows.Forms.Label round_label;
        private System.Windows.Forms.Label eventValue_label;
        private System.Windows.Forms.Label resultValue_label;
        private System.Windows.Forms.Label blackValue_label;
        private System.Windows.Forms.Label whiteValue_label;
        private System.Windows.Forms.Label dateValue_label;
        private System.Windows.Forms.Label siteValue_label;
        private System.Windows.Forms.Label result_label;
        private System.Windows.Forms.Label black_label;
        private System.Windows.Forms.Label white_label;
        private System.Windows.Forms.Label date_label;
        private System.Windows.Forms.Label site_label;
        private System.Windows.Forms.Label event_label;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripComboBox event_toolStripComboBox;
        private System.Windows.Forms.ToolStripButton clearEvent_toolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripComboBox whitePlayer_toolStripComboBox;
        private System.Windows.Forms.ToolStripButton clearWhitePlayer_toolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripComboBox blackPlayer_toolStripComboBox;
        private System.Windows.Forms.ToolStripButton clearBlackPlayer_toolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel_toolStrip;
        private System.Windows.Forms.ToolStripLabel toolStripLabel4;
        private System.Windows.Forms.ToolStripComboBox site_toolStripComboBox;
        private System.Windows.Forms.ToolStripButton clearSite_toolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.TextBox fenChessboardState_textBox;
        private System.Windows.Forms.Label fen_label;
        private System.Windows.Forms.DataGridView chessGames_dataGridView;
        private Database.ChessTournamentsDatabaseDataSet chessTournamentsDataSet;
        private System.Windows.Forms.BindingSource chessGamesBindingSource;
        private Database.ChessTournamentsDatabaseDataSetTableAdapters.ChessGamesTableAdapter chessGamesTableAdapter;
        private System.Windows.Forms.ToolStripMenuItem exportFiltered_ToolStripMenuItem;
        private System.Windows.Forms.Button showSelectedChessGame_button;
        private System.Windows.Forms.Button fenGenerator_button;
        private System.Windows.Forms.CheckBox piecePlacementFilter_checkBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn idColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn eventColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn siteColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn roundColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn whiteColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn blackColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn resultColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn movetextColumn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
    }
}