﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.IO;

namespace ChessBox
{
    public partial class PGNGamesImportForm : Form
    {
        public PGNGamesImportForm()
        {
            InitializeComponent();
        }

        private bool closeFormOnCloseButtonClick = false;
        private PGNImporter importer;

        private void button_SelectPGNFile_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                textBox_PGNFilePath.Text = openFileDialog.FileName;
            }
        }

        private void button_Import_Click(object sender, EventArgs e)
        {
            button_SelectPGNFile.Enabled = false;
            textBox_PGNFilePath.Enabled = false;
            button_Import.Enabled = false;

            try
            {
                importer = new PGNImporter(textBox_PGNFilePath.Text, checkBox_withIndexing.Checked);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);

                button_SelectPGNFile.Enabled = true;
                textBox_PGNFilePath.Enabled = true;
                button_Import.Enabled = true;

                return;
            }

            progressBar_Import.Minimum = 0;
            progressBar_Import.Value = 0;
            progressBar_Import.Maximum = importer.GetApproximateTournamentNumber();

            importer.OnTournamentImportEvent += new PGNImporter.ImportEventHandler(importer_OnTournamentImportEvent);
            importer.OnAllTournamentsImportEvent += new PGNImporter.ImportEventHandler(importer_OnAllTournamentsImportEvent);
            label_ActionDescription.Text = "Импорт шахматных партий из \"" + textBox_PGNFilePath.Text + "\".";

            importer.ImportFromPGNAsync();
        }

        void importer_OnAllTournamentsImportEvent(object sender, ImportEventArgs e)
        {
            progressBar_Import.Invoke(new MethodInvoker(delegate()
            {
                progressBar_Import.Value = progressBar_Import.Maximum;
                label_ActionDescription.Text = "Импорт завершен успешно. Всего обработано: " + e.TournametnNumber.ToString() + " " + GetGameWordFormForNumber(e.TournametnNumber) + ".";

                button_SelectPGNFile.Enabled = true;
                textBox_PGNFilePath.Enabled = true;
                button_Import.Enabled = true;
            }));
        }

        void importer_OnTournamentImportEvent(object sender, ImportEventArgs e)
        {
            progressBar_Import.Invoke(new MethodInvoker(delegate() {
                if (progressBar_Import.Maximum - progressBar_Import.Value > 1)
                {
                    progressBar_Import.Value++;
                }
            }));
           
        }

        string GetGameWordFormForNumber(int number)
        {
            const string FIRST_GAME_WORD_FORM = "шахматная партия";
            const string SECOND_GAME_WORD_FORM = "шахматные партии";
            const string THIRD_GAME_WORD_FORM = "шахматных партий";

            number = Math.Abs(number) % 100;
            int one_digit = number % 10;

            if (number > 10 && number < 20)
                return THIRD_GAME_WORD_FORM;
            if (one_digit > 1 && one_digit < 5)
                return SECOND_GAME_WORD_FORM;
            if (one_digit == 1)
                return FIRST_GAME_WORD_FORM;

            return THIRD_GAME_WORD_FORM;
        }

        private void PGNGamesImportForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!this.closeFormOnCloseButtonClick)
            {
                CloseFormOnCloseButtonClick();
            }
        }

        public void CloseFormOnCloseButtonClick()
        {
            if (this.importer != null && this.importer.IsImporting)
            {
                label_ActionDescription.Text = "Остановка процесса импорта шахматных партий...";
                importer.StopImporting();
            }
            this.closeFormOnCloseButtonClick = true;
            this.Close();
        }

        private void button_Close_Click(object sender, EventArgs e)
        {
            CloseFormOnCloseButtonClick();
        }
    }
}
