﻿namespace ChessBox
{
    partial class PGNGamesImportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_ActionDescription = new System.Windows.Forms.Label();
            this.button_Close = new System.Windows.Forms.Button();
            this.button_Import = new System.Windows.Forms.Button();
            this.label_Action = new System.Windows.Forms.Label();
            this.progressBar_Import = new System.Windows.Forms.ProgressBar();
            this.textBox_PGNFilePath = new System.Windows.Forms.TextBox();
            this.button_SelectPGNFile = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.checkBox_withIndexing = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label_ActionDescription
            // 
            this.label_ActionDescription.AutoSize = true;
            this.label_ActionDescription.Location = new System.Drawing.Point(70, 38);
            this.label_ActionDescription.Name = "label_ActionDescription";
            this.label_ActionDescription.Size = new System.Drawing.Size(0, 13);
            this.label_ActionDescription.TabIndex = 0;
            // 
            // button_Close
            // 
            this.button_Close.Location = new System.Drawing.Point(528, 87);
            this.button_Close.Name = "button_Close";
            this.button_Close.Size = new System.Drawing.Size(95, 23);
            this.button_Close.TabIndex = 1;
            this.button_Close.Text = "Закрыть";
            this.button_Close.UseVisualStyleBackColor = true;
            this.button_Close.Click += new System.EventHandler(this.button_Close_Click);
            // 
            // button_Import
            // 
            this.button_Import.Location = new System.Drawing.Point(427, 87);
            this.button_Import.Name = "button_Import";
            this.button_Import.Size = new System.Drawing.Size(95, 23);
            this.button_Import.TabIndex = 2;
            this.button_Import.Text = "Импорт";
            this.button_Import.UseVisualStyleBackColor = true;
            this.button_Import.Click += new System.EventHandler(this.button_Import_Click);
            // 
            // label_Action
            // 
            this.label_Action.AutoSize = true;
            this.label_Action.Location = new System.Drawing.Point(9, 38);
            this.label_Action.Name = "label_Action";
            this.label_Action.Size = new System.Drawing.Size(60, 13);
            this.label_Action.TabIndex = 3;
            this.label_Action.Text = "Действие:";
            // 
            // progressBar_Import
            // 
            this.progressBar_Import.Location = new System.Drawing.Point(12, 58);
            this.progressBar_Import.Name = "progressBar_Import";
            this.progressBar_Import.Size = new System.Drawing.Size(610, 23);
            this.progressBar_Import.TabIndex = 4;
            // 
            // textBox_PGNFilePath
            // 
            this.textBox_PGNFilePath.Location = new System.Drawing.Point(12, 12);
            this.textBox_PGNFilePath.Name = "textBox_PGNFilePath";
            this.textBox_PGNFilePath.Size = new System.Drawing.Size(580, 20);
            this.textBox_PGNFilePath.TabIndex = 5;
            this.textBox_PGNFilePath.Text = "C:\\";
            // 
            // button_SelectPGNFile
            // 
            this.button_SelectPGNFile.Location = new System.Drawing.Point(598, 10);
            this.button_SelectPGNFile.Name = "button_SelectPGNFile";
            this.button_SelectPGNFile.Size = new System.Drawing.Size(25, 23);
            this.button_SelectPGNFile.TabIndex = 2;
            this.button_SelectPGNFile.Text = "...";
            this.button_SelectPGNFile.UseVisualStyleBackColor = true;
            this.button_SelectPGNFile.Click += new System.EventHandler(this.button_SelectPGNFile_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "PGN files|*.pgn|All files|*.*";
            this.openFileDialog.InitialDirectory = "C:\\";
            // 
            // checkBox_withIndexing
            // 
            this.checkBox_withIndexing.AutoSize = true;
            this.checkBox_withIndexing.Checked = true;
            this.checkBox_withIndexing.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_withIndexing.Enabled = false;
            this.checkBox_withIndexing.Location = new System.Drawing.Point(12, 87);
            this.checkBox_withIndexing.Name = "checkBox_withIndexing";
            this.checkBox_withIndexing.Size = new System.Drawing.Size(376, 17);
            this.checkBox_withIndexing.TabIndex = 6;
            this.checkBox_withIndexing.Text = "Проиндексировать игры для поиска по состоянию шахматной доски";
            this.checkBox_withIndexing.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(9, 104);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(417, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Внимание! Индексирование может увеличить время импортирования до 10 раз!";
            // 
            // PGNGamesImportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 122);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkBox_withIndexing);
            this.Controls.Add(this.textBox_PGNFilePath);
            this.Controls.Add(this.progressBar_Import);
            this.Controls.Add(this.label_Action);
            this.Controls.Add(this.button_SelectPGNFile);
            this.Controls.Add(this.button_Import);
            this.Controls.Add(this.button_Close);
            this.Controls.Add(this.label_ActionDescription);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PGNGamesImportForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Импорт шахматных партий из PGN-файла";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PGNGamesImportForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_ActionDescription;
        private System.Windows.Forms.Button button_Close;
        private System.Windows.Forms.Button button_Import;
        private System.Windows.Forms.Label label_Action;
        private System.Windows.Forms.ProgressBar progressBar_Import;
        private System.Windows.Forms.TextBox textBox_PGNFilePath;
        private System.Windows.Forms.Button button_SelectPGNFile;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.CheckBox checkBox_withIndexing;
        private System.Windows.Forms.Label label1;
    }
}