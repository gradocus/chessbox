﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using ChessboardGamesCore.IO.PGN;

namespace ChessBox
{
    public partial class PGNGamesExportForm : Form
    {
        public PGNGamesExportForm(Int64[] pgnGames)
        {
            this.pgnGameIDs = pgnGames;

            InitializeComponent();
        }

        private object progressBarLockObject = new object();
        private bool closeFormOnCloseButtonClick = false;
        private PGNExporter exporter;
        private Int64[] pgnGameIDs;

        private void export_button_Click(object sender, EventArgs e)
        {
            export_button.Enabled = false;
            button_SavePGNFile.Enabled = false;
            textBox_PGNFilePath.Enabled = false;

            try
            {
                this.exporter = new PGNExporter(this.textBox_PGNFilePath.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);

                export_button.Enabled = true;
                button_SavePGNFile.Enabled = true;
                textBox_PGNFilePath.Enabled = true;

                return;
            }

            export_progressBar.Minimum = 0;
            export_progressBar.Value = 0;
            export_progressBar.Maximum = this.pgnGameIDs.Length;

            exporter.OnTournamentExportEvent += new PGNExporter.ExportEventHandler(exporter_OnTournamentExportEvent);
            exporter.OnAllTournamentsExportEvent += new PGNExporter.ExportEventHandler(exporter_OnAllTournamentsExportEvent);

            label_ActionDescription.Text = "Экспорт шахматных партий в \"" + textBox_PGNFilePath.Text + "\".";
            exporter.ExportIntoPGNAsync(this.pgnGameIDs);
        }

        void exporter_OnAllTournamentsExportEvent(object sender, ExportEventArgs e)
        {
            export_progressBar.Invoke(new MethodInvoker(delegate()
            {
                export_progressBar.Value = export_progressBar.Maximum;
                label_ActionDescription.Text = "Экспорт шахматных партий завершен успешно. Всего обработано: " + e.TournametnNumber.ToString() + " " + GetGameWordFormForNumber(e.TournametnNumber) + ".";

                export_button.Enabled = true;
                button_SavePGNFile.Enabled = true;
                textBox_PGNFilePath.Enabled = true;
            }));
        }

        void exporter_OnTournamentExportEvent(object sender, ExportEventArgs e)
        {
            export_progressBar.Invoke(new MethodInvoker(delegate()
            {
                if (export_progressBar.Maximum - export_progressBar.Value > 1)
                {
                        export_progressBar.Value++;

                }
            }));
        }

        private void button_SavePGNFile_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                textBox_PGNFilePath.Text = saveFileDialog.FileName;
            }
        }

        private void CloseFormOnCloseButtonClick()
        {
            if (this.exporter != null && this.exporter.IsExporting)
            {
                label_ActionDescription.Text = "Остановка процесса экспорта шахматных партий...";
                exporter.StopExporting();
            }
            this.closeFormOnCloseButtonClick = true;
            this.Close();
        }

        private void button_Close_Click(object sender, EventArgs e)
        {
            CloseFormOnCloseButtonClick();
        }

        private void PGNGamesExportForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!this.closeFormOnCloseButtonClick)
            {
                CloseFormOnCloseButtonClick();
            }
        }

        string GetGameWordFormForNumber(int number)
        {
            const string FIRST_GAME_WORD_FORM = "шахматная партия";
            const string SECOND_GAME_WORD_FORM = "шахматные партии";
            const string THIRD_GAME_WORD_FORM = "шахматных партий";

            number = Math.Abs(number) % 100;
            int one_digit = number % 10;

            if (number > 10 && number < 20)
                return THIRD_GAME_WORD_FORM;
            if (one_digit > 1 && one_digit < 5)
                return SECOND_GAME_WORD_FORM;
            if (one_digit == 1)
                return FIRST_GAME_WORD_FORM;

            return THIRD_GAME_WORD_FORM;
        }
    }
}
