﻿namespace ChessBox
{
    partial class PGNGamesExportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox_PGNFilePath = new System.Windows.Forms.TextBox();
            this.export_progressBar = new System.Windows.Forms.ProgressBar();
            this.label_Action = new System.Windows.Forms.Label();
            this.button_SavePGNFile = new System.Windows.Forms.Button();
            this.export_button = new System.Windows.Forms.Button();
            this.button_Close = new System.Windows.Forms.Button();
            this.label_ActionDescription = new System.Windows.Forms.Label();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.SuspendLayout();
            // 
            // textBox_PGNFilePath
            // 
            this.textBox_PGNFilePath.Location = new System.Drawing.Point(12, 12);
            this.textBox_PGNFilePath.Name = "textBox_PGNFilePath";
            this.textBox_PGNFilePath.Size = new System.Drawing.Size(580, 20);
            this.textBox_PGNFilePath.TabIndex = 14;
            this.textBox_PGNFilePath.Text = "C:\\";
            // 
            // export_progressBar
            // 
            this.export_progressBar.Location = new System.Drawing.Point(12, 58);
            this.export_progressBar.Name = "export_progressBar";
            this.export_progressBar.Size = new System.Drawing.Size(610, 23);
            this.export_progressBar.TabIndex = 13;
            // 
            // label_Action
            // 
            this.label_Action.AutoSize = true;
            this.label_Action.Location = new System.Drawing.Point(9, 38);
            this.label_Action.Name = "label_Action";
            this.label_Action.Size = new System.Drawing.Size(60, 13);
            this.label_Action.TabIndex = 12;
            this.label_Action.Text = "Действие:";
            // 
            // button_SavePGNFile
            // 
            this.button_SavePGNFile.Location = new System.Drawing.Point(598, 10);
            this.button_SavePGNFile.Name = "button_SavePGNFile";
            this.button_SavePGNFile.Size = new System.Drawing.Size(25, 23);
            this.button_SavePGNFile.TabIndex = 10;
            this.button_SavePGNFile.Text = "...";
            this.button_SavePGNFile.UseVisualStyleBackColor = true;
            this.button_SavePGNFile.Click += new System.EventHandler(this.button_SavePGNFile_Click);
            // 
            // export_button
            // 
            this.export_button.Location = new System.Drawing.Point(427, 87);
            this.export_button.Name = "export_button";
            this.export_button.Size = new System.Drawing.Size(95, 23);
            this.export_button.TabIndex = 11;
            this.export_button.Text = "Экспорт";
            this.export_button.UseVisualStyleBackColor = true;
            this.export_button.Click += new System.EventHandler(this.export_button_Click);
            // 
            // button_Close
            // 
            this.button_Close.Location = new System.Drawing.Point(528, 87);
            this.button_Close.Name = "button_Close";
            this.button_Close.Size = new System.Drawing.Size(95, 23);
            this.button_Close.TabIndex = 9;
            this.button_Close.Text = "Закрыть";
            this.button_Close.UseVisualStyleBackColor = true;
            this.button_Close.Click += new System.EventHandler(this.button_Close_Click);
            // 
            // label_ActionDescription
            // 
            this.label_ActionDescription.AutoSize = true;
            this.label_ActionDescription.Location = new System.Drawing.Point(70, 38);
            this.label_ActionDescription.Name = "label_ActionDescription";
            this.label_ActionDescription.Size = new System.Drawing.Size(0, 13);
            this.label_ActionDescription.TabIndex = 8;
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "pgn";
            this.saveFileDialog.Filter = "PGN files|*.pgn|All files|*.*";
            this.saveFileDialog.InitialDirectory = "C:\\";
            this.saveFileDialog.OverwritePrompt = false;
            // 
            // PGNGamesExportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 122);
            this.Controls.Add(this.textBox_PGNFilePath);
            this.Controls.Add(this.export_progressBar);
            this.Controls.Add(this.label_Action);
            this.Controls.Add(this.button_SavePGNFile);
            this.Controls.Add(this.export_button);
            this.Controls.Add(this.button_Close);
            this.Controls.Add(this.label_ActionDescription);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PGNGamesExportForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Экспорт шахматных партий в PGN-файл";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PGNGamesExportForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_PGNFilePath;
        private System.Windows.Forms.ProgressBar export_progressBar;
        private System.Windows.Forms.Label label_Action;
        private System.Windows.Forms.Button button_SavePGNFile;
        private System.Windows.Forms.Button export_button;
        private System.Windows.Forms.Button button_Close;
        private System.Windows.Forms.Label label_ActionDescription;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
    }
}