﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ChessboardGamesCore;
using ChessboardGamesCore.IO.FEN;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace ChessBox.ChessGameCoreExtensions
{
    public static class ChessboardExtensions
    {
        public static FENPiecePlacement GetFENPiecePlacement(this Chessboard chessboard)
        {
            FENPiecePlacement piecePlacement = new FENPiecePlacement();

            for (int file = 0; file < 8; file++)
            {
                for (int rank = 0; rank < 8; rank++)
                {
                    if (chessboard[file, rank] != null)
                    {
                        piecePlacement[file, rank] = chessboard[file, rank].GetANLetter();
                    }
                }
            }

            return piecePlacement;
        }

        public static void LoadStateByFENPiecePlacement(this Chessboard chessboard, FENPiecePlacement piecePlacement)
        {
            for (int file = 0; file < 8; file++) // From A to H.
            {
                for (int rank = 0; rank < 8; rank++) // From 1 to 8.
                {
                    ChessboardCoordinate coordinate = new ChessboardCoordinate(file, rank);
                    if (piecePlacement[file, rank] != FENPiecePlacement.EMPTY_PLACE)
                    {
                        if (chessboard[coordinate] != null)
                            chessboard.RemovePiece(coordinate);
                        chessboard.SetPiece(GetPieceByANPieceName(piecePlacement[file, rank]), coordinate);
                    }
                    else
                        if (chessboard[coordinate] != null)
                            chessboard.RemovePiece(coordinate);
                }
            }
        }

        public static Int64 GetBitmask(this Chessboard chessboardState)
        {
            StringBuilder bitmaskStringBuilder = new StringBuilder();

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (chessboardState[i, j] != null)
                        bitmaskStringBuilder.Append("1");
                    else
                        bitmaskStringBuilder.Append("0");
                }
            }

            return Convert.ToInt64(bitmaskStringBuilder.ToString(), 2);
        }


        public static Image GetImage(this Chessboard chessboard)
        {
            return chessboard.GetImage(new List<ChessboardCoordinate>());
        }

        public static Image GetImage(this Chessboard chessboard, List<ChessboardCoordinate> markedSquares)
        {
            return chessboard.DrawSquares(Properties.Resources.chessboard, markedSquares, Color.YellowGreen);
        }

        public static Image DrawCursorSquare(this Chessboard chessboard, Image image, List<ChessboardCoordinate> markedSquares)
        {
            return chessboard.DrawSquares(image, markedSquares, Color.GreenYellow);
        }

        public static Image DrawPath(this Chessboard chessboard, Image image, List<ChessboardCoordinate> markedSquares)
        {
            const int SQUARE_SIZE = 55;
            const int CHESSBOARD_PADDING = 15;

            Bitmap b = new Bitmap(image);
            Graphics graphics = Graphics.FromImage(b);

            ChessboardCoordinate firstCoordinate = null;
            foreach (ChessboardCoordinate coordinate in markedSquares)
            {
                if (firstCoordinate == null)
                {
                    firstCoordinate = coordinate;
                    continue;
                }
                int x1 = CHESSBOARD_PADDING + firstCoordinate.File * SQUARE_SIZE + SQUARE_SIZE / 2;
                int y1 = CHESSBOARD_PADDING + (7 - firstCoordinate.Rank) * SQUARE_SIZE + SQUARE_SIZE / 2;

                int x2 = CHESSBOARD_PADDING + coordinate.File * SQUARE_SIZE + SQUARE_SIZE / 2;
                int y2 = CHESSBOARD_PADDING + (7 - coordinate.Rank) * SQUARE_SIZE + SQUARE_SIZE / 2;

                graphics.SmoothingMode = SmoothingMode.AntiAlias;
                Pen p = new Pen(Color.Orange, 5);
                p.StartCap = LineCap.Round;
                p.EndCap = LineCap.ArrowAnchor;
                graphics.DrawLine(p, x1, y1, x2, y2);
                p.Dispose();

                firstCoordinate = coordinate;
            }
            
            graphics.Dispose();
            return b;
        }

        private static Image DrawSquares(this Chessboard chessboard, Image image, List<ChessboardCoordinate> markedSquares, Color color)
        {
            const int SQUARE_SIZE = 55;
            const int CHESSBOARD_PADDING = 15;

            Bitmap b = new Bitmap(image);
            Graphics graphics = Graphics.FromImage(b);

            for (int file = 0; file < 8; file++) // From A to H.
            {
                for (int rank = 0; rank < 8; rank++) // From 1 to 8.
                {
                    ChessboardCoordinate coordinate = new ChessboardCoordinate(file, rank);
                    if (markedSquares.Contains(coordinate))
                    {
                        Rectangle squareRectangle = new Rectangle(CHESSBOARD_PADDING + file * SQUARE_SIZE, CHESSBOARD_PADDING + (7 - rank) * SQUARE_SIZE, SQUARE_SIZE, SQUARE_SIZE);
                        graphics.DrawRectangle(new Pen(color, 2f), squareRectangle);
                    }

                    if (chessboard[coordinate] != null)
                    {
                        graphics.DrawImage(chessboard[coordinate].GetImage(), new Point(CHESSBOARD_PADDING + file * SQUARE_SIZE, CHESSBOARD_PADDING + (7 - rank) * SQUARE_SIZE));
                    }
                }
            }

            graphics.Dispose();
            return b;
        }

        private static Piece GetPieceByANPieceName(char pieceLetter)
        {
            Piece iPiece;
            string stringPieceLetter = pieceLetter.ToString();
            PieceColor pieceColor = (stringPieceLetter.ToUpper().Equals(stringPieceLetter)) ? PieceColor.White : PieceColor.Black;

            switch (stringPieceLetter.ToLower())
            {
                case "r":
                    iPiece = new Piece(PieceType.Rook, pieceColor);
                    break;
                case "n":
                    iPiece = new Piece(PieceType.Knight, pieceColor);
                    break;
                case "b":
                    iPiece = new Piece(PieceType.Bishop, pieceColor);
                    break;
                case "q":
                    iPiece = new Piece(PieceType.Queen, pieceColor);
                    break;
                case "k":
                    iPiece = new Piece(PieceType.King, pieceColor);
                    break;
                default:
                    iPiece = new Piece(PieceType.Pawn, pieceColor);
                    break;
            }

            return iPiece;
        }
    }
}
