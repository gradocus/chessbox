﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Drawing;
using ChessboardGamesCore;
using ChessboardGamesCore.IO.FEN;

namespace ChessBox.ChessGameCoreExtensions
{
    public static class GameExtensions
    {
        public static FENData GetFENData(this IGame chessGame)
        {
            FENData fenData = new FENData();

            fenData.PiecePlacement = chessGame.Chessboard.GetFENPiecePlacement();
            fenData.ActiveColor = (chessGame as ChessGame).NextMoveColor;
            fenData.CastlingAvailability = new FENCastlingAvailability();

            return fenData;
        }

        public static Image GetGameStateImage(this IGame chessGame)
        {
            List<ChessboardCoordinate> markedSquares = new List<ChessboardCoordinate>();
            
            if (chessGame.MovementHistory.Position > 0)
            {
                ICommand command = chessGame.MovementHistory.Commands[chessGame.MovementHistory.Position - 1];
                markedSquares = new List<ChessboardCoordinate>(
                    chessGame.Chessboard[command.DestinationCoordinate]
                        .MovementHistory.ToArray().SubArray(chessGame.Chessboard[command.DestinationCoordinate]
                        .MovementHistory.Count - 2, 2));
            }

            return chessGame.Chessboard.GetImage(markedSquares);
        }

        public static T[] SubArray<T>(this T[] data, int index, int length)
        {
            T[] result = new T[length];
            Array.Copy(data, index, result, 0, length);
            return result;
        }
    }
}
