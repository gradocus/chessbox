﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ChessboardGamesCore;
using System.Drawing;

namespace ChessBox.ChessGameCoreExtensions
{
    public static class PieceExtensions
    {
        public static char GetANLetter(this Piece piece)
        {
            string pieceName = piece.Type.ToString();
            
            string pieceLetter = pieceName[0].ToString();
            if (pieceName.Equals("Knight"))
            {
                pieceLetter = "n";
            }

            return (piece.Color == PieceColor.White ? pieceLetter.ToUpper() : pieceLetter.ToLower())[0];
        }

        public static Image GetImage(this Piece piece)
        {
            switch (piece.GetANLetter().ToString().ToLower()[0])
            {
                case 'b':
                    return piece.Color == PieceColor.White ? Properties.Resources.whiteBishop : Properties.Resources.blackBishop;
                case 'k':
                    return piece.Color == PieceColor.White ? Properties.Resources.whiteKing : Properties.Resources.blackKing;
                case 'n':
                    return piece.Color == PieceColor.White ? Properties.Resources.whiteKnight : Properties.Resources.blackKnight;
                case 'p':
                    return piece.Color == PieceColor.White ? Properties.Resources.whitePawn : Properties.Resources.blackPawn;
                case 'q':
                    return piece.Color == PieceColor.White ? Properties.Resources.whiteQueen : Properties.Resources.blackQueen;
                case 'r':
                    return piece.Color == PieceColor.White ? Properties.Resources.whiteRook : Properties.Resources.blackRook;
            }

            throw new Exception("Unknown piece type.");
        }
    }
}
