﻿namespace ChessBox
{
    partial class StaticticForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statictic_dataGridView = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.statictic_dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // statictic_dataGridView
            // 
            this.statictic_dataGridView.AllowUserToAddRows = false;
            this.statictic_dataGridView.AllowUserToDeleteRows = false;
            this.statictic_dataGridView.AllowUserToResizeRows = false;
            this.statictic_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.statictic_dataGridView.Location = new System.Drawing.Point(12, 12);
            this.statictic_dataGridView.Name = "statictic_dataGridView";
            this.statictic_dataGridView.ReadOnly = true;
            this.statictic_dataGridView.Size = new System.Drawing.Size(380, 238);
            this.statictic_dataGridView.TabIndex = 0;
            // 
            // StaticticForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 262);
            this.Controls.Add(this.statictic_dataGridView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "StaticticForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Стистика по данной ситуации";
            this.Load += new System.EventHandler(this.StaticticForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.statictic_dataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView statictic_dataGridView;
    }
}