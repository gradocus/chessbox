﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections;

namespace ChessBox.Database
{
    public class Filter
    {
        public static string EQUALS = "=";

        public static string ANY_VALUE = "*";

        public Filter()
        {
            filters = new Hashtable();
        }

        protected Hashtable filters;

        public void SetFilter(string column, string condition, string value)
        {
            this.RemoveColumnIfExists(column);
            if (value != ANY_VALUE)
                this.filters.Add(column, condition + " '" + value.Replace("'", "''") + "'");
        }

        private void RemoveColumnIfExists(string column)
        {
            if (this.filters.ContainsKey(column))
            {
                this.filters.Remove(column);
            }
        }

        public override string ToString()
        {
            StringBuilder resultStringBuilder = new StringBuilder();

            foreach (string column in this.filters.Keys)
            {
                if (resultStringBuilder.Length != 0)
                {
                    resultStringBuilder.Append(" AND ");
                }

                resultStringBuilder.Append(column + " " + this.filters[column].ToString());
            }
            
            return resultStringBuilder.ToString();
        }
    }
}
