﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.SqlServerCe;
using ChessboardGamesCore;
using System.Data;
using ChessboardGamesCore.IO;
using ChessboardGamesCore.IO.PGN;

using ChessBox.ChessGameCoreExtensions;

namespace ChessBox.Database
{
    public class ChessTournamentsDataBase
    {
        public ChessTournamentsDataBase()
        {
            InitializeSQLConnection();
        }

        protected static void InitializeSQLConnection()
        {
            if (sqlConnection == null)
            {
                sqlConnection = new SqlCeConnection();
                sqlConnection.ConnectionString = Properties.Settings.Default.ChessTournamentsDatabaseConnectionString;
            }
        }

        protected static SqlCeConnection sqlConnection;
        private static List<string>
            sevenTagRosterList = new List<string>(new string[] { "Event", "Site", "Date", "Round", "White", "Black", "Result" });

        private const int NONE_GAME_ID = -1;
        private Int64 lastPGNGameID = NONE_GAME_ID;

        public Int64 GetLastPGNGameID()
        {
            if (this.lastPGNGameID != NONE_GAME_ID)
                return this.lastPGNGameID;

            SqlCeDataReader dataReader = this.ExecuteReader("SELECT MAX([id]) FROM [ChessGames]");

            if (dataReader.Read())
            {
                this.lastPGNGameID = (Int64)dataReader[0];
                return (Int64)dataReader[0];
            }

            return 0;
        }

        public void AddPGNGame(PGNGameRecord pgnGame)
        {
            AddPGNGame(pgnGame, false);
        }

        public void AddPGNGame(PGNGameRecord pgnGame, bool withIndexing)
        {
            StringBuilder queryBuilder = new StringBuilder("INSERT INTO [ChessGames] ([event], [site], [date], [round], [white], [black], [result], [movetext]) VALUES (");
            foreach (string tagName in sevenTagRosterList)
            {
                queryBuilder.Append("'");
                queryBuilder.Append(pgnGame.TagPairs.GetTagValueByName(tagName).Replace("'", "''"));
                queryBuilder.Append("', ");
            }
            queryBuilder.Append("'");
            queryBuilder.Append(pgnGame.Movetext.ToString().Replace("'", "''"));
            queryBuilder.Append("')");

            this.ExecuteNonQuery(queryBuilder.ToString());
            this.lastPGNGameID = (this.lastPGNGameID == NONE_GAME_ID ? NONE_GAME_ID : this.lastPGNGameID + 1);

            // Get ID of added pgn game.
            Int64 pgnGameID = this.GetLastPGNGameID();

            // Add additional tags if exists.
            List<String> additionalTagNameList = new List<string>(pgnGame.TagPairs.GetTagNames());
            additionalTagNameList.RemoveAll(tagName => sevenTagRosterList.Contains(tagName));
            queryBuilder = new StringBuilder("INSERT INTO [AdditionalTags] ([chess_game_id], [name], [value]) VALUES ");
            foreach (string tagName in additionalTagNameList)
            {
                queryBuilder.Append("('");
                queryBuilder.Append(pgnGameID.ToString());
                queryBuilder.Append("', '");
                queryBuilder.Append(tagName);
                queryBuilder.Append("', '");
                queryBuilder.Append(pgnGame.TagPairs.GetTagValueByName(tagName).Replace("'", "''"));
                queryBuilder.Append("'),");
            }
            this.ExecuteNonQuery(queryBuilder.Remove(queryBuilder.Length - 1, 1).ToString());
            if (withIndexing)
                IndexPGNGameByID(pgnGameID, pgnGame);
        }

        public PGNTagPairs[] GetAllTagPairs()
        {
            const string selectAllTagPairsQuery = "SELECT [id], [event], [site], [date], [round], [white], [black], [result] FROM [ChessGames]";

            SqlCeDataReader dataReader = this.ExecuteReader(selectAllTagPairsQuery);
            List<PGNTagPairs> resultTagPairsList = new List<PGNTagPairs>();
            while (dataReader.Read())
            {
                PGNTagPairs tagPairs = new PGNTagPairs();
                tagPairs.AddTagPair("Event", (string)dataReader[1]);
                tagPairs.AddTagPair("Site", (string)dataReader[2]);
                tagPairs.AddTagPair("Date", (string)dataReader[3]);
                tagPairs.AddTagPair("Round", (string)dataReader[4]);
                tagPairs.AddTagPair("White", (string)dataReader[5]);
                tagPairs.AddTagPair("Black", (string)dataReader[6]);
                tagPairs.AddTagPair("Result", (string)dataReader[7]);

                resultTagPairsList.Add(tagPairs);
            }
            dataReader.Close();

            return resultTagPairsList.ToArray();
        }

        public string[] GetAllEvents()
        {
            const string selectAllEventsQuery = "SELECT DISTINCT [event] FROM [ChessGames]";

            SqlCeDataReader dataReader = this.ExecuteReader(selectAllEventsQuery);
            List<string> resultEventList = new List<string>();
            while (dataReader.Read())
            {
                resultEventList.Add((string)dataReader[0]);
            }
            dataReader.Close();

            return resultEventList.ToArray();
        }

        public void IndexPGNGameByID(Int64 pgnGameID)
        {
            IndexPGNGameByID(pgnGameID, GetPGNGameByID(pgnGameID));
        }

        protected void IndexPGNGameByID(Int64 pgnGameID, PGNGameRecord pgnGame)
        {
            ChessGameBuilder builder = new ChessGameBuilder(pgnGame);
            builder.BuildGame();
            IGame game = builder.Game;
            while (game.MovementHistory.Position > 0)
                game.MovementHistory.Undo();
            
            SqlCeCommand sqlCommand = sqlConnection.CreateCommand();
            sqlCommand.CommandText = "INSERT INTO [ChessGameMoves] ([chess_game_id], [move_number], [chessboard_mask], [fen_string]) VALUES (@game_id,@move_number,@bitmask,@fen_data)";

            var game_id = sqlCommand.CreateParameter();
            game_id.ParameterName = "@game_id";
            sqlCommand.Parameters.Add(game_id);

            var move_number = sqlCommand.CreateParameter();
            move_number.ParameterName = "@move_number";
            sqlCommand.Parameters.Add(move_number);

            var bitmask = sqlCommand.CreateParameter();
            bitmask.ParameterName = "@bitmask";
            sqlCommand.Parameters.Add(bitmask);

            var fen_data = sqlCommand.CreateParameter();
            fen_data.ParameterName = "@fen_data";
            sqlCommand.Parameters.Add(fen_data);

            while (game.MovementHistory.Position < game.MovementHistory.Count)
            {
                game.MovementHistory.Redo();

                game_id.Value = pgnGameID.ToString();
                move_number.Value = game.MovementHistory.Position.ToString();
                bitmask.Value = game.Chessboard.GetBitmask().ToString();
                fen_data.Value = game.GetFENData().ToString();

                sqlCommand.ExecuteNonQuery();
            }

            sqlCommand.Dispose();
        }

        public void OpenConnectionIfClosed()
        {
            if (sqlConnection.State != ConnectionState.Open)
                sqlConnection.Open();
        }

        protected void ExecuteNonQuery(string sqlCommandText)
        {
            SqlCeCommand sqlCommand = new SqlCeCommand(sqlCommandText, sqlConnection);
            sqlCommand.ExecuteNonQuery();
            sqlCommand.Dispose();
        }

        protected SqlCeDataReader ExecuteReader(string sqlCommandText)
        {
            SqlCeDataReader resultDataReader = null;

            SqlCeCommand sqlCommand = new SqlCeCommand(sqlCommandText, sqlConnection);
            resultDataReader = sqlCommand.ExecuteReader();
            sqlCommand.Dispose();
            
            return resultDataReader;
        }

        public void CloseConnection()
        {
            sqlConnection.Close();
        }

        public PGNTagPairs GetTagPairsByID(Int64 id)
        {
            SqlCeDataReader dataReader = this.ExecuteReader("SELECT [id], [event], [site], [date], [round], [white], [black], [result] FROM [ChessGames] WHERE [id]='" + id.ToString() + "'");
            PGNTagPairs resultTagPairs = new PGNTagPairs();
            if (dataReader.Read())
            {
                resultTagPairs.AddTagPair("Event", (string)dataReader[1]);
                resultTagPairs.AddTagPair("Site", (string)dataReader[2]);
                resultTagPairs.AddTagPair("Date", (string)dataReader[3]);
                resultTagPairs.AddTagPair("Round", (string)dataReader[4]);
                resultTagPairs.AddTagPair("White", (string)dataReader[5]);
                resultTagPairs.AddTagPair("Black", (string)dataReader[6]);
                resultTagPairs.AddTagPair("Result", (string)dataReader[7]);
            }
            dataReader.Close();

            return resultTagPairs;
        }

        public PGNTagPairs GetAdditionalTagPairsByID(Int64 id)
        {
            SqlCeDataReader dataReader = this.ExecuteReader("SELECT [name], [value] FROM [AdditionalTags] WHERE [chess_game_id]='" + id.ToString() + "'");
            PGNTagPairs resultTagPairs = new PGNTagPairs();
            while (dataReader.Read())
            {
                resultTagPairs.AddTagPair((string)dataReader[0], (string)dataReader[1]);
            }
            dataReader.Close();

            return resultTagPairs;
        }

        public PGNMovetext GetMovetextByID(Int64 id)
        {
            string query = "SELECT [movetext] FROM [ChessGames] WHERE [id]='" + id.ToString() + "'";

            SqlCeDataReader dataReader = this.ExecuteReader(query);
            PGNMovetext resultMovetext = new PGNMovetext();

            if (dataReader.Read())
            {
                resultMovetext.Append(PGNMovetext.CreateFromString(dataReader[0].ToString()));
            }
            dataReader.Close();

            return resultMovetext;
        }

        public string[] GetAllWhitePlayers()
        {
            const string selectAllWhitePlayersQuery = "SELECT DISTINCT [white] FROM [ChessGames]";

            SqlCeDataReader dataReader = this.ExecuteReader(selectAllWhitePlayersQuery);
            List<string> resultWhitePlayersList = new List<string>();
            while (dataReader.Read())
            {
                resultWhitePlayersList.Add((string)dataReader[0]);
            }
            dataReader.Close();

            return resultWhitePlayersList.ToArray();
        }

        public string[] GetAllBlackPlayers()
        {
            const string selectAllBlackPlayersQuery = "SELECT DISTINCT [black] FROM [ChessGames]";

            SqlCeDataReader dataReader = this.ExecuteReader(selectAllBlackPlayersQuery);
            List<string> resultBlackPlayersList = new List<string>();
            while (dataReader.Read())
            {
                resultBlackPlayersList.Add((string)dataReader[0]);
            }
            dataReader.Close();

            return resultBlackPlayersList.ToArray();
        }

        public static SqlCeConnection GetSQLConnection()
        {
            InitializeSQLConnection();

            return sqlConnection;
        }

        public string[] GetAllSites()
        {
            const string selectAllSitesQuery = "SELECT DISTINCT [site] FROM [ChessGames]";

            SqlCeDataReader dataReader = this.ExecuteReader(selectAllSitesQuery);
            List<string> resultSiteList = new List<string>();
            while (dataReader.Read())
            {
                resultSiteList.Add((string)dataReader[0]);
            }
            dataReader.Close();

            return resultSiteList.ToArray();
        }

        public PGNGameRecord GetPGNGameByID(long id)
        {
            PGNTagPairs tagPairs = GetTagPairsByID(id);
            tagPairs.Append(GetAdditionalTagPairsByID(id));

            PGNMovetext movetext = GetMovetextByID(id);

            return new PGNGameRecord(tagPairs, movetext);
        }
    }
}
