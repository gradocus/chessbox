﻿namespace ChessBox
{
    partial class ChessTournamentsViewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.играToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.close_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox_chessGame = new System.Windows.Forms.PictureBox();
            this.button_GoToStartingPosition = new System.Windows.Forms.Button();
            this.button_GoToFinalPosition = new System.Windows.Forms.Button();
            this.button_GoForwardMove = new System.Windows.Forms.Button();
            this.button_GoBackMove = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label_gameHeader = new System.Windows.Forms.Label();
            this.moveNumber_textBox = new System.Windows.Forms.TextBox();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.tagPairs_groupBox = new System.Windows.Forms.GroupBox();
            this.roundValue_label = new System.Windows.Forms.Label();
            this.round_label = new System.Windows.Forms.Label();
            this.eventValue_label = new System.Windows.Forms.Label();
            this.resultValue_label = new System.Windows.Forms.Label();
            this.blackValue_label = new System.Windows.Forms.Label();
            this.whiteValue_label = new System.Windows.Forms.Label();
            this.dateValue_label = new System.Windows.Forms.Label();
            this.siteValue_label = new System.Windows.Forms.Label();
            this.result_label = new System.Windows.Forms.Label();
            this.black_label = new System.Windows.Forms.Label();
            this.white_label = new System.Windows.Forms.Label();
            this.date_label = new System.Windows.Forms.Label();
            this.site_label = new System.Windows.Forms.Label();
            this.event_label = new System.Windows.Forms.Label();
            this.movetext_listBox = new System.Windows.Forms.ListBox();
            this.contextMenuMoveHistory = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.показатьИсториюХодовToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.показатьСтатистикуДляДанногоСостоянияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_chessGame)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.tagPairs_groupBox.SuspendLayout();
            this.contextMenuMoveHistory.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.играToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(794, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip";
            // 
            // играToolStripMenuItem
            // 
            this.играToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.close_ToolStripMenuItem});
            this.играToolStripMenuItem.Name = "играToolStripMenuItem";
            this.играToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.играToolStripMenuItem.Text = "Партия";
            // 
            // close_ToolStripMenuItem
            // 
            this.close_ToolStripMenuItem.Name = "close_ToolStripMenuItem";
            this.close_ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.close_ToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.close_ToolStripMenuItem.Text = "Закрыть";
            this.close_ToolStripMenuItem.Click += new System.EventHandler(this.Close_ToolStripMenuItem_Click);
            // 
            // pictureBox_chessGame
            // 
            this.pictureBox_chessGame.Location = new System.Drawing.Point(3, 29);
            this.pictureBox_chessGame.MinimumSize = new System.Drawing.Size(470, 470);
            this.pictureBox_chessGame.Name = "pictureBox_chessGame";
            this.pictureBox_chessGame.Size = new System.Drawing.Size(470, 470);
            this.pictureBox_chessGame.TabIndex = 2;
            this.pictureBox_chessGame.TabStop = false;
            this.pictureBox_chessGame.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox_chessGame_MouseMove);
            this.pictureBox_chessGame.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox_chessGame_MouseUp);
            // 
            // button_GoToStartingPosition
            // 
            this.button_GoToStartingPosition.Location = new System.Drawing.Point(12, 513);
            this.button_GoToStartingPosition.Name = "button_GoToStartingPosition";
            this.button_GoToStartingPosition.Size = new System.Drawing.Size(75, 23);
            this.button_GoToStartingPosition.TabIndex = 3;
            this.button_GoToStartingPosition.Text = "<<";
            this.button_GoToStartingPosition.UseVisualStyleBackColor = true;
            this.button_GoToStartingPosition.Click += new System.EventHandler(this.button_GoToStartingPosition_Click);
            // 
            // button_GoToFinalPosition
            // 
            this.button_GoToFinalPosition.Location = new System.Drawing.Point(388, 513);
            this.button_GoToFinalPosition.Name = "button_GoToFinalPosition";
            this.button_GoToFinalPosition.Size = new System.Drawing.Size(75, 23);
            this.button_GoToFinalPosition.TabIndex = 3;
            this.button_GoToFinalPosition.Text = ">>";
            this.button_GoToFinalPosition.UseVisualStyleBackColor = true;
            this.button_GoToFinalPosition.Click += new System.EventHandler(this.button_GoToFinalPosition_Click);
            // 
            // button_GoForwardMove
            // 
            this.button_GoForwardMove.Location = new System.Drawing.Point(307, 513);
            this.button_GoForwardMove.Name = "button_GoForwardMove";
            this.button_GoForwardMove.Size = new System.Drawing.Size(75, 23);
            this.button_GoForwardMove.TabIndex = 3;
            this.button_GoForwardMove.Text = ">";
            this.button_GoForwardMove.UseVisualStyleBackColor = true;
            this.button_GoForwardMove.Click += new System.EventHandler(this.button_GoForwardMove_Click);
            // 
            // button_GoBackMove
            // 
            this.button_GoBackMove.Location = new System.Drawing.Point(93, 513);
            this.button_GoBackMove.Name = "button_GoBackMove";
            this.button_GoBackMove.Size = new System.Drawing.Size(75, 23);
            this.button_GoBackMove.TabIndex = 3;
            this.button_GoBackMove.Text = "<";
            this.button_GoBackMove.UseVisualStyleBackColor = true;
            this.button_GoBackMove.Click += new System.EventHandler(this.button_GoBackMove_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(0, 0);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 0;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.label_gameHeader);
            this.splitContainer1.Panel1.Controls.Add(this.moveNumber_textBox);
            this.splitContainer1.Panel1.Controls.Add(this.pictureBox_chessGame);
            this.splitContainer1.Panel1.Controls.Add(this.button_GoForwardMove);
            this.splitContainer1.Panel1.Controls.Add(this.button_GoToFinalPosition);
            this.splitContainer1.Panel1.Controls.Add(this.button_GoToStartingPosition);
            this.splitContainer1.Panel1.Controls.Add(this.button_GoBackMove);
            this.splitContainer1.Panel1MinSize = 470;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(794, 548);
            this.splitContainer1.SplitterDistance = 476;
            this.splitContainer1.TabIndex = 6;
            this.splitContainer1.MouseEnter += new System.EventHandler(this.splitContainer1_MouseEnter);
            // 
            // label_gameHeader
            // 
            this.label_gameHeader.AutoSize = true;
            this.label_gameHeader.Location = new System.Drawing.Point(7, 9);
            this.label_gameHeader.Name = "label_gameHeader";
            this.label_gameHeader.Size = new System.Drawing.Size(178, 13);
            this.label_gameHeader.TabIndex = 6;
            this.label_gameHeader.Text = "Event: White player vs. Black player";
            // 
            // moveNumber_textBox
            // 
            this.moveNumber_textBox.Location = new System.Drawing.Point(188, 515);
            this.moveNumber_textBox.Name = "moveNumber_textBox";
            this.moveNumber_textBox.ReadOnly = true;
            this.moveNumber_textBox.Size = new System.Drawing.Size(100, 20);
            this.moveNumber_textBox.TabIndex = 4;
            this.moveNumber_textBox.Text = "0";
            this.moveNumber_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.tagPairs_groupBox);
            this.splitContainer2.Panel1.Padding = new System.Windows.Forms.Padding(3);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.movetext_listBox);
            this.splitContainer2.Panel2.Padding = new System.Windows.Forms.Padding(5);
            this.splitContainer2.Size = new System.Drawing.Size(314, 548);
            this.splitContainer2.SplitterDistance = 180;
            this.splitContainer2.TabIndex = 0;
            // 
            // tagPairs_groupBox
            // 
            this.tagPairs_groupBox.Controls.Add(this.roundValue_label);
            this.tagPairs_groupBox.Controls.Add(this.round_label);
            this.tagPairs_groupBox.Controls.Add(this.eventValue_label);
            this.tagPairs_groupBox.Controls.Add(this.resultValue_label);
            this.tagPairs_groupBox.Controls.Add(this.blackValue_label);
            this.tagPairs_groupBox.Controls.Add(this.whiteValue_label);
            this.tagPairs_groupBox.Controls.Add(this.dateValue_label);
            this.tagPairs_groupBox.Controls.Add(this.siteValue_label);
            this.tagPairs_groupBox.Controls.Add(this.result_label);
            this.tagPairs_groupBox.Controls.Add(this.black_label);
            this.tagPairs_groupBox.Controls.Add(this.white_label);
            this.tagPairs_groupBox.Controls.Add(this.date_label);
            this.tagPairs_groupBox.Controls.Add(this.site_label);
            this.tagPairs_groupBox.Controls.Add(this.event_label);
            this.tagPairs_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tagPairs_groupBox.Location = new System.Drawing.Point(3, 3);
            this.tagPairs_groupBox.Name = "tagPairs_groupBox";
            this.tagPairs_groupBox.Size = new System.Drawing.Size(308, 174);
            this.tagPairs_groupBox.TabIndex = 0;
            this.tagPairs_groupBox.TabStop = false;
            this.tagPairs_groupBox.Text = "Информация о шахматной партии";
            // 
            // roundValue_label
            // 
            this.roundValue_label.AutoSize = true;
            this.roundValue_label.Location = new System.Drawing.Point(236, 75);
            this.roundValue_label.Name = "roundValue_label";
            this.roundValue_label.Size = new System.Drawing.Size(10, 13);
            this.roundValue_label.TabIndex = 7;
            this.roundValue_label.Text = "-";
            // 
            // round_label
            // 
            this.round_label.AutoSize = true;
            this.round_label.Location = new System.Drawing.Point(202, 75);
            this.round_label.Name = "round_label";
            this.round_label.Size = new System.Drawing.Size(28, 13);
            this.round_label.TabIndex = 6;
            this.round_label.Text = "Тур:";
            // 
            // eventValue_label
            // 
            this.eventValue_label.AutoSize = true;
            this.eventValue_label.Location = new System.Drawing.Point(117, 25);
            this.eventValue_label.Name = "eventValue_label";
            this.eventValue_label.Size = new System.Drawing.Size(10, 13);
            this.eventValue_label.TabIndex = 5;
            this.eventValue_label.Text = "-";
            // 
            // resultValue_label
            // 
            this.resultValue_label.AutoSize = true;
            this.resultValue_label.Location = new System.Drawing.Point(152, 150);
            this.resultValue_label.Name = "resultValue_label";
            this.resultValue_label.Size = new System.Drawing.Size(10, 13);
            this.resultValue_label.TabIndex = 5;
            this.resultValue_label.Text = "-";
            // 
            // blackValue_label
            // 
            this.blackValue_label.AutoSize = true;
            this.blackValue_label.Location = new System.Drawing.Point(117, 125);
            this.blackValue_label.Name = "blackValue_label";
            this.blackValue_label.Size = new System.Drawing.Size(10, 13);
            this.blackValue_label.TabIndex = 5;
            this.blackValue_label.Text = "-";
            // 
            // whiteValue_label
            // 
            this.whiteValue_label.AutoSize = true;
            this.whiteValue_label.Location = new System.Drawing.Point(117, 100);
            this.whiteValue_label.Name = "whiteValue_label";
            this.whiteValue_label.Size = new System.Drawing.Size(10, 13);
            this.whiteValue_label.TabIndex = 5;
            this.whiteValue_label.Text = "-";
            // 
            // dateValue_label
            // 
            this.dateValue_label.AutoSize = true;
            this.dateValue_label.Location = new System.Drawing.Point(117, 75);
            this.dateValue_label.Name = "dateValue_label";
            this.dateValue_label.Size = new System.Drawing.Size(10, 13);
            this.dateValue_label.TabIndex = 5;
            this.dateValue_label.Text = "-";
            // 
            // siteValue_label
            // 
            this.siteValue_label.AutoSize = true;
            this.siteValue_label.Location = new System.Drawing.Point(117, 50);
            this.siteValue_label.Name = "siteValue_label";
            this.siteValue_label.Size = new System.Drawing.Size(10, 13);
            this.siteValue_label.TabIndex = 5;
            this.siteValue_label.Text = "-";
            // 
            // result_label
            // 
            this.result_label.AutoSize = true;
            this.result_label.Location = new System.Drawing.Point(46, 150);
            this.result_label.Name = "result_label";
            this.result_label.Size = new System.Drawing.Size(100, 13);
            this.result_label.TabIndex = 4;
            this.result_label.Text = "Результат партии:";
            // 
            // black_label
            // 
            this.black_label.AutoSize = true;
            this.black_label.Location = new System.Drawing.Point(6, 125);
            this.black_label.Name = "black_label";
            this.black_label.Size = new System.Drawing.Size(99, 13);
            this.black_label.TabIndex = 3;
            this.black_label.Text = "За черных играет:";
            // 
            // white_label
            // 
            this.white_label.AutoSize = true;
            this.white_label.Location = new System.Drawing.Point(6, 100);
            this.white_label.Name = "white_label";
            this.white_label.Size = new System.Drawing.Size(94, 13);
            this.white_label.TabIndex = 3;
            this.white_label.Text = "За белых играет:";
            // 
            // date_label
            // 
            this.date_label.AutoSize = true;
            this.date_label.Location = new System.Drawing.Point(6, 75);
            this.date_label.Name = "date_label";
            this.date_label.Size = new System.Drawing.Size(99, 13);
            this.date_label.TabIndex = 2;
            this.date_label.Text = "Дата проведения:";
            // 
            // site_label
            // 
            this.site_label.AutoSize = true;
            this.site_label.Location = new System.Drawing.Point(6, 50);
            this.site_label.Name = "site_label";
            this.site_label.Size = new System.Drawing.Size(105, 13);
            this.site_label.TabIndex = 1;
            this.site_label.Text = "Место проведения:";
            // 
            // event_label
            // 
            this.event_label.AutoSize = true;
            this.event_label.Location = new System.Drawing.Point(6, 25);
            this.event_label.Name = "event_label";
            this.event_label.Size = new System.Drawing.Size(103, 13);
            this.event_label.TabIndex = 0;
            this.event_label.Text = "Название турнира:";
            // 
            // movetext_listBox
            // 
            this.movetext_listBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.movetext_listBox.FormattingEnabled = true;
            this.movetext_listBox.Location = new System.Drawing.Point(5, 5);
            this.movetext_listBox.Name = "movetext_listBox";
            this.movetext_listBox.Size = new System.Drawing.Size(304, 354);
            this.movetext_listBox.TabIndex = 0;
            this.movetext_listBox.SelectedIndexChanged += new System.EventHandler(this.movetext_listBox_SelectedIndexChanged);
            // 
            // contextMenuMoveHistory
            // 
            this.contextMenuMoveHistory.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.показатьИсториюХодовToolStripMenuItem,
            this.показатьСтатистикуДляДанногоСостоянияToolStripMenuItem});
            this.contextMenuMoveHistory.Name = "contextMenuMoveHistory";
            this.contextMenuMoveHistory.Size = new System.Drawing.Size(221, 70);
            // 
            // показатьИсториюХодовToolStripMenuItem
            // 
            this.показатьИсториюХодовToolStripMenuItem.Name = "показатьИсториюХодовToolStripMenuItem";
            this.показатьИсториюХодовToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.показатьИсториюХодовToolStripMenuItem.Text = "Показать историю ходов";
            this.показатьИсториюХодовToolStripMenuItem.Click += new System.EventHandler(this.показатьИсториюХодовToolStripMenuItem_Click);
            // 
            // показатьСтатистикуДляДанногоСостоянияToolStripMenuItem
            // 
            this.показатьСтатистикуДляДанногоСостоянияToolStripMenuItem.Name = "показатьСтатистикуДляДанногоСостоянияToolStripMenuItem";
            this.показатьСтатистикуДляДанногоСостоянияToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.показатьСтатистикуДляДанногоСостоянияToolStripMenuItem.Text = "Показать статистику ходов";
            this.показатьСтатистикуДляДанногоСостоянияToolStripMenuItem.Click += new System.EventHandler(this.показатьСтатистикуХодовToolStripMenuItem_Click);
            // 
            // ChessTournamentsViewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 572);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip;
            this.MaximizeBox = false;
            this.Name = "ChessTournamentsViewForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Проигрыватель шахматных партий";
            this.Load += new System.EventHandler(this.ChessTournamentsViewForm_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_chessGame)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.tagPairs_groupBox.ResumeLayout(false);
            this.tagPairs_groupBox.PerformLayout();
            this.contextMenuMoveHistory.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.PictureBox pictureBox_chessGame;
        private System.Windows.Forms.Button button_GoToStartingPosition;
        private System.Windows.Forms.Button button_GoToFinalPosition;
        private System.Windows.Forms.Button button_GoForwardMove;
        private System.Windows.Forms.Button button_GoBackMove;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TextBox moveNumber_textBox;
        private System.Windows.Forms.ToolStripMenuItem играToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem close_ToolStripMenuItem;
        private System.Windows.Forms.Label label_gameHeader;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.GroupBox tagPairs_groupBox;
        private System.Windows.Forms.ListBox movetext_listBox;
        private System.Windows.Forms.Label date_label;
        private System.Windows.Forms.Label site_label;
        private System.Windows.Forms.Label event_label;
        private System.Windows.Forms.Label result_label;
        private System.Windows.Forms.Label black_label;
        private System.Windows.Forms.Label white_label;
        private System.Windows.Forms.Label eventValue_label;
        private System.Windows.Forms.Label siteValue_label;
        private System.Windows.Forms.Label resultValue_label;
        private System.Windows.Forms.Label blackValue_label;
        private System.Windows.Forms.Label whiteValue_label;
        private System.Windows.Forms.Label dateValue_label;
        private System.Windows.Forms.Label roundValue_label;
        private System.Windows.Forms.Label round_label;
        private System.Windows.Forms.ContextMenuStrip contextMenuMoveHistory;
        private System.Windows.Forms.ToolStripMenuItem показатьИсториюХодовToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem показатьСтатистикуДляДанногоСостоянияToolStripMenuItem;
    }
}

