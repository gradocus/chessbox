﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Threading;
using System.Windows.Forms;
using ChessBox.Database;
using ChessboardGamesCore.IO.PGN;

namespace ChessBox
{
    public class ExportEventArgs
    {
        public ExportEventArgs(int tournametnNumber) { TournametnNumber = tournametnNumber; }
        public int TournametnNumber { get; private set; }
    }

    class PGNExporter
    {
        public PGNExporter(string filePath)
        {
            this.filePath = filePath;
        }

        public delegate void ExportEventHandler(object sender, ExportEventArgs e);

        public event ExportEventHandler OnTournamentExportEvent;
        protected virtual void TournamentExportEvent(int tournametnNumber)
        {
            if (OnTournamentExportEvent != null)
                OnTournamentExportEvent(this, new ExportEventArgs(tournametnNumber));
        }

        public event ExportEventHandler OnAllTournamentsExportEvent;
        protected virtual void AllTournamentsExportEvent(int tournametnNumber)
        {
            if (OnAllTournamentsExportEvent != null)
                OnAllTournamentsExportEvent(this, new ExportEventArgs(tournametnNumber));
        }

        private Thread exporterThread;

        public bool IsExporting { get; protected set; }
        private bool stopExporting = false;

        private string filePath = "";
        private Int64[] pgnGameID;

        public void ExportIntoPGNAsync(Int64[] pgnGameID)
        {
            if (!this.IsExporting)
            {
                this.pgnGameID = pgnGameID;
                this.exporterThread = new Thread(new ThreadStart(ExportFunction));
                this.stopExporting = false;
                this.exporterThread.Start();
            }
            else
            {
                throw new Exception("Something wrong!");
            }
        }

        public void ExportIntoPGN(Int64[] pgnGameID)
        {
            this.pgnGameID = pgnGameID;
        }

        protected void ExportFunction()
        {
            bool addGamesIntoEndExistingFile = false;
            int tournamentCount = 0;

            if (File.Exists(this.filePath))
            {
                StringBuilder messageStringBuilder = new StringBuilder("Файл \"");
                messageStringBuilder.Append(this.filePath);
                messageStringBuilder.AppendLine("\" уже существует!");
                messageStringBuilder.AppendLine("Добавить выбранные шахматные партии в конец файла или перезаписать его?");
                messageStringBuilder.AppendLine("* Да - добавить в конец.");
                messageStringBuilder.AppendLine("* Нет - перезаписать файл (Все данные, которые содержит файл, будут потеряны!).");

                if (MessageBox.Show(messageStringBuilder.ToString(), "File already exists", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    addGamesIntoEndExistingFile = true;
                }
            }

            StreamWriter streamWriter;
            try
            {
                streamWriter = new StreamWriter(this.filePath, addGamesIntoEndExistingFile);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            ChessTournamentsDataBase dataBase = new ChessTournamentsDataBase();
            
            this.IsExporting = true;
            dataBase.OpenConnectionIfClosed();
            for (int i = 0; !this.stopExporting && i < this.pgnGameID.Length; i++)
            {
                PGNGameRecord pgnGame = dataBase.GetPGNGameByID(this.pgnGameID[i]);
                streamWriter.WriteLine();
                streamWriter.WriteLine(pgnGame.TagPairs.ToString());
                streamWriter.WriteLine(pgnGame.Movetext.ToString());
                
                if (!this.stopExporting)
                    TournamentExportEvent(++tournamentCount);
            }
            streamWriter.Close();

            if (!this.stopExporting)
                AllTournamentsExportEvent(tournamentCount);

            this.IsExporting = false;
        }

        public void StopExporting()
        {
            this.stopExporting = true;
        }
    }
}
