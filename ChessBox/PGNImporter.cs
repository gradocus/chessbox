﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ChessBox.Database;
using System.Threading;
using System.IO;
using System.Windows.Forms;
using ChessboardGamesCore.IO.PGN;

namespace ChessBox
{
    public class ImportEventArgs
    {
        public ImportEventArgs(int tournametnNumber) { TournametnNumber = tournametnNumber; }
        public int TournametnNumber { get; private set; }
    }

    class PGNImporter
    {
        const int averageTournamentDataSize = 630;

        public PGNImporter(string filePath) : this(filePath, false) { }

        public PGNImporter(string filePath, bool withIndexing)
        {
            this.dataBase = new ChessTournamentsDataBase();

            if (!File.Exists(filePath))
                throw new Exception("File \"" + filePath + "\" not found.");

            this.filePath = filePath;
            this.withIndexing = withIndexing;
        }

        public delegate void ImportEventHandler(object sender, ImportEventArgs e);

        public event ImportEventHandler OnTournamentImportEvent;
        protected virtual void TournamentImportEvent(int tournametnNumber)
        {
            if (OnTournamentImportEvent != null)
                OnTournamentImportEvent(this, new ImportEventArgs(tournametnNumber));
        }

        public event ImportEventHandler OnAllTournamentsImportEvent;
        protected virtual void AllTournamentsImportEvent(int tournametnNumber)
        {
            if (OnAllTournamentsImportEvent != null)
                OnAllTournamentsImportEvent(this, new ImportEventArgs(tournametnNumber));
        }

        private Thread importerThread;
        private bool withIndexing;
        public bool IsImporting { get; protected set; }
        private bool stopImporting = false;

        ChessTournamentsDataBase dataBase;
        string filePath = "";

        public int GetApproximateTournamentNumber()
        {
            int approximateTournamentNumber = 0;

            if (File.Exists(this.filePath))
            {
                approximateTournamentNumber = (int)((new FileInfo(this.filePath)).Length / averageTournamentDataSize);

                if (approximateTournamentNumber == 0)
                    approximateTournamentNumber = 1;
            }

            return approximateTournamentNumber;
        }

        public void ImportFromPGNAsync()
        {
            if (!this.IsImporting)
            {
                this.importerThread = new Thread(new ThreadStart(ImportFromPGN));
                this.stopImporting = false;
                this.importerThread.Start();
            }
            else
            {
                throw new Exception("Something wrong!");
            }
        }

        public void ImportFromPGN()
        {
            this.IsImporting = true;
            
            PGNGameReader pgnReader = new PGNGameReader(this.filePath);
            int tournamentCount = 0;
            dataBase.OpenConnectionIfClosed();
            while (!this.stopImporting && pgnReader.ReadNextGame())
            {
                try
                {
                    dataBase.AddPGNGame(pgnReader.GameRecord, this.withIndexing);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ошибка обработки партии (" + (tournamentCount + 1).ToString() + "):"
                        + ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                if (!this.stopImporting)
                    TournamentImportEvent(++tournamentCount);
            }
            if (!this.stopImporting)
                AllTournamentsImportEvent(tournamentCount);
            
            this.IsImporting = false;
        }

        public void StopImporting()
        {
            stopImporting = true;
        }
    }
}
