﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlServerCe;
using ChessboardGamesCore;
using System.Threading;
using ChessBox.Database;
using ChessBox.ChessGameCoreExtensions;
using ChessboardGamesCore.IO.PGN;
using ChessboardGamesCore.IO.FEN;

namespace ChessBox
{
    public partial class StaticticForm : Form
    {
        public StaticticForm(IGame game)
        {
            InitializeComponent();

            this.game = game;
        }

        IGame game;

        SqlCeDataAdapter chessTournamentsTableAdapter;
        DataTable chessTournamentsTable;
        Filter dataFilter = new Filter();
        //ChessTournamentsDataBase dataBase;

        private void StaticticForm_Load(object sender, EventArgs e)
        {
            FENData fenData = new FENData();
            fenData.PiecePlacement = game.Chessboard.GetFENPiecePlacement();
            this.chessTournamentsTableAdapter = new SqlCeDataAdapter(
                        "SELECT count([id]) as Games, [result] as Results FROM [ChessGames], [ChessGameMoves]"
                        + " WHERE [ChessGames].[id]=[ChessGameMoves].[chess_game_id]"
                        + " AND [chessboard_mask]='" + game.Chessboard.GetBitmask().ToString() + "' AND [fen_string]='" + game.GetFENData().ToString() + "'"
                        + " GROUP BY [ChessGames].[result];",
                    ChessTournamentsDataBase.GetSQLConnection()
                );
            this.chessTournamentsTable = new DataTable();

            UpdateDataGridView();
        }

        private void UpdateDataGridView()
        {
            this.chessTournamentsTable = new DataTable();
            this.chessTournamentsTableAdapter.Fill(this.chessTournamentsTable);

            BindingSource bindingSource = new BindingSource();
            bindingSource.DataSource = chessTournamentsTable;
            statictic_dataGridView.DataSource = bindingSource;
            bindingSource.Filter = this.dataFilter.ToString();
            statictic_dataGridView.ClearSelection();
        }
    }
}
