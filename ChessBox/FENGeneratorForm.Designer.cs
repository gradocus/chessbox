﻿namespace ChessBox
{
    partial class FENGeneratorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.cancel_button = new System.Windows.Forms.Button();
            this.ok_button = new System.Windows.Forms.Button();
            this.nextMoveColor_groupBox = new System.Windows.Forms.GroupBox();
            this.blackColor_radioButton = new System.Windows.Forms.RadioButton();
            this.whiteColor_radioButton = new System.Windows.Forms.RadioButton();
            this.pieceSelection_groupBox = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.clearChessboard_button = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.chessboard_pictureBox = new System.Windows.Forms.PictureBox();
            this.clearCell_pictureBox = new System.Windows.Forms.PictureBox();
            this.whitePawn_pictureBox = new System.Windows.Forms.PictureBox();
            this.blackKing_pictureBox = new System.Windows.Forms.PictureBox();
            this.blackQueen_pictureBox = new System.Windows.Forms.PictureBox();
            this.blackKnight_pictureBox = new System.Windows.Forms.PictureBox();
            this.blackBishop_pictureBox = new System.Windows.Forms.PictureBox();
            this.blackRook_pictureBox = new System.Windows.Forms.PictureBox();
            this.blackPawn_pictureBox = new System.Windows.Forms.PictureBox();
            this.whiteQueen_pictureBox = new System.Windows.Forms.PictureBox();
            this.whiteRook_pictureBox = new System.Windows.Forms.PictureBox();
            this.whiteKing_pictureBox = new System.Windows.Forms.PictureBox();
            this.whiteBishop_pictureBox = new System.Windows.Forms.PictureBox();
            this.whiteKnight_pictureBox = new System.Windows.Forms.PictureBox();
            this.selectPiece_pictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.nextMoveColor_groupBox.SuspendLayout();
            this.pieceSelection_groupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chessboard_pictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clearCell_pictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.whitePawn_pictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blackKing_pictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blackQueen_pictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blackKnight_pictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blackBishop_pictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blackRook_pictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blackPawn_pictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.whiteQueen_pictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.whiteRook_pictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.whiteKing_pictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.whiteBishop_pictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.whiteKnight_pictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectPiece_pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.chessboard_pictureBox);
            this.splitContainer1.Panel1.Padding = new System.Windows.Forms.Padding(1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.cancel_button);
            this.splitContainer1.Panel2.Controls.Add(this.ok_button);
            this.splitContainer1.Panel2.Controls.Add(this.nextMoveColor_groupBox);
            this.splitContainer1.Panel2.Controls.Add(this.pieceSelection_groupBox);
            this.splitContainer1.Panel2.Controls.Add(this.clearChessboard_button);
            this.splitContainer1.Size = new System.Drawing.Size(765, 472);
            this.splitContainer1.SplitterDistance = 472;
            this.splitContainer1.TabIndex = 0;
            // 
            // cancel_button
            // 
            this.cancel_button.Location = new System.Drawing.Point(187, 444);
            this.cancel_button.Name = "cancel_button";
            this.cancel_button.Size = new System.Drawing.Size(90, 23);
            this.cancel_button.TabIndex = 4;
            this.cancel_button.Text = "Отмена";
            this.cancel_button.UseVisualStyleBackColor = true;
            this.cancel_button.Click += new System.EventHandler(this.cancel_button_Click);
            // 
            // ok_button
            // 
            this.ok_button.Location = new System.Drawing.Point(6, 444);
            this.ok_button.Name = "ok_button";
            this.ok_button.Size = new System.Drawing.Size(90, 23);
            this.ok_button.TabIndex = 3;
            this.ok_button.Text = "OK";
            this.ok_button.UseVisualStyleBackColor = true;
            this.ok_button.Click += new System.EventHandler(this.ok_button_Click);
            // 
            // nextMoveColor_groupBox
            // 
            this.nextMoveColor_groupBox.Controls.Add(this.blackColor_radioButton);
            this.nextMoveColor_groupBox.Controls.Add(this.whiteColor_radioButton);
            this.nextMoveColor_groupBox.Location = new System.Drawing.Point(6, 361);
            this.nextMoveColor_groupBox.Name = "nextMoveColor_groupBox";
            this.nextMoveColor_groupBox.Size = new System.Drawing.Size(271, 77);
            this.nextMoveColor_groupBox.TabIndex = 2;
            this.nextMoveColor_groupBox.TabStop = false;
            this.nextMoveColor_groupBox.Text = "Следующими ходят";
            // 
            // blackColor_radioButton
            // 
            this.blackColor_radioButton.AutoSize = true;
            this.blackColor_radioButton.Location = new System.Drawing.Point(7, 48);
            this.blackColor_radioButton.Name = "blackColor_radioButton";
            this.blackColor_radioButton.Size = new System.Drawing.Size(65, 17);
            this.blackColor_radioButton.TabIndex = 0;
            this.blackColor_radioButton.Text = "Черные";
            this.blackColor_radioButton.UseVisualStyleBackColor = true;
            // 
            // whiteColor_radioButton
            // 
            this.whiteColor_radioButton.AutoSize = true;
            this.whiteColor_radioButton.Location = new System.Drawing.Point(6, 25);
            this.whiteColor_radioButton.Name = "whiteColor_radioButton";
            this.whiteColor_radioButton.Size = new System.Drawing.Size(58, 17);
            this.whiteColor_radioButton.TabIndex = 0;
            this.whiteColor_radioButton.Text = "Белые";
            this.whiteColor_radioButton.UseVisualStyleBackColor = true;
            // 
            // pieceSelection_groupBox
            // 
            this.pieceSelection_groupBox.Controls.Add(this.clearCell_pictureBox);
            this.pieceSelection_groupBox.Controls.Add(this.whitePawn_pictureBox);
            this.pieceSelection_groupBox.Controls.Add(this.label7);
            this.pieceSelection_groupBox.Controls.Add(this.label6);
            this.pieceSelection_groupBox.Controls.Add(this.label5);
            this.pieceSelection_groupBox.Controls.Add(this.label4);
            this.pieceSelection_groupBox.Controls.Add(this.label3);
            this.pieceSelection_groupBox.Controls.Add(this.label2);
            this.pieceSelection_groupBox.Controls.Add(this.label8);
            this.pieceSelection_groupBox.Controls.Add(this.label1);
            this.pieceSelection_groupBox.Controls.Add(this.blackKing_pictureBox);
            this.pieceSelection_groupBox.Controls.Add(this.blackQueen_pictureBox);
            this.pieceSelection_groupBox.Controls.Add(this.blackKnight_pictureBox);
            this.pieceSelection_groupBox.Controls.Add(this.blackBishop_pictureBox);
            this.pieceSelection_groupBox.Controls.Add(this.blackRook_pictureBox);
            this.pieceSelection_groupBox.Controls.Add(this.blackPawn_pictureBox);
            this.pieceSelection_groupBox.Controls.Add(this.whiteQueen_pictureBox);
            this.pieceSelection_groupBox.Controls.Add(this.whiteRook_pictureBox);
            this.pieceSelection_groupBox.Controls.Add(this.whiteKing_pictureBox);
            this.pieceSelection_groupBox.Controls.Add(this.whiteBishop_pictureBox);
            this.pieceSelection_groupBox.Controls.Add(this.whiteKnight_pictureBox);
            this.pieceSelection_groupBox.Controls.Add(this.selectPiece_pictureBox);
            this.pieceSelection_groupBox.Location = new System.Drawing.Point(6, 41);
            this.pieceSelection_groupBox.Name = "pieceSelection_groupBox";
            this.pieceSelection_groupBox.Size = new System.Drawing.Size(271, 314);
            this.pieceSelection_groupBox.TabIndex = 1;
            this.pieceSelection_groupBox.TabStop = false;
            this.pieceSelection_groupBox.Text = "Выбор шахматной фигуры";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(211, 225);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Король";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(148, 225);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Ферзь";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(88, 225);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Слон";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Enabled = false;
            this.label4.Location = new System.Drawing.Point(73, 151);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(190, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "-------------------------------------------------------------";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(217, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Конь";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(149, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Ладья";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(83, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Пешка";
            // 
            // clearChessboard_button
            // 
            this.clearChessboard_button.Location = new System.Drawing.Point(3, 12);
            this.clearChessboard_button.Name = "clearChessboard_button";
            this.clearChessboard_button.Size = new System.Drawing.Size(274, 23);
            this.clearChessboard_button.TabIndex = 0;
            this.clearChessboard_button.Text = "Очистить доску";
            this.clearChessboard_button.UseVisualStyleBackColor = true;
            this.clearChessboard_button.Click += new System.EventHandler(this.clearChessboard_button_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 77);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Очистить";
            // 
            // chessboard_pictureBox
            // 
            this.chessboard_pictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chessboard_pictureBox.Image = global::ChessBox.Properties.Resources.chessboard;
            this.chessboard_pictureBox.Location = new System.Drawing.Point(1, 1);
            this.chessboard_pictureBox.Name = "chessboard_pictureBox";
            this.chessboard_pictureBox.Size = new System.Drawing.Size(470, 470);
            this.chessboard_pictureBox.TabIndex = 0;
            this.chessboard_pictureBox.TabStop = false;
            this.chessboard_pictureBox.MouseLeave += new System.EventHandler(this.chessboard_pictureBox_MouseLeave);
            this.chessboard_pictureBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.chessboard_pictureBox_MouseMove);
            this.chessboard_pictureBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.chessboard_pictureBox_MouseUp);
            // 
            // clearCell_pictureBox
            // 
            this.clearCell_pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.clearCell_pictureBox.Image = global::ChessBox.Properties.Resources.clearCell;
            this.clearCell_pictureBox.Location = new System.Drawing.Point(9, 19);
            this.clearCell_pictureBox.Name = "clearCell_pictureBox";
            this.clearCell_pictureBox.Size = new System.Drawing.Size(55, 55);
            this.clearCell_pictureBox.TabIndex = 0;
            this.clearCell_pictureBox.TabStop = false;
            this.clearCell_pictureBox.Click += new System.EventHandler(this.clearCell_pictureBox_Click);
            // 
            // whitePawn_pictureBox
            // 
            this.whitePawn_pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.whitePawn_pictureBox.Image = global::ChessBox.Properties.Resources.whitePawn;
            this.whitePawn_pictureBox.Location = new System.Drawing.Point(76, 19);
            this.whitePawn_pictureBox.Name = "whitePawn_pictureBox";
            this.whitePawn_pictureBox.Size = new System.Drawing.Size(55, 55);
            this.whitePawn_pictureBox.TabIndex = 0;
            this.whitePawn_pictureBox.TabStop = false;
            this.whitePawn_pictureBox.Click += new System.EventHandler(this.whitePawn_pictureBox_Click);
            // 
            // blackKing_pictureBox
            // 
            this.blackKing_pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.blackKing_pictureBox.Image = global::ChessBox.Properties.Resources.blackKing;
            this.blackKing_pictureBox.Location = new System.Drawing.Point(206, 241);
            this.blackKing_pictureBox.Name = "blackKing_pictureBox";
            this.blackKing_pictureBox.Size = new System.Drawing.Size(55, 55);
            this.blackKing_pictureBox.TabIndex = 0;
            this.blackKing_pictureBox.TabStop = false;
            this.blackKing_pictureBox.Click += new System.EventHandler(this.blackKing_pictureBox_Click);
            // 
            // blackQueen_pictureBox
            // 
            this.blackQueen_pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.blackQueen_pictureBox.Image = global::ChessBox.Properties.Resources.blackQueen;
            this.blackQueen_pictureBox.Location = new System.Drawing.Point(141, 241);
            this.blackQueen_pictureBox.Name = "blackQueen_pictureBox";
            this.blackQueen_pictureBox.Size = new System.Drawing.Size(55, 55);
            this.blackQueen_pictureBox.TabIndex = 0;
            this.blackQueen_pictureBox.TabStop = false;
            this.blackQueen_pictureBox.Click += new System.EventHandler(this.blackQueen_pictureBox_Click);
            // 
            // blackKnight_pictureBox
            // 
            this.blackKnight_pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.blackKnight_pictureBox.Image = global::ChessBox.Properties.Resources.blackKnight;
            this.blackKnight_pictureBox.Location = new System.Drawing.Point(206, 93);
            this.blackKnight_pictureBox.Name = "blackKnight_pictureBox";
            this.blackKnight_pictureBox.Size = new System.Drawing.Size(55, 55);
            this.blackKnight_pictureBox.TabIndex = 0;
            this.blackKnight_pictureBox.TabStop = false;
            this.blackKnight_pictureBox.Click += new System.EventHandler(this.blackKnight_pictureBox_Click);
            // 
            // blackBishop_pictureBox
            // 
            this.blackBishop_pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.blackBishop_pictureBox.Image = global::ChessBox.Properties.Resources.blackBishop;
            this.blackBishop_pictureBox.Location = new System.Drawing.Point(76, 241);
            this.blackBishop_pictureBox.Name = "blackBishop_pictureBox";
            this.blackBishop_pictureBox.Size = new System.Drawing.Size(55, 55);
            this.blackBishop_pictureBox.TabIndex = 0;
            this.blackBishop_pictureBox.TabStop = false;
            this.blackBishop_pictureBox.Click += new System.EventHandler(this.blackBishop_pictureBox_Click);
            // 
            // blackRook_pictureBox
            // 
            this.blackRook_pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.blackRook_pictureBox.Image = global::ChessBox.Properties.Resources.blackRook;
            this.blackRook_pictureBox.Location = new System.Drawing.Point(141, 93);
            this.blackRook_pictureBox.Name = "blackRook_pictureBox";
            this.blackRook_pictureBox.Size = new System.Drawing.Size(55, 55);
            this.blackRook_pictureBox.TabIndex = 0;
            this.blackRook_pictureBox.TabStop = false;
            this.blackRook_pictureBox.Click += new System.EventHandler(this.blackRook_pictureBox_Click);
            // 
            // blackPawn_pictureBox
            // 
            this.blackPawn_pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.blackPawn_pictureBox.Image = global::ChessBox.Properties.Resources.blackPawn;
            this.blackPawn_pictureBox.Location = new System.Drawing.Point(76, 93);
            this.blackPawn_pictureBox.Name = "blackPawn_pictureBox";
            this.blackPawn_pictureBox.Size = new System.Drawing.Size(55, 55);
            this.blackPawn_pictureBox.TabIndex = 0;
            this.blackPawn_pictureBox.TabStop = false;
            this.blackPawn_pictureBox.Click += new System.EventHandler(this.blackPawn_pictureBox_Click);
            // 
            // whiteQueen_pictureBox
            // 
            this.whiteQueen_pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.whiteQueen_pictureBox.Image = global::ChessBox.Properties.Resources.whiteQueen;
            this.whiteQueen_pictureBox.Location = new System.Drawing.Point(141, 167);
            this.whiteQueen_pictureBox.Name = "whiteQueen_pictureBox";
            this.whiteQueen_pictureBox.Size = new System.Drawing.Size(55, 55);
            this.whiteQueen_pictureBox.TabIndex = 0;
            this.whiteQueen_pictureBox.TabStop = false;
            this.whiteQueen_pictureBox.Click += new System.EventHandler(this.whiteQueen_pictureBox_Click);
            // 
            // whiteRook_pictureBox
            // 
            this.whiteRook_pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.whiteRook_pictureBox.Image = global::ChessBox.Properties.Resources.whiteRook;
            this.whiteRook_pictureBox.Location = new System.Drawing.Point(141, 19);
            this.whiteRook_pictureBox.Name = "whiteRook_pictureBox";
            this.whiteRook_pictureBox.Size = new System.Drawing.Size(55, 55);
            this.whiteRook_pictureBox.TabIndex = 0;
            this.whiteRook_pictureBox.TabStop = false;
            this.whiteRook_pictureBox.Click += new System.EventHandler(this.whiteRook_pictureBox_Click);
            // 
            // whiteKing_pictureBox
            // 
            this.whiteKing_pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.whiteKing_pictureBox.Image = global::ChessBox.Properties.Resources.whiteKing;
            this.whiteKing_pictureBox.Location = new System.Drawing.Point(206, 167);
            this.whiteKing_pictureBox.Name = "whiteKing_pictureBox";
            this.whiteKing_pictureBox.Size = new System.Drawing.Size(55, 55);
            this.whiteKing_pictureBox.TabIndex = 0;
            this.whiteKing_pictureBox.TabStop = false;
            this.whiteKing_pictureBox.Click += new System.EventHandler(this.whiteKing_pictureBox_Click);
            // 
            // whiteBishop_pictureBox
            // 
            this.whiteBishop_pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.whiteBishop_pictureBox.Image = global::ChessBox.Properties.Resources.whiteBishop;
            this.whiteBishop_pictureBox.Location = new System.Drawing.Point(76, 167);
            this.whiteBishop_pictureBox.Name = "whiteBishop_pictureBox";
            this.whiteBishop_pictureBox.Size = new System.Drawing.Size(55, 55);
            this.whiteBishop_pictureBox.TabIndex = 0;
            this.whiteBishop_pictureBox.TabStop = false;
            this.whiteBishop_pictureBox.Click += new System.EventHandler(this.whiteBishop_pictureBox_Click);
            // 
            // whiteKnight_pictureBox
            // 
            this.whiteKnight_pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.whiteKnight_pictureBox.Image = global::ChessBox.Properties.Resources.whiteKnight;
            this.whiteKnight_pictureBox.Location = new System.Drawing.Point(206, 19);
            this.whiteKnight_pictureBox.Name = "whiteKnight_pictureBox";
            this.whiteKnight_pictureBox.Size = new System.Drawing.Size(55, 55);
            this.whiteKnight_pictureBox.TabIndex = 0;
            this.whiteKnight_pictureBox.TabStop = false;
            this.whiteKnight_pictureBox.Click += new System.EventHandler(this.whiteKnight_pictureBox_Click);
            // 
            // selectPiece_pictureBox
            // 
            this.selectPiece_pictureBox.BackColor = System.Drawing.Color.Chartreuse;
            this.selectPiece_pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.selectPiece_pictureBox.Location = new System.Drawing.Point(7, 17);
            this.selectPiece_pictureBox.Name = "selectPiece_pictureBox";
            this.selectPiece_pictureBox.Size = new System.Drawing.Size(59, 59);
            this.selectPiece_pictureBox.TabIndex = 0;
            this.selectPiece_pictureBox.TabStop = false;
            // 
            // FENGeneratorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(765, 472);
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FENGeneratorForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Генератор FEN";
            this.Load += new System.EventHandler(this.FENGeneratorForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.nextMoveColor_groupBox.ResumeLayout(false);
            this.nextMoveColor_groupBox.PerformLayout();
            this.pieceSelection_groupBox.ResumeLayout(false);
            this.pieceSelection_groupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chessboard_pictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clearCell_pictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.whitePawn_pictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blackKing_pictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blackQueen_pictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blackKnight_pictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blackBishop_pictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blackRook_pictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blackPawn_pictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.whiteQueen_pictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.whiteRook_pictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.whiteKing_pictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.whiteBishop_pictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.whiteKnight_pictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectPiece_pictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.PictureBox chessboard_pictureBox;
        private System.Windows.Forms.Button clearChessboard_button;
        private System.Windows.Forms.GroupBox pieceSelection_groupBox;
        private System.Windows.Forms.Button cancel_button;
        private System.Windows.Forms.Button ok_button;
        private System.Windows.Forms.GroupBox nextMoveColor_groupBox;
        private System.Windows.Forms.RadioButton blackColor_radioButton;
        private System.Windows.Forms.RadioButton whiteColor_radioButton;
        private System.Windows.Forms.PictureBox whitePawn_pictureBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox blackKing_pictureBox;
        private System.Windows.Forms.PictureBox blackQueen_pictureBox;
        private System.Windows.Forms.PictureBox blackKnight_pictureBox;
        private System.Windows.Forms.PictureBox blackBishop_pictureBox;
        private System.Windows.Forms.PictureBox blackRook_pictureBox;
        private System.Windows.Forms.PictureBox blackPawn_pictureBox;
        private System.Windows.Forms.PictureBox whiteQueen_pictureBox;
        private System.Windows.Forms.PictureBox whiteRook_pictureBox;
        private System.Windows.Forms.PictureBox whiteKing_pictureBox;
        private System.Windows.Forms.PictureBox whiteBishop_pictureBox;
        private System.Windows.Forms.PictureBox whiteKnight_pictureBox;
        private System.Windows.Forms.PictureBox selectPiece_pictureBox;
        private System.Windows.Forms.PictureBox clearCell_pictureBox;
        private System.Windows.Forms.Label label8;
    }
}