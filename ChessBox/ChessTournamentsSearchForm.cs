﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using ChessBox.Database;
using System.Data.SqlServerCe;
using ChessboardGamesCore;
using ChessboardGamesCore.IO.FEN;
using ChessboardGamesCore.IO.PGN;
using ChessBox.ChessGameCoreExtensions;

namespace ChessBox
{
    public partial class ChessTournamentsSearchForm : Form
    {
        public ChessTournamentsSearchForm()
        {
            this.dataBase = new ChessTournamentsDataBase();

            InitializeComponent();
        }

        SqlCeDataAdapter chessTournamentsTableAdapter;
        DataTable chessTournamentsTable;
        Filter dataFilter = new Filter();
        ChessTournamentsDataBase dataBase;
        bool searchPiecePlacement = false;

        private void ChessTournamentsSearchForm_Load(object sender, EventArgs e)
        {
            FENData fenData = new FENData();
            fenData.PiecePlacement = FENPiecePlacement.GetStartChessGamePiecePlacement();
            this.fenChessboardState_textBox.Text = fenData.ToString();

            this.chessTournamentsTableAdapter = new SqlCeDataAdapter("SELECT * FROM ChessGames", ChessTournamentsDataBase.GetSQLConnection());
            this.chessTournamentsTable = new DataTable();

            UpdateFiltersData();
            UpdateDataGridView();
        }

        private void ImportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PGNGamesImportForm importForm = new PGNGamesImportForm();
            importForm.ShowDialog();

            UpdateFiltersData();
            UpdateDataGridView();
        }

        private void exit_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void UpdateDataGridViewAsync()
        {
            this.statusLabel_toolStrip.Text = "Обновление списка партий, содержащихся в базе данных...";
            this.UseWaitCursor = true;

            Thread updateDataGridViewThread = new Thread(new ThreadStart(UpdateDataGridView));
            updateDataGridViewThread.Start();
        }

        private void UpdateDataGridView()
        {
            this.chessTournamentsTable = new DataTable();
            this.chessTournamentsTableAdapter.Fill(this.chessTournamentsTable);

            BindingSource bindingSource = new BindingSource();
            bindingSource.DataSource = chessTournamentsTable;
            chessGames_dataGridView.DataSource = bindingSource;
            bindingSource.Filter = this.dataFilter.ToString();
            chessGames_dataGridView.ClearSelection();
            showSelectedChessGame_button.Enabled = false;
            
            this.UseWaitCursor = false;
            this.statusLabel_toolStrip.Text = "Шахматных партий, удовлетворяющих условиям: " + this.chessGames_dataGridView.Rows.Count.ToString();
        }

        private void UpdateFiltersData()
        {
            this.dataBase.OpenConnectionIfClosed();
            string[] eventsArray = dataBase.GetAllEvents();
            this.event_toolStripComboBox.Items.Clear();
            this.event_toolStripComboBox.Items.AddRange(eventsArray);
            
            string[] whitePlayersArray = dataBase.GetAllWhitePlayers();
            this.whitePlayer_toolStripComboBox.Items.Clear();
            this.whitePlayer_toolStripComboBox.Items.AddRange(whitePlayersArray);
           
            string[] blackPlayersArray = dataBase.GetAllBlackPlayers();
            this.blackPlayer_toolStripComboBox.Items.Clear();
            this.blackPlayer_toolStripComboBox.Items.AddRange(blackPlayersArray);

            string[] siteArray = dataBase.GetAllSites();
            this.site_toolStripComboBox.Items.Clear();
            this.site_toolStripComboBox.Items.AddRange(siteArray);
        }

        private void clearEvent_toolStripButton_Click(object sender, EventArgs e)
        {
            this.event_toolStripComboBox.SelectedIndex = -1;
            this.event_toolStripComboBox.Text = "Выберите турнир";
            this.clearEvent_toolStripButton.Enabled = false;
        }

        private void clearWhitePlayer_toolStripButton_Click(object sender, EventArgs e)
        {
            this.whitePlayer_toolStripComboBox.SelectedIndex = -1;
            this.whitePlayer_toolStripComboBox.Text = "Выберите шахматиста";
            this.clearWhitePlayer_toolStripButton.Enabled = false;
        }

        private void clearBlackPlayer_toolStripButton_Click(object sender, EventArgs e)
        {
            this.blackPlayer_toolStripComboBox.SelectedIndex = -1;
            this.blackPlayer_toolStripComboBox.Text = "Выберите шахматиста";
            this.clearBlackPlayer_toolStripButton.Enabled = false;
        }

        private void clearSite_toolStripButton_Click(object sender, EventArgs e)
        {
            this.site_toolStripComboBox.SelectedIndex = -1;
            this.site_toolStripComboBox.Text = "Выберите место";
            this.clearSite_toolStripButton.Enabled = false;
        }

        private void event_toolStripComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.event_toolStripComboBox.SelectedIndex != -1)
            {
                this.clearEvent_toolStripButton.Enabled = true;
                this.dataFilter.SetFilter("event", Filter.EQUALS, event_toolStripComboBox.Items[event_toolStripComboBox.SelectedIndex].ToString());
                UpdateDataGridView();
            }
            else
            {
                this.dataFilter.SetFilter("event", Filter.EQUALS, Filter.ANY_VALUE);
                UpdateDataGridView();
            }
        }

        private void whitePlayer_toolStripComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.whitePlayer_toolStripComboBox.SelectedIndex != -1)
            {
                this.clearWhitePlayer_toolStripButton.Enabled = true;
                this.dataFilter.SetFilter("white", Filter.EQUALS, whitePlayer_toolStripComboBox.Items[whitePlayer_toolStripComboBox.SelectedIndex].ToString());
                UpdateDataGridView();
            }
            else
            {
                this.dataFilter.SetFilter("white", Filter.EQUALS, Filter.ANY_VALUE);
                UpdateDataGridView();
            }
        }

        private void blackPlayer_toolStripComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.blackPlayer_toolStripComboBox.SelectedIndex != -1)
            {
                this.clearBlackPlayer_toolStripButton.Enabled = true;
                this.dataFilter.SetFilter("black", Filter.EQUALS, blackPlayer_toolStripComboBox.Items[blackPlayer_toolStripComboBox.SelectedIndex].ToString());
                UpdateDataGridView();
            }
            else
            {
                this.dataFilter.SetFilter("black", Filter.EQUALS, Filter.ANY_VALUE);
                UpdateDataGridView();
            }
        }

        private void site_toolStripComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.site_toolStripComboBox.SelectedIndex != -1)
            {
                this.clearSite_toolStripButton.Enabled = true;
                this.dataFilter.SetFilter("site", Filter.EQUALS, site_toolStripComboBox.Items[site_toolStripComboBox.SelectedIndex].ToString());
                UpdateDataGridView();
            }
            else
            {
                this.dataFilter.SetFilter("site", Filter.EQUALS, Filter.ANY_VALUE);
                UpdateDataGridView();
            }
        }

        private void chessGames_dataGridView_SelectionChanged(object sender, EventArgs e)
        {
            if (chessGames_dataGridView.SelectedRows.Count > 0)
            {
                dataBase.OpenConnectionIfClosed();
                PGNTagPairs tagPairs = dataBase.GetTagPairsByID((Int64)chessGames_dataGridView.SelectedRows[0].Cells["idColumn"].Value);

                ShowPGNGameInfo(tagPairs);
                showSelectedChessGame_button.Enabled = true;
            }
        }

        private void ShowPGNGameInfo(PGNTagPairs tagPairs)
        {
            eventValue_label.Text = (tagPairs["Event"] == String.Empty? "-": tagPairs["Event"]);
            siteValue_label.Text = (tagPairs["Site"] == String.Empty ? "-" : tagPairs["Site"]);
            dateValue_label.Text = (tagPairs["Date"] == String.Empty ? "-" : tagPairs["Date"]);
            roundValue_label.Text = (tagPairs["Round"] == String.Empty ? "-" : tagPairs["Round"]);
            whiteValue_label.Text = (tagPairs["White"] == String.Empty ? "-" : tagPairs["White"]);
            blackValue_label.Text = (tagPairs["Black"] == String.Empty ? "-" : tagPairs["Black"]);
            resultValue_label.Text = (tagPairs["Result"] == String.Empty ? "-" : tagPairs["Result"]);
        }

        private void ChessTournamentsSearchForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            dataBase.CloseConnection();
        }

        private void export_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<Int64> pgnGameIDList = new List<Int64>();

            foreach (DataGridViewRow row in this.chessGames_dataGridView.SelectedRows)
            {
                pgnGameIDList.Add((Int64)row.Cells[0].Value);
            }
            if (pgnGameIDList.Count > 0)
            {
                PGNGamesExportForm exportForm = new PGNGamesExportForm(pgnGameIDList.ToArray());
                exportForm.ShowDialog();
            }
        }

        private void exportFiltered_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<Int64> pgnGameIDList = new List<Int64>();

            foreach (DataGridViewRow row in this.chessGames_dataGridView.Rows)
            {
                pgnGameIDList.Add((Int64)row.Cells[0].Value);
            }
            if (pgnGameIDList.Count > 0)
            {
                PGNGamesExportForm exportForm = new PGNGamesExportForm(pgnGameIDList.ToArray());
                exportForm.ShowDialog();
            }
        }

        private void showSelectedChessGame_button_Click(object sender, EventArgs e)
        {
            if (chessGames_dataGridView.SelectedRows.Count > 0)
            {
                PGNGameRecord pgnGame = dataBase.GetPGNGameByID((Int64)chessGames_dataGridView.SelectedRows[0].Cells[0].Value);
                ChessTournamentsViewForm viewForm = new ChessTournamentsViewForm(pgnGame);
                viewForm.Show();
            }
        }

        private void fenGenerator_button_Click(object sender, EventArgs e)
        {
            FENGeneratorForm fenGeneratorForm = new FENGeneratorForm(this.fenChessboardState_textBox.Text);
            fenGeneratorForm.ShowDialog();

            if (fenGeneratorForm.DialogResult == DialogResult.OK)
            {
                this.fenChessboardState_textBox.Text = fenGeneratorForm.FENStringResult;

                if (piecePlacementFilter_checkBox.Checked)
                {
                    this.searchPiecePlacement = false;
                    UpdateDataGridViewByFENFilter();
                }
            }
        }

        private void piecePlacementFilter_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            UpdateDataGridViewByFENFilter();
        }

        protected void UpdateDataGridViewByFENFilter()
        {
            FENData fenData = new FENData();
            fenData.PiecePlacement = FENPiecePlacement.GetStartChessGamePiecePlacement();

            this.statusLabel_toolStrip.Text = "Обновление списка шахматных партий, отностельно расположения фигур на доске...";

            if (!searchPiecePlacement && piecePlacementFilter_checkBox.Checked && fenChessboardState_textBox.Text != fenData.ToString())
            {
                Chessboard chessboard = new Chessboard();
                chessboard.LoadStateByFENPiecePlacement(new FENParser().GetExistingFENDataFromString(fenChessboardState_textBox.Text).PiecePlacement);
                
                string query = "SELECT * FROM [ChessGames] WHERE [id] IN (SELECT [chess_game_id] AS [id] FROM [ChessGameMoves] "
                + "WHERE [chessboard_mask]='" + chessboard.GetBitmask().ToString() + "' AND [fen_string]='" + fenChessboardState_textBox.Text + "')";
                
                this.chessTournamentsTableAdapter = new SqlCeDataAdapter(query, ChessTournamentsDataBase.GetSQLConnection());

                UpdateDataGridView();
                searchPiecePlacement = true;
                return;
            }
            
            if ((searchPiecePlacement && !piecePlacementFilter_checkBox.Checked) || fenChessboardState_textBox.Text == fenData.ToString())
            {
                this.chessTournamentsTableAdapter = new SqlCeDataAdapter("SELECT * FROM [ChessGames]", ChessTournamentsDataBase.GetSQLConnection());

                UpdateDataGridView();
                searchPiecePlacement = false;
            }
        }
    }
}
