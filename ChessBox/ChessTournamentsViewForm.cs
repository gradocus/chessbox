﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using ChessboardGamesCore;
using ChessboardGamesCore.IO;
using ChessboardGamesCore.IO.PGN;
using ChessBox.ChessGameCoreExtensions;

namespace ChessBox
{
    public partial class ChessTournamentsViewForm : Form
    {
        public ChessTournamentsViewForm(PGNGameRecord pgnGame)
        {
            this.pgnGame = pgnGame;
            ChessGameBuilder builder = new ChessGameBuilder(pgnGame);
            builder.BuildGame();
            this.chessGame = builder.Game;

            while (this.chessGame.MovementHistory.Position > 0)
                this.chessGame.MovementHistory.Undo();

            this.movetext_ListBox_SelfSelectedIndexChanged = false;

            InitializeComponent();
        }

        protected PGNGameRecord pgnGame;
        protected IGame chessGame;
        protected bool movetext_ListBox_SelfSelectedIndexChanged;

        private void ChessTournamentsViewForm_Load(object sender, EventArgs e)
        {
            this.label_gameHeader.Text = this.pgnGame.TagPairs.GetTagValueByName("Event") + ": "
                + this.pgnGame.TagPairs.GetTagValueByName("White") + " против " + this.pgnGame.TagPairs.GetTagValueByName("Black");

            this.eventValue_label.Text = this.pgnGame.TagPairs.GetTagValueByName("Event");
            this.siteValue_label.Text = this.pgnGame.TagPairs.GetTagValueByName("Site");
            this.dateValue_label.Text = this.pgnGame.TagPairs.GetTagValueByName("Date");
            this.roundValue_label.Text = this.pgnGame.TagPairs.GetTagValueByName("Round");
            this.whiteValue_label.Text = this.pgnGame.TagPairs.GetTagValueByName("White");
            this.blackValue_label.Text = this.pgnGame.TagPairs.GetTagValueByName("Black");
            this.resultValue_label.Text = this.pgnGame.TagPairs.GetTagValueByName("Result");

            for (int i = 0; i < this.pgnGame.Movetext.Count; i++)
            {
                StringBuilder movetextItemStringBuilder = new StringBuilder("\t");
                movetextItemStringBuilder.Append(this.pgnGame.Movetext[i].Number.ToString());
                movetextItemStringBuilder.Append(".\t");
                movetextItemStringBuilder.Append(this.pgnGame.Movetext[i].WhiteMove);
                movetextItemStringBuilder.Append("\t");
                movetextItemStringBuilder.Append(this.pgnGame.Movetext[i].BlackMove);
                this.movetext_listBox.Items.Add(movetextItemStringBuilder.ToString());
            }

            UpdateCurrentMoveNumberData();
        }

        private void button_GoToStartingPosition_Click(object sender, EventArgs e)
        {
            while (chessGame.MovementHistory.Position > 0)
                chessGame.MovementHistory.Undo();
            UpdateCurrentMoveNumberData();
        }

        private void button_GoBackMove_Click(object sender, EventArgs e)
        {
            chessGame.MovementHistory.Undo();
            UpdateCurrentMoveNumberData();
        }

        private void button_GoForwardMove_Click(object sender, EventArgs e)
        {
            chessGame.MovementHistory.Redo();
            UpdateCurrentMoveNumberData();
        }

        private void button_GoToFinalPosition_Click(object sender, EventArgs e)
        {
            while (chessGame.MovementHistory.Position < chessGame.MovementHistory.Count)
                chessGame.MovementHistory.Redo();
            UpdateCurrentMoveNumberData();
        }

        private void Close_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void UpdateCurrentMoveNumberData()
        {
            int currentMoveNumber = (this.chessGame.MovementHistory.Position - this.chessGame.MovementHistory.Position % 2) / 2;

            if (currentMoveNumber >= this.movetext_listBox.Items.Count)
                currentMoveNumber--;

            this.moveNumber_textBox.Text = (currentMoveNumber + 1).ToString();

            if (this.movetext_listBox.SelectedIndex != currentMoveNumber)
            {
                this.movetext_ListBox_SelfSelectedIndexChanged = true;
                this.movetext_listBox.SelectedIndex = currentMoveNumber;
            }

            this.drawPath = false;
            this.backgroundImage = chessGame.GetGameStateImage();
            pictureBox_chessGame.Image = backgroundImage;
        }

        private void movetext_listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!movetext_ListBox_SelfSelectedIndexChanged)
            {
                int requiredMoveNumber = this.movetext_listBox.SelectedIndex * 2;

                if (requiredMoveNumber == this.chessGame.MovementHistory.Position - 1)
                    return;

                int deltaMove = (requiredMoveNumber - this.chessGame.MovementHistory.Position) / Math.Abs(requiredMoveNumber - this.chessGame.MovementHistory.Position);
                while (this.chessGame.MovementHistory.Position != requiredMoveNumber)
                {
                    if (deltaMove < 0)
                    {
                        this.chessGame.MovementHistory.Undo();
                    }
                    else
                    {
                        this.chessGame.MovementHistory.Redo();
                    }
                }

                UpdateCurrentMoveNumberData();
            }
            else
            {
                this.movetext_ListBox_SelfSelectedIndexChanged = false;
            }
        }

        ChessboardCoordinate activeCoordinate;
        Image backgroundImage;
        bool drawPath = false;

        private void pictureBox_chessGame_MouseMove(object sender, MouseEventArgs e)
        {
            if (!this.drawPath)
            {
                int mouseX = e.X - 15;
                int mouseY = e.Y - 15;
                Rectangle chessboardRectangle = new Rectangle(0, 0, pictureBox_chessGame.Width - 15, pictureBox_chessGame.Height - 15);
                if (chessboardRectangle.Contains(new Point(mouseX, mouseY)))
                {
                    int file = (mouseX - mouseX % 55) / 55;
                    int rank = 7 - (mouseY - mouseY % 55) / 55;
                    ChessboardCoordinate newCoordinate = new ChessboardCoordinate(file > 7 ? 7 : file, rank < 0 ? 0 : rank);
                    if (newCoordinate != activeCoordinate && file <= 7 && rank >= 0)
                    {
                        activeCoordinate = newCoordinate;
                        RedrawChessboard();
                    }
                }
            }
        }

        private void pictureBox_chessGame_MouseUp(object sender, MouseEventArgs e)
        {
            if (this.drawPath && (e.Button == MouseButtons.Left || e.Button == MouseButtons.Right))
                this.drawPath = false;

            if (this.activeCoordinate != null && e.Button == MouseButtons.Right)
            {
                if (this.chessGame.Chessboard[this.activeCoordinate] != null)
                {
                    contextMenuMoveHistory.Items[0].Enabled = true;
                }
                else
                {
                    contextMenuMoveHistory.Items[0].Enabled = false;
                }
                Point location = new Point(this.Location.X + e.X, this.Location.Y + e.Y + 75);
                contextMenuMoveHistory.Show(location);
            }
        }

        protected void RedrawChessboard()
        {
            this.pictureBox_chessGame.Image = this.chessGame.Chessboard.DrawCursorSquare(this.backgroundImage, new List<ChessboardCoordinate>() { activeCoordinate });
        }

        private void splitContainer1_MouseEnter(object sender, EventArgs e)
        {
            if (!this.drawPath)
            {
                activeCoordinate = null;
                RedrawChessboard();
            }
        }

        private void показатьИсториюХодовToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.drawPath = true;
            this.pictureBox_chessGame.Image = this.chessGame.Chessboard.DrawPath(
                this.backgroundImage,
                this.chessGame.Chessboard[this.activeCoordinate].MovementHistory);
        }

        private void показатьСтатистикуХодовToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StaticticForm statistic = new StaticticForm(this.chessGame);
            statistic.ShowDialog();
        }
    }
}
