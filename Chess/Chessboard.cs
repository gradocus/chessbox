﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections;
using ChessboardGamesCore.Exceptions;

namespace ChessboardGamesCore
{
    public class Chessboard
    {
        public Chessboard()
        {
            this.pieces = new Piece[8, 8];
            this.pieceCoordinates = new Hashtable();
        }

        private Piece[,] pieces;
        private Hashtable pieceCoordinates;

        public Piece this[ChessboardCoordinate coordinate]
        {
            get
            {
                this.checkOnAnyValueException(coordinate);
                return this.pieces[coordinate.File, coordinate.Rank];
            }
            private set
            {
                this.checkOnAnyValueException(coordinate);
                this.pieces[coordinate.File, coordinate.Rank] = value;
            }
        }

        public Piece this[int file, int rank]
        {
            get
            {
                try
                {
                    return this.pieces[file, rank];
                }
                catch
                {
                    throw new ChessboardCoordinateOutOfRangeException();
                }
            }
            private set
            {
                try
                {
                    this.pieces[file, rank] = value;
                }
                catch
                {
                    throw new ChessboardCoordinateOutOfRangeException();
                }
            }
        }

        public void SetPiece(Piece piece, ChessboardCoordinate destinationCoordinate)
        {
            if (this[destinationCoordinate] != null)
            {
                throw new SetPieceOnOtherPieceException();
            }

            if (piece == null)
            {
                throw new SetNullPieceOnChessboardException();
            }

            string pieceKey = piece.ToString();
            List<ChessboardCoordinate> piecesCoordinateList = (List<ChessboardCoordinate>)this.pieceCoordinates[pieceKey];
            if (piecesCoordinateList == null)
                piecesCoordinateList = new List<ChessboardCoordinate>();
            else
                this.pieceCoordinates.Remove(pieceKey);
            piecesCoordinateList.Add(destinationCoordinate);
            this.pieceCoordinates.Add(pieceKey, piecesCoordinateList);

            this[destinationCoordinate] = piece;
        }

        public void RemovePiece(ChessboardCoordinate sourceCoordinate)
        {
            if (this[sourceCoordinate] == null)
            {
                throw new RemoveNullPieceFromChessboardException();
            }

            string pieceKey = this.pieces[sourceCoordinate.File, sourceCoordinate.Rank].ToString();
            List<ChessboardCoordinate> piecesCoordinateList = (List<ChessboardCoordinate>)this.pieceCoordinates[pieceKey];
            if (piecesCoordinateList != null)
            {
                this.pieceCoordinates.Remove(pieceKey);
                piecesCoordinateList.Remove(sourceCoordinate);
                this.pieceCoordinates.Add(pieceKey, piecesCoordinateList);
            }

            this[sourceCoordinate] = null;
        }

        public ChessboardCoordinate[] GetPiecesCoordinateArray(Piece pieceTemplate)
        {
            List<ChessboardCoordinate> resultList = new List<ChessboardCoordinate>();

            string pieceKey = pieceTemplate.ToString();
            if (this.pieceCoordinates.ContainsKey(pieceKey))
            {
                List<ChessboardCoordinate> allPiecesCoordinates = (List<ChessboardCoordinate>)this.pieceCoordinates[pieceKey];
                foreach (ChessboardCoordinate coordinate in allPiecesCoordinates)
                {
                    if (this.pieces[coordinate.File, coordinate.Rank] == pieceTemplate)
                        resultList.Add(coordinate);
                }
            }

            return resultList.ToArray();
        }

        private void checkOnAnyValueException(ChessboardCoordinate coordinate)
        {
            if (coordinate.File == ChessboardCoordinate.ANY_VALUE
                || coordinate.Rank == ChessboardCoordinate.ANY_VALUE)
            {
                throw new ChessboardCoordinateOutOfRangeException("Can't process ANY_VALUE coordinate.");
            }
        }

        #region Equal methods for chessboard comparisons.
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            Chessboard otherBoard = obj as Chessboard;

            return this.Equals(otherBoard);
        }


        public static bool operator ==(Chessboard firstBoard, Chessboard secondBoard)
        {
            if (System.Object.ReferenceEquals(firstBoard, secondBoard))
            {
                return true;
            }

            if (((object)firstBoard == null) || ((object)secondBoard == null))
            {
                return false;
            }

            return firstBoard.Equals(secondBoard);
        }

        public static bool operator !=(Chessboard firstBoard, Chessboard secondBoard)
        {
            return !(firstBoard == secondBoard);
        }

        public bool Equals(Chessboard otherBoard)
        {
            if ((object)otherBoard == null)
            {
                return false;
            }

            for (int file = 0; file < 8; file++)
                for (int rank = 0; rank < 8; rank++)
                    if (this.pieces[file, rank] != otherBoard.pieces[file, rank])
                        return false;

            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion
    }
}
