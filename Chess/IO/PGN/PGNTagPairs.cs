﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections;
using ChessboardGamesCore.IO.PGN.Exceptions;

namespace ChessboardGamesCore.IO.PGN
{
    public class PGNTagPairs : PGNData
    {
        public PGNTagPairs()
        {
            this.tagPairs = new Hashtable();
        }

        private Hashtable tagPairs;

        public string this[string tagName]
        {
            get { return this.GetTagValueByName(tagName); }
        }

        public int Count
        {
            get { return this.tagPairs.Count; }
        }

        public void AddTagPair(string tagName, string tagValue)
        {
            this.RemoveTagPairIfExists(tagName);
            this.tagPairs.Add(tagName, tagValue);
        }

        public bool ContainsTag(string tagName)
        {
            return this.tagPairs.ContainsKey(tagName);
        }

        public void RemoveTagPairIfExists(string tagName)
        {
            if (this.tagPairs.ContainsKey(tagName))
            {
                this.tagPairs.Remove(tagName);
            }
        }

        public override void Append(PGNData data)
        {
            PGNTagPairs newPGNTagPairs = data as PGNTagPairs;

            if (newPGNTagPairs == null)
            {
                throw new IncorrectPGNDataTypeException();
            }

            foreach (string tagName in newPGNTagPairs.GetTagNames())
            {
                this.RemoveTagPairIfExists(tagName);
                this.AddTagPair(tagName, newPGNTagPairs.GetTagValueByName(tagName));
            }
        }

        public string[] GetTagNames()
        {
            string[] tagNames = new string[this.tagPairs.Keys.Count];
            int tagNameCount = 0;

            foreach (DictionaryEntry d in this.tagPairs)
            {
                tagNames[tagNameCount] = d.Key.ToString();
                tagNameCount++;
            }

            return tagNames;
        }

        public string GetTagValueByName(string tagName)
        {
            if (this.tagPairs.ContainsKey(tagName))
            {
                return this.tagPairs[tagName].ToString();
            }
            else
            {
                return null;
            }
        }

        public override string ToString()
        {
            string sourceString = base.ToString();
            if (sourceString != String.Empty)
            {
                return sourceString;
            }

            ArrayList reverseKeys = new ArrayList(this.tagPairs.Keys);
            reverseKeys.Reverse();

            foreach (string tagName in reverseKeys)
            {
                this.sourceStringBuilder.Append("[");
                this.sourceStringBuilder.Append(tagName);
                this.sourceStringBuilder.Append(" \"");
                this.sourceStringBuilder.Append(this.tagPairs[tagName].ToString());
                this.sourceStringBuilder.Append("\"]\n");
            }

            return this.sourceStringBuilder.ToString();
        }
    }
}
