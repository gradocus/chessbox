﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace ChessboardGamesCore.IO.PGN
{
    public class PGNGameReader
    {
        public PGNGameReader(string pathToPGNFile)
        {
            this.GameRecord = null;
            if (File.Exists(pathToPGNFile))
            {
                this.PGNFileStreamReader = File.OpenText(pathToPGNFile);
                this.canRead = !this.PGNFileStreamReader.EndOfStream;
            }
            else
            {
                this.PGNFileStreamReader = null;
                this.canRead = false;
            }
        }

        public PGNGameReader(StreamReader streamReader)
        {
            this.PGNFileStreamReader = streamReader;
            this.GameRecord = null;
            this.canRead = !this.PGNFileStreamReader.EndOfStream;
        }

        private bool canRead;
        private StreamReader PGNFileStreamReader = null;
        private string lastReadedStringFromPGNFile = String.Empty;

        public PGNGameRecord GameRecord { get; private set; }

        public bool ReadNextGame()
        {
            if (!this.canRead)
            {
                return false;
            }

            PGNGameRecord newGame = new PGNGameRecord();

            try
            {
                newGame.TagPairs = (PGNTagPairs)this.GetParsedPGNData(new PGNTagPairsParser());
                newGame.Movetext = (PGNMovetext)this.GetParsedPGNData(new PGNMovetextParser());
            }
            catch (EndOfStreamException)
            {
                this.canRead = false;
                this.PGNFileStreamReader.Close();
                return false;
            }

            this.GameRecord = newGame;

            return true;
        }

        private PGNData GetParsedPGNData(IPGNDataParser parser)
        {
            PGNData PGNDataParsingResult = parser.GetEmptyPGNData();

            string stringFromPGNFile = String.Empty;
            while (!this.PGNFileStreamReader.EndOfStream && stringFromPGNFile == String.Empty)
            {
                stringFromPGNFile = this.PGNFileStreamReader.ReadLine();
            }
            
            if (this.PGNFileStreamReader.EndOfStream)
                throw new EndOfStreamException();

            while (!this.PGNFileStreamReader.EndOfStream || stringFromPGNFile != String.Empty)
            {
                PGNDataParsingResult.Append(parser.GetPGNDataFromString(stringFromPGNFile));
                PGNDataParsingResult.AddSourceLine(stringFromPGNFile);
                if (!this.PGNFileStreamReader.EndOfStream)
                    stringFromPGNFile = this.PGNFileStreamReader.ReadLine();
                else
                    break;

                if (stringFromPGNFile.Trim().Equals(String.Empty))
                {
                    break;
                }
            }

            return PGNDataParsingResult;
        }
    }
}
