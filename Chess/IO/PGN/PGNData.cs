﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChessboardGamesCore.IO.PGN
{
    public abstract class PGNData
    {
        protected StringBuilder sourceStringBuilder = new StringBuilder();

        public abstract void Append(PGNData data);

        public void AddSourceLine(string sourceString)
        {
            this.sourceStringBuilder.AppendLine(sourceString);
        }

        public override string ToString()
        {
            return sourceStringBuilder.ToString();
        }
    }
}
