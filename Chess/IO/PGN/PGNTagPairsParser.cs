﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChessboardGamesCore.IO.PGN
{
    class PGNTagPairsParser : IPGNDataParser
    {
        public PGNData GetEmptyPGNData()
        {
            return new PGNTagPairs();
        }

        const int TAG_NAME = 1;
        const int TAG_VALUE = 2;

        public PGNData GetPGNDataFromString(string PGNDataString)
        {
            PGNTagPairs resultTagPairs = new PGNTagPairs();

            foreach (string line in PGNDataString.Replace("\r", "").Split(new char[] { '\n' }))
            {
                if (line != String.Empty)
                {
                    string[] tagPairs = line.Split(new char[] { '"' }, 2);
                    resultTagPairs.AddTagPair(
                            tagPairs[0].Substring(1, tagPairs[0].Length - 2),
                            tagPairs[1].Substring(0, tagPairs[1].Length - 2)
                        );
                }
            }

            return resultTagPairs;
        }
    }
}
