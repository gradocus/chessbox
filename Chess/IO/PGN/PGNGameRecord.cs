﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChessboardGamesCore.IO.PGN
{
    public class PGNGameRecord
    {
        public PGNGameRecord() : this(new PGNTagPairs(), new PGNMovetext()) { }

        public PGNGameRecord(PGNTagPairs tagPairs, PGNMovetext movetext)
        {
            this.TagPairs = tagPairs;

            this.Movetext = movetext;
        }

        public PGNTagPairs TagPairs { get; set; }

        public PGNMovetext Movetext { get; set; }

        public override string ToString()
        {
            return this.TagPairs.ToString() + "\n" + this.Movetext.ToString();
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
