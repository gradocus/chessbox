﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChessboardGamesCore.IO.PGN.Exceptions
{
    public class DifferentMovesComplementException: Exception
    {
        public DifferentMovesComplementException()
            : base() { }
    }
}
