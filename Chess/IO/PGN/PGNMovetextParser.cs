﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Text.RegularExpressions;

namespace ChessboardGamesCore.IO.PGN
{
    class PGNMovetextParser : IPGNDataParser
    {
        public PGNData GetEmptyPGNData()
        {
            return new PGNMovetext();
        }

        private string reminderOfLastStringArgument = "";
        private PGNMove currentPGNMove = null;
        private int currentMoveNumber = 0;
        private bool parseWhiteMove = true;
        private bool parseBlackMoveComment = false;

        public PGNData GetPGNDataFromString(string PGNDataString)
        {
            PGNMovetext result = new PGNMovetext();

            const int GAME_RESULT = 1;
            const int MOVE_NUMBER = 2;
            const int MOVE_TEXT = 3;
            const int COMMENT = 4;

            Regex regex = new Regex("((?:\\*)|(?:[01]{1}(?:\\/2)?-[01]{1}(?:\\/2)?))|(?:([0-9]+)\\.{1,3})|([-a-hx1-8rnkqpRNBKQPO=+]+)|(?:\\{([^\\}]+)\\})");
            Match matchResult = regex.Match((this.reminderOfLastStringArgument != "") ? this.reminderOfLastStringArgument + " " + PGNDataString : PGNDataString);

            while (matchResult.Success)
            {
                if (matchResult.Groups[GAME_RESULT].Value != String.Empty)
                {
                    matchResult = matchResult.NextMatch();
                    continue;
                }

                if (matchResult.Groups[MOVE_NUMBER].Value != String.Empty)
                {
                    int newMoveNumber = Int32.Parse(matchResult.Groups[MOVE_NUMBER].Value);
                    if (newMoveNumber != currentMoveNumber)
                    {
                        if (currentPGNMove != null)
                        {
                            result.Add(currentPGNMove);
                        }

                        this.reminderOfLastStringArgument = matchResult.Value;
                        currentMoveNumber = newMoveNumber;
                        currentPGNMove = new PGNMove(currentMoveNumber);
                    }

                    matchResult = matchResult.NextMatch();
                    continue;
                }
                // We found chess move or comment.
                this.reminderOfLastStringArgument += " " + matchResult.Value;

                if (matchResult.Groups[COMMENT].Value != String.Empty)
                {
                    if (parseBlackMoveComment)
                    {
                        currentPGNMove.BlackMoveComment = (currentPGNMove.BlackMoveComment + " " + matchResult.Groups[COMMENT].Value).TrimStart();
                    }
                    else
                    {
                        currentPGNMove.WhiteMoveComment = (currentPGNMove.WhiteMoveComment + " " + matchResult.Groups[COMMENT].Value).TrimStart();
                    }

                    if (ThisCommentIsCompleted(matchResult.Value))
                    {
                        parseBlackMoveComment = !parseBlackMoveComment;
                    }

                    matchResult = matchResult.NextMatch();
                    continue;
                }

                if (parseWhiteMove)
                {
                    currentPGNMove.WhiteMove = matchResult.Groups[MOVE_TEXT].Value;
                    parseBlackMoveComment = false;
                }
                else
                {
                    currentPGNMove.BlackMove = matchResult.Groups[MOVE_TEXT].Value;
                    parseBlackMoveComment = true;
                }

                parseWhiteMove = !parseWhiteMove;

                matchResult = matchResult.NextMatch();
            }

            if (currentPGNMove != null)
            {
                result.Add(currentPGNMove);
                this.reminderOfLastStringArgument = this.currentMoveNumber.ToString() + ". ";
            }

            return result;
        }

        private bool ThisCommentIsCompleted(string commentText)
        {
            Regex regexp = new Regex("^(?:\\{[^\\}]+\\})|(?:;.+)$");
            return regexp.IsMatch(commentText);
        }
    }
}
