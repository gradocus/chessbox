﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ChessboardGamesCore.IO.PGN.Exceptions;

namespace ChessboardGamesCore.IO.PGN
{
    public class PGNMove
    {
        public PGNMove() : this(1, "", "", "", "") { }

        public PGNMove(int moveNumber) : this(moveNumber, "", "", "", "") { }

        public PGNMove(int moveNumber, string firstMoveArgument, string secondMoveArgument) : this(moveNumber, firstMoveArgument, "", secondMoveArgument, "") { }

        public PGNMove(int moveNumber, string firstMoveArgument, string firstMoveComment, string secondMoveArgument, string secondMoveComment)
        {
            this.Number = moveNumber;

            this.WhiteMove = firstMoveArgument;
            this.BlackMove = secondMoveArgument;

            this.WhiteMoveComment = firstMoveComment;
            this.BlackMoveComment = secondMoveComment;
        }

        public int Number { get; private set; }

        public string WhiteMove { get; set; }
        public string WhiteMoveComment { get; set; }

        public string BlackMove { get; set; }
        public string BlackMoveComment { get; set; }

        public void Complement(PGNMove move)
        {
            if (move.Number != this.Number)
            {
                throw new DifferentMovesComplementException();
            }

            if (this.WhiteMove == "" && move.WhiteMove != "")
                this.WhiteMove = move.WhiteMove;
            if (move.WhiteMoveComment != "")
                this.WhiteMoveComment = GetComplementComment(this.WhiteMoveComment, move.WhiteMoveComment);

            if (this.BlackMove == "" && move.BlackMove != "")
                this.BlackMove = move.BlackMove;
            if (move.BlackMoveComment != "")
                this.BlackMoveComment = GetComplementComment(this.BlackMoveComment, move.BlackMoveComment);
        }

        private string GetComplementComment(string commentText, string otherCommentText)
        {
            if (!commentText.Equals(otherCommentText))
                return (commentText != "") ? commentText + " " + otherCommentText : otherCommentText;
            else
                return commentText;
        }
    }
}
