﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChessboardGamesCore.IO.PGN
{
    interface IPGNDataParser
    {
        PGNData GetEmptyPGNData();

        PGNData GetPGNDataFromString(string PGNDataString);
    }
}
