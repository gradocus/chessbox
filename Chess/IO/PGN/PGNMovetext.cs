﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ChessboardGamesCore.IO.PGN.Exceptions;

namespace ChessboardGamesCore.IO.PGN
{
    public class PGNMovetext : PGNData
    {
        public PGNMovetext()
        {
            this.moves = new List<PGNMove>();
            this.sourceStringBuilder = new StringBuilder();
        }

        private List<PGNMove> moves;

        public static PGNData CreateFromString(string movetextString)
        {
            return new PGNMovetextParser().GetPGNDataFromString(movetextString);
        }

        public int Count
        {
            get { return this.moves.Count; }
        }

        public PGNMove this[int index]
        {
            get { return this.moves[index]; }
            set { this.moves[index] = value; }
        }

        public void Add(PGNMove move)
        {
            if ((this.moves.Count > 0) && ((this.moves.Count - this.moves[0].Number) == (move.Number - 1)))
            {
                this.moves[move.Number - this.moves[0].Number].Complement(move);
            }
            else
            {
                this.moves.Add(move);
                this.moves.Sort(delegate(PGNMove x, PGNMove y) { return x.Number.CompareTo(y.Number); });
            }
        }

        public void Add(PGNMove[] moves)
        {
            for (int i = 0; i < moves.Length; i++)
            {
                this.Add(moves[i]);
            }
        }

        public override void Append(PGNData data)
        {
            PGNMovetext otherPGNMovetext = data as PGNMovetext;

            if (otherPGNMovetext == null)
            {
                throw new IncorrectPGNDataTypeException();
            }

            this.Add(otherPGNMovetext.GetPGNMoves());
        }

        public PGNMove[] GetPGNMoves()
        {
            return this.moves.ToArray();
        }

        public override string ToString()
        {
            string sourceString = base.ToString();
            return sourceString != String.Empty ? sourceString : this.ToString(4);
        }

        public string ToString(int movesPerString)
        {
            const int UNLIMIT_MOVES_PER_STRING = 0;

            StringBuilder resultStringBuilder = new StringBuilder();

            int moveCountOnString = 0;
            for (int i = 0; i < this.Count; i++)
            {
                if (movesPerString != UNLIMIT_MOVES_PER_STRING && moveCountOnString == movesPerString)
                {
                    resultStringBuilder.Append("\n");
                    moveCountOnString = 0;
                }

                resultStringBuilder.Append(this.moves[i].Number.ToString());
                resultStringBuilder.Append(". ");
                resultStringBuilder.Append(this.moves[i].WhiteMove);
                resultStringBuilder.Append(" ");
                if (this.moves[i].WhiteMoveComment != String.Empty)
                {
                    resultStringBuilder.Append("{");
                    resultStringBuilder.Append(this.moves[i].WhiteMoveComment);
                    resultStringBuilder.Append("} ");
                    moveCountOnString++;

                    if (movesPerString != UNLIMIT_MOVES_PER_STRING && moveCountOnString == movesPerString)
                    {
                        resultStringBuilder.Append("\n");
                        moveCountOnString = 0;
                    }

                    resultStringBuilder.Append(this.moves[i].Number.ToString());
                    resultStringBuilder.Append("... ");
                }

                resultStringBuilder.Append(this.moves[i].BlackMove);
                if (this.moves[i].BlackMoveComment != String.Empty)
                {
                    resultStringBuilder.Append(" {");
                    resultStringBuilder.Append(this.moves[i].BlackMoveComment);
                    resultStringBuilder.Append("}");
                }
                resultStringBuilder.Append(" ");
                moveCountOnString++;
            }

            return resultStringBuilder.ToString();
        }
    }
}
