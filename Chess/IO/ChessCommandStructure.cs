﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ChessboardGamesCore.Commands.Chess;

namespace ChessboardGamesCore.IO
{
    class ChessCommandStructure
    {
        public ChessCommandStructure(string commandRecord)
        {
            this.commandString = commandRecord;

            this.buildSelfValues();
        }

        private string commandString;

        public bool IsCastling;

        public CastlingType CastlingType;

        public ChessboardCoordinate SourceCoordinate;

        public ChessboardCoordinate DestinationCoordinate;

        public PieceType MovePieceType;

        public bool IsCapture;

        public bool IsPawnPromotion;

        public PieceType PromotionPieceType;

        private void buildSelfValues()
        {
            this.IsCastling = this.isCastling();

            if (this.IsCastling)
            {
                this.CastlingType = this.getCastlingType();
            }
            else
            {
                this.MovePieceType = this.getMovePieceType();

                this.SourceCoordinate = this.getSourceCoordinate();
                this.DestinationCoordinate = this.getDestinationCoordinate();

                this.IsCapture = this.isCapture();

                this.IsPawnPromotion = this.isPawnPromotion();
                if (this.IsPawnPromotion)
                    this.PromotionPieceType = this.getPromotionPieceType();
            }
        }

        private bool isCastling()
        {
            return this.commandString.Contains("O-O");
        }

        private CastlingType getCastlingType()
        {
            CastlingType resultCastlingType = CastlingType.Kingside;

            if (this.commandString.Length > 4 && this.commandString.Substring(0, 5) == "O-O-O")
                resultCastlingType = CastlingType.Queenside;

            return resultCastlingType;
        }

        private ChessboardCoordinate getSourceCoordinate()
        {
            int anotherLetterCount = 2; // File and rank of move destination coordinate.

            if (this.commandString.Contains("#") || this.commandString.Contains("+"))
                anotherLetterCount++;
            if (this.isCapture())
                anotherLetterCount++;
            if (this.isPawnPromotion())
                anotherLetterCount++;
            if (this.commandString.Contains("="))
                anotherLetterCount++;

            int startIndex = 0;
            if (this.MovePieceType != PieceType.Pawn)
                startIndex++;

            if (this.commandString.Length > startIndex + anotherLetterCount)
            {
                string sourceCoordinateString = this.commandString.Substring(startIndex, this.commandString.Length - anotherLetterCount - startIndex);

                if (sourceCoordinateString.Length == 2)
                {
                    return new ChessboardCoordinate(sourceCoordinateString);
                }
                else
                {
                    int testCoordValue = sourceCoordinateString[0] - 'a';
                    if (testCoordValue >= 0 && testCoordValue <= 7)
                        return new ChessboardCoordinate(sourceCoordinateString[0], ChessboardCoordinate.ANY_VALUE);
                    else
                        return new ChessboardCoordinate(ChessboardCoordinate.ANY_VALUE, Int32.Parse(sourceCoordinateString) - 1);
                }
            }

            return new ChessboardCoordinate(
                    ChessboardCoordinate.ANY_VALUE,
                    ChessboardCoordinate.ANY_VALUE
                );
        }

        private ChessboardCoordinate getDestinationCoordinate()
        {
            int anotherLetterCount = 0;

            if (this.commandString.Contains("+") || this.commandString.Contains("#"))
                anotherLetterCount++;
            if (this.isPawnPromotion())
                anotherLetterCount++;
            if (this.commandString.Contains("="))
                anotherLetterCount++;

            string destinationCoordinateString = this.commandString.Substring(this.commandString.Length - anotherLetterCount - 2, 2);
            return new ChessboardCoordinate(destinationCoordinateString);
        }

        private PieceType getMovePieceType()
        {
            return getPieceType(this.commandString[0]);
        }

        private PieceType getPieceType(char ANLetter)
        {
            switch (ANLetter)
            {
                case 'K':
                    return PieceType.King;
                case 'Q':
                    return PieceType.Queen;
                case 'R':
                    return PieceType.Rook;
                case 'B':
                    return PieceType.Bishop;
                case 'N':
                    return PieceType.Knight;
                default:
                    return PieceType.Pawn;
            }
        }

        private bool isCapture()
        {
            return this.commandString.Contains("x");
        }

        private bool isPawnPromotion()
        {
            int otherCharCount = 0;
            if (this.commandString.Contains("+") || this.commandString.Contains("#"))
                otherCharCount++;

            char potentialPromotionChar = this.commandString[this.commandString.Length - 1 - otherCharCount];

            char[] promotedPieceANLetters = new char[] { 'Q', 'R', 'B', 'N' };
            return promotedPieceANLetters.Contains(potentialPromotionChar) || this.commandString.Contains("=");
        }

        private PieceType getPromotionPieceType()
        {
            int otherCharCount = 0;
            if (this.commandString.Contains("+") || this.commandString.Contains("#"))
                otherCharCount++;

            char potentialPromotionChar = this.commandString[this.commandString.Length - 1 - otherCharCount];

            return this.getPieceType(potentialPromotionChar);
        }
    }
}
