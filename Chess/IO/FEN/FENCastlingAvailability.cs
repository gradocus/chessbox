﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChessboardGamesCore.IO.FEN
{
    public class FENCastlingAvailability
    {
        public FENCastlingAvailability() : this("KQkq") { }

        public FENCastlingAvailability(string castlingAvailabilityString)
        {
            this.BlackCanCastleKingside = false;
            this.BlackCanCastleQueenside = false;
            this.WhiteCanCastleKingside = false;
            this.WhiteCanCastleQueenside = false;

            for (int i = 0; i < castlingAvailabilityString.Length; i++)
            {
                switch (castlingAvailabilityString[i])
                {
                    case 'K':
                        this.WhiteCanCastleKingside = true;
                        break;
                    case 'Q':
                        this.WhiteCanCastleQueenside = true;
                        break;
                    case 'k':
                        this.BlackCanCastleKingside = true;
                        break;
                    case 'q':
                        this.BlackCanCastleQueenside = true;
                        break;
                }
            }
        }

        public bool WhiteCanCastleKingside;
        public bool WhiteCanCastleQueenside;

        public bool BlackCanCastleKingside;
        public bool BlackCanCastleQueenside;

        public override string ToString()
        {
            StringBuilder resultStringBuilder = new StringBuilder();

            if (this.WhiteCanCastleKingside)
                resultStringBuilder.Append("K");
            if (this.WhiteCanCastleQueenside)
                resultStringBuilder.Append("Q");

            if (this.BlackCanCastleKingside)
                resultStringBuilder.Append("k");
            if (this.BlackCanCastleQueenside)
                resultStringBuilder.Append("q");

            if (resultStringBuilder.Length == 0)
                resultStringBuilder.Append("-");

            return resultStringBuilder.ToString();
        }
    }
}
