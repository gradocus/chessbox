﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ChessboardGamesCore.IO.FEN.Exceptions;

namespace ChessboardGamesCore.IO.FEN
{
    public class FENPiecePlacement
    {
        public static readonly char EMPTY_PLACE;

        public FENPiecePlacement()
        {
            this.piecePlacement = new char[8, 8];
        }

        public FENPiecePlacement(string FENPiecePlacementString)
        {
            this.piecePlacement = new char[8, 8];

            string[] piecePlacementRanks = FENPiecePlacementString.Split('/');

            if (!piecePlacementRanks.Length.Equals(8))
            {
                throw new InvalidFENStringException();
            }

            for (int rank = 7; rank >= 0; rank--) // Rank from 8 to 1.
            {
                // Use invertedRank because FENPiecePlacementString contain ranks from 8 to 1.
                int invertedRank = 7 - rank;
                int piecePlacementFileIndex = 0;

                for (int file = 0; file < 8; file++) // From A to H.
                {
                    int nextEmpty = 0;
                    if (Int32.TryParse(piecePlacementRanks[invertedRank][piecePlacementFileIndex].ToString(), out nextEmpty))
                    {
                        file += nextEmpty - 1;
                        piecePlacementFileIndex++;
                        continue;
                    }

                    this.piecePlacement[file, rank] = piecePlacementRanks[invertedRank][piecePlacementFileIndex];
                    piecePlacementFileIndex++;
                }
            }
        }

        public static FENPiecePlacement GetStartChessGamePiecePlacement()
        {
            return new FENPiecePlacement("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR");
        }

        public char this[int File, int Rank]
        {
            get
            {
                checkFileAndRank(File, Rank);
                return this.piecePlacement[File, Rank];
            }
            set
            {
                checkFileAndRank(File, Rank);
                this.piecePlacement[File, Rank] = value;
            }
        }

        private char[,] piecePlacement;

        private void checkFileAndRank(int File, int Rank)
        {
            if (File < 0 || File > 7)
                throw new Exception("File is out of range.");

            if (Rank < 0 || Rank > 7)
                throw new Exception("Rank is out of range.");
        }

        public override string ToString()
        {
            StringBuilder resultStringBuilder = new StringBuilder();

            for (int rank = 7; rank >= 0; rank--) // Rank from 8 to 1.
            {
                int emptyPlaceCount = 0;
                for (int file = 0; file < 8; file++) // File from A to H.
                {
                    if (this.piecePlacement[file, rank] == EMPTY_PLACE)
                    {
                        emptyPlaceCount++;
                        continue;
                    }

                    if (emptyPlaceCount > 0)
                    {
                        resultStringBuilder.Append(emptyPlaceCount.ToString());
                        emptyPlaceCount = 0;
                    }
                    resultStringBuilder.Append(this.piecePlacement[file, rank]);
                }

                if (emptyPlaceCount > 0)
                    resultStringBuilder.Append(emptyPlaceCount.ToString());

                if (rank > 0)
                    resultStringBuilder.Append('/');
            }

            return resultStringBuilder.ToString();
        }

        public override bool Equals(object obj)
        {
            FENPiecePlacement otherPiecePlacement = (FENPiecePlacement)obj;

            if (otherPiecePlacement == null) return false;

            for (int file = 0; file < this.piecePlacement.GetLength(0); file++)
            {
                for (int rank = 0; rank < this.piecePlacement.GetLength(1); rank++)
                {
                    if (!this.piecePlacement[file, rank].Equals(otherPiecePlacement[file, rank]))
                        return false;
                }
            }

            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
