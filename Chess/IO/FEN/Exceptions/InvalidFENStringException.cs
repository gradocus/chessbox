﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChessboardGamesCore.IO.FEN.Exceptions
{
    public class InvalidFENStringException
        : Exception
    {
        public InvalidFENStringException()
            : base() { }
    }
}
