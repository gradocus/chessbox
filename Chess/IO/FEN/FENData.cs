﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChessboardGamesCore.IO.FEN
{
    public class FENData
    {
        public FENData()
        {
            this.PiecePlacement = new FENPiecePlacement();
            this.ActiveColor = PieceColor.White;
            this.CastlingAvailability = new FENCastlingAvailability();
            this.EnPassantTargetSquare = "-";
            this.HalfmoveClock = 0;
            this.FullmoveClock = 1;
        }

        public FENPiecePlacement PiecePlacement;
        public PieceColor ActiveColor;
        public FENCastlingAvailability CastlingAvailability;
        public string EnPassantTargetSquare;
        public int HalfmoveClock;
        public int FullmoveClock;

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.Append(this.PiecePlacement.ToString());
            stringBuilder.Append(" ");
            stringBuilder.Append(this.ActiveColor == PieceColor.White ? "w" : "b");
            stringBuilder.Append(" ");
            stringBuilder.Append(this.CastlingAvailability.ToString());

            return stringBuilder.ToString();
        }
    }
}
