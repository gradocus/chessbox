﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ChessboardGamesCore.IO.FEN.Exceptions;

namespace ChessboardGamesCore.IO.FEN
{
    public class FENParser
    {
        private const byte PIECE_PLACEMENT = 0;
        private const byte ACTIVE_COLOR = 1;
        private const byte CASTLING_AVAILABILITY = 2;
        private const byte ENPASSANT_TARGET_SQUARE = 3;
        private const byte HALFMOVE_CLOCK = 4;
        private const byte FULLMOVE_CLOCK = 5;

        public FENData GetFENDataFromFENString(string FENString)
        {
            FENData data = new FENData();
            string[] FENParts = FENString.Split(' ');

            if (FENParts.Length < 6)
                throw new InvalidFENStringException();

            data.PiecePlacement = new FENPiecePlacement(FENParts[PIECE_PLACEMENT]);
            data.ActiveColor = FENParts[ACTIVE_COLOR] == "w" ? PieceColor.White : PieceColor.Black;
            data.CastlingAvailability = new FENCastlingAvailability(FENParts[CASTLING_AVAILABILITY]);
            data.EnPassantTargetSquare = FENParts[ENPASSANT_TARGET_SQUARE];
            data.HalfmoveClock = Int32.Parse(FENParts[HALFMOVE_CLOCK]);
            data.FullmoveClock = Int32.Parse(FENParts[FULLMOVE_CLOCK]);

            return data;
        }

        public FENData GetExistingFENDataFromString(string String)
        {
            FENData data = new FENData();
            string[] FENParts = String.Split(' ');

            if (FENParts.Length != 3 && FENParts.Length < 6)
                throw new InvalidFENStringException();

            data.PiecePlacement = new FENPiecePlacement(FENParts[PIECE_PLACEMENT]);
            data.ActiveColor = FENParts[ACTIVE_COLOR] == "w" ? PieceColor.White : PieceColor.Black;
            data.CastlingAvailability = new FENCastlingAvailability(FENParts[CASTLING_AVAILABILITY]);

            if (FENParts.Length > 3)
            {
                data.EnPassantTargetSquare = FENParts[ENPASSANT_TARGET_SQUARE];
                data.HalfmoveClock = Int32.Parse(FENParts[HALFMOVE_CLOCK]);
                data.FullmoveClock = Int32.Parse(FENParts[FULLMOVE_CLOCK]);
            }

            return data;
        }
    }
}
