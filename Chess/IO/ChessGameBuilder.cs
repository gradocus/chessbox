﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ChessboardGamesCore.IO.PGN;
using ChessboardGamesCore.IO.FEN;

namespace ChessboardGamesCore.IO
{
    public class ChessGameBuilder
    {
        public ChessGameBuilder(PGNGameRecord gameRecord)
        {
            Game = null;
            this.gameRecord = gameRecord;
        }

        private PGNGameRecord gameRecord;

        public IGame Game { get; private set; }

        public bool BuildGame()
        {
            ChessGame resultGame;

            if (this.gameRecord.TagPairs.ContainsTag("FEN"))
            {
                Chessboard chessboard = new Chessboard();

                FENParser parser = new FENParser();
                FENData fen = parser.GetExistingFENDataFromString(this.gameRecord.TagPairs["FEN"]);

                resultGame = new ChessGame(chessboard, fen.ActiveColor, fen.HalfmoveClock, fen.FullmoveClock);
            }
            else
            {
                resultGame = new ChessGame();
            }

            foreach (PGNMove move in this.gameRecord.Movetext.GetPGNMoves())
            {
                if (move.WhiteMove != String.Empty)
                {
                    ChessGameCommandBuilder commandBuilder = new ChessGameCommandBuilder(resultGame, move.WhiteMove);
                    if (commandBuilder.BuildCommand())
                    {
                        resultGame.MovementHistory.ExecuteCommand(commandBuilder.Command);
                        if (move.BlackMove != String.Empty)
                        {
                            commandBuilder = new ChessGameCommandBuilder(resultGame, move.BlackMove);
                            if (commandBuilder.BuildCommand())
                            {
                                resultGame.MovementHistory.ExecuteCommand(commandBuilder.Command);
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            this.Game = resultGame;
            return true;
        }
    }
}
