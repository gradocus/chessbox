﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChessboardGamesCore.IO
{
    public interface IGameCommandBuilder
    {
        ICommand Command { get; }

        bool BuildCommand();
    }
}
