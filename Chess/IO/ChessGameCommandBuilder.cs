﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections;
using ChessboardGamesCore.Commands;
using ChessboardGamesCore.Commands.Chess;

namespace ChessboardGamesCore.IO
{
    public class ChessGameCommandBuilder
        : IGameCommandBuilder
    {
        public ChessGameCommandBuilder(IGame game, string commandRecord)
        {
            this.Game = game as ChessGame;
            if (this.Game == null)
                throw new Exception();

            this.Command = null;
            this.commandRecord = commandRecord;
        }

        private ChessGame Game;

        public ICommand Command { get; private set; }

        private string commandRecord;

        public bool BuildCommand()
        {
            ChessCommandStructure commandStruct
                = new ChessCommandStructure(this.commandRecord);

            Piece pieceTemplate = new Piece(commandStruct.MovePieceType, this.Game.NextMoveColor);

            IPieceBehavior pieceBehavior = this.Game.CreatePieceBehavior(pieceTemplate);

            ICommand resultCommand = null;

            if (commandStruct.IsCastling)
            {
                resultCommand = new Castling(this.Game, this.Game.NextMoveColor, commandStruct.CastlingType);
                if (!pieceBehavior.CanDo(resultCommand))
                    return false;
            }
            else
            {
                List<ICommand> pieceCommandList = new List<ICommand>();
                List<ChessboardCoordinate> pieceCoordinateList = new List<ChessboardCoordinate>();
                foreach (ChessboardCoordinate sourceCoordinate
                    in this.Game.Chessboard.GetPiecesCoordinateArray(pieceTemplate))
                {
                    if (sourceCoordinate != commandStruct.SourceCoordinate)
                        continue;

                    ICommand currentCommand = GetCommadFromStructureAndSourceCoordinate(commandStruct, sourceCoordinate);

                    if (pieceBehavior.CanDo(currentCommand))
                    {
                        pieceCommandList.Add(currentCommand);
                        pieceCoordinateList.Add(sourceCoordinate);
                    }
                }

                if (pieceCoordinateList.Count == 0)
                {
                    return false;
                }

                if (pieceCoordinateList.Count == 1)
                {
                    resultCommand = pieceCommandList[0];
                }
                else
                {
                    Piece currentKing = new Piece(PieceType.King, this.Game.NextMoveColor);

                    if (this.Game.IsCheckFor(currentKing))
                    {
                        for (int i = 0; i< pieceCoordinateList.Count; i++)
                        {
                            try
                            {
                                pieceCommandList[i].Execute();
                                if (!this.Game.IsCheckFor(currentKing))
                                {
                                    pieceCommandList[i].UnExecute();
                                    resultCommand = pieceCommandList[i];
                                    break;
                                }
                                pieceCommandList[i].UnExecute();
                            }
                            catch { }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < pieceCoordinateList.Count; i++)
                        {
                            try
                            {
                                pieceCommandList[i].Execute();
                                if (this.Game.IsCheckFor(currentKing))
                                {
                                    pieceCommandList[i].UnExecute();

                                    pieceCommandList.RemoveAt(i);
                                    pieceCoordinateList.RemoveAt(i);

                                    i--;
                                }
                                pieceCommandList[i].UnExecute();
                            }
                            catch { }
                        }

                        if (pieceCoordinateList.Count == 1)
                        {
                            resultCommand = pieceCommandList[0];
                        }
                    }
                }
            }

            if (resultCommand != null)
            {
                this.Command = resultCommand;
                return true;
            }

            return false;
        }

        private ICommand GetCommadFromStructureAndSourceCoordinate(ChessCommandStructure structure, ChessboardCoordinate coordinate)
        {
            ICommand resultCommand = null;
            
            if (structure.IsCastling)
            {
                resultCommand = new Castling(this.Game, this.Game.NextMoveColor, structure.CastlingType);
            }
            else
            {
                Move move = new Move(this.Game, coordinate, structure.DestinationCoordinate);
                if (structure.IsCapture)
                {
                    if (this.Game.Chessboard[move.SourceCoordinate].Type == PieceType.Pawn
                        && this.Game.EnPassantCoordinate == move.DestinationCoordinate)
                    {
                        resultCommand = new Capture(move,
                                new ChessboardCoordinate(
                                        move.DestinationCoordinate.File,
                                        move.SourceCoordinate.Rank
                                    )
                            );
                    }
                    else
                        resultCommand = new Capture(move, move.DestinationCoordinate);
                }
                else
                    resultCommand = move;

                if (structure.IsPawnPromotion)
                {
                    CompositeCommand compositeCommand = new CompositeCommand();
                    compositeCommand.AddCommand(resultCommand);
                    Promotion promotion = new Promotion(this.Game, structure.DestinationCoordinate, structure.PromotionPieceType);
                    compositeCommand.AddCommand(promotion);

                    resultCommand = compositeCommand;
                }
            }
            
            return resultCommand;
        }
    }
}
