﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ChessboardGamesCore.Commands;
using ChessboardGamesCore.Commands.Chess;
using ChessboardGamesCore.Behaviors;
using ChessboardGamesCore.Behaviors.Chess;

namespace ChessboardGamesCore
{
    public class ChessGame : IGame
    {
        public ChessGame() : this(new Chessboard(), PieceColor.White, 0, 1)
        {
            this.LoadNewGameState();
        }

        public ChessGame(Chessboard initialBoardState,  PieceColor nextMoveColor, int halfmoveClock, int fullmoveClock)
        {
            this.Chessboard = initialBoardState;

            this.EnPassantCoordinate = null;

            this.HalfmoveClock = halfmoveClock;
            this.deltaFullmoveClock = fullmoveClock;

            this.MovementHistory = new GameCommandHistory();
            this.deltaNextMoveColor = (byte)(this.NextMoveColor == nextMoveColor ? 0 : 1);
        }

        public Chessboard Chessboard
        {
            get;
            private set;
        }

        public GameCommandHistory MovementHistory
        {
            get;
            private set;
        }

        public IPieceBehavior CreatePieceBehavior(Piece piece)
        {
            switch (piece.Type)
            {
                case PieceType.Pawn:
                    return new PawnBehavior(piece.Color);
                case PieceType.Bishop:
                    return new BishopBehavior(piece.Color);
                case PieceType.Rook:
                    return new RookBehavior(piece.Color);
                case PieceType.Queen:
                    return new QueenBehavior(piece.Color);
                case PieceType.King:
                    return new KingBehavior(piece.Color);
                case PieceType.Knight:
                    return new KnightBehavior(piece.Color);
                default:
                    return new NullPieceBehavior();
            }
        }

        private byte deltaNextMoveColor = 0;
        public PieceColor NextMoveColor
        {
            get
            {
                return (this.MovementHistory.Position + deltaNextMoveColor) % 2 == 0
                    ? PieceColor.White : PieceColor.Black;
            }
        }

        public ChessboardCoordinate EnPassantCoordinate;

        public int HalfmoveClock;

        private int deltaFullmoveClock;
        public int FullmoveClock
        {
            get
            {
                return this.MovementHistory.Position / 2 + this.deltaFullmoveClock;
            }
        }

        public bool IsCheckForSquare(ChessboardCoordinate destinationCoordinate, PieceColor enemyPieceColor)
        {

            bool colorFlag = false;
            if (enemyPieceColor != this.NextMoveColor)
            {
                colorFlag = true;
                this.deltaNextMoveColor++;
            }

            foreach (PieceType type in Enum.GetValues(typeof(PieceType)))
            {
                IPieceBehavior CurrentTypePiecesBehavior
                    = this.CreatePieceBehavior(new Piece(type, enemyPieceColor));

                foreach (ChessboardCoordinate coordinate in
                    this.Chessboard.GetPiecesCoordinateArray(new Piece(type, enemyPieceColor)))
                {
                    Move move = new Move(this, coordinate, destinationCoordinate);
                    Capture capture = new Capture(move, destinationCoordinate);
                    if (CurrentTypePiecesBehavior.CanDo(move))
                    {
                        if (colorFlag)
                            this.deltaNextMoveColor--;

                        return true;
                    }
                }
            }

            if (colorFlag)
                this.deltaNextMoveColor--;

            return false;
        }

        public bool IsCheckFor(Piece king)
        {
            ChessboardCoordinate kingCoordinate = this.Chessboard.GetPiecesCoordinateArray(king)[0];

            PieceColor enemyPieceColor = (king.Color == PieceColor.White) ? PieceColor.Black : PieceColor.White;

            return this.IsCheckForSquare(kingCoordinate, enemyPieceColor);
        }

        private void LoadNewGameState()
        {
            this.SetupNewPieceOnChessboard(PieceType.Rook, PieceColor.White, new ChessboardCoordinate("A1"));
            this.SetupNewPieceOnChessboard(PieceType.Knight, PieceColor.White, new ChessboardCoordinate("B1"));
            this.SetupNewPieceOnChessboard(PieceType.Bishop, PieceColor.White, new ChessboardCoordinate("C1"));
            this.SetupNewPieceOnChessboard(PieceType.Queen, PieceColor.White, new ChessboardCoordinate("D1"));
            this.SetupNewPieceOnChessboard(PieceType.King, PieceColor.White, new ChessboardCoordinate("E1"));
            this.SetupNewPieceOnChessboard(PieceType.Bishop, PieceColor.White, new ChessboardCoordinate("F1"));
            this.SetupNewPieceOnChessboard(PieceType.Knight, PieceColor.White, new ChessboardCoordinate("G1"));
            this.SetupNewPieceOnChessboard(PieceType.Rook, PieceColor.White, new ChessboardCoordinate("H1"));

            // Setting pawns.
            for (int file = 0; file < 8; file++) // From A to H.
            {
                this.SetupNewPieceOnChessboard( // Set white pawn on 2nd square.
                    PieceType.Pawn, PieceColor.White,
                    new ChessboardCoordinate(file, 1));
                this.SetupNewPieceOnChessboard( // Set black pawn on 7th square.
                    PieceType.Pawn, PieceColor.Black,
                    new ChessboardCoordinate(file, 6));
            }

            this.SetupNewPieceOnChessboard(PieceType.Rook, PieceColor.Black, new ChessboardCoordinate("A8"));
            this.SetupNewPieceOnChessboard(PieceType.Knight, PieceColor.Black, new ChessboardCoordinate("B8"));
            this.SetupNewPieceOnChessboard(PieceType.Bishop, PieceColor.Black, new ChessboardCoordinate("C8"));
            this.SetupNewPieceOnChessboard(PieceType.Queen, PieceColor.Black, new ChessboardCoordinate("D8"));
            this.SetupNewPieceOnChessboard(PieceType.King, PieceColor.Black, new ChessboardCoordinate("E8"));
            this.SetupNewPieceOnChessboard(PieceType.Bishop, PieceColor.Black, new ChessboardCoordinate("F8"));
            this.SetupNewPieceOnChessboard(PieceType.Knight, PieceColor.Black, new ChessboardCoordinate("G8"));
            this.SetupNewPieceOnChessboard(PieceType.Rook, PieceColor.Black, new ChessboardCoordinate("H8"));
        }

        private void SetupNewPieceOnChessboard(PieceType pieceType, PieceColor pieceColor, ChessboardCoordinate destinationCoordinate)
        {
            this.Chessboard.SetPiece(new Piece(pieceType, pieceColor), destinationCoordinate);
            this.Chessboard[destinationCoordinate].MovementHistory.Add(destinationCoordinate);
        }
    }
}
