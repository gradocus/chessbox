﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChessboardGamesCore
{
    public interface IGame
    {
        Chessboard Chessboard { get; }

        GameCommandHistory MovementHistory { get; }

        IPieceBehavior CreatePieceBehavior(Piece piece);
    }
}
