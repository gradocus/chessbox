﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ChessboardGamesCore.Commands;
using ChessboardGamesCore.Commands.Chess;

namespace ChessboardGamesCore
{
    public interface IPieceBehavior
    {
        bool CanDo(ICommand command);
    }
}
