﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ChessboardGamesCore.Exceptions;

namespace ChessboardGamesCore.Commands
{
    public class CompositeCommand
        : ICommand
    {
        public CompositeCommand(params ICommand[] commands)
        {
            this.commandList = new List<ICommand>();
            this.commandList.AddRange(commands);

            this.IsReversible = false;
        }

        public ChessboardCoordinate DestinationCoordinate
        {
            get { return commandList.Last().DestinationCoordinate; }
        }

        public bool IsReversible
        {
            get;
            private set;
        }

        public int Count
        {
            get { return this.commandList.Count; }
        }

        private readonly List<ICommand> commandList;

        public void AddCommand(ICommand command)
        {
            this.commandList.Add(command);
        }

        public void RemoveCommand(ICommand command)
        {
            this.commandList.Remove(command);
        }

        public void Execute()
        {
            List<ICommand> executedCommands = new List<ICommand>();

            foreach (ICommand command in this.commandList)
            {
                try
                {
                    command.Execute();
                    executedCommands.Add(command);
                }
                catch
                {
                    foreach (ICommand executedCommand in
                        executedCommands.AsEnumerable().Reverse())
                    {
                        executedCommand.UnExecute();
                    }
                    throw new IncorrectCommandException();
                }
            }

            this.IsReversible = true;
        }

        public void UnExecute()
        {
            foreach (ICommand command in
                this.commandList.AsEnumerable().Reverse())
            {
                command.UnExecute();
            }

            this.IsReversible = false;
        }
    }
}
