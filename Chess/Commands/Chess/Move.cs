﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ChessboardGamesCore.Behaviors;
using ChessboardGamesCore.Exceptions;

namespace ChessboardGamesCore.Commands.Chess
{
    public class Move : ChessCommand
    {
        public Move(ChessGame chessGame,
                         ChessboardCoordinate sourceCoordinate,
                         ChessboardCoordinate destinationCoordinate)
            : this(chessGame, sourceCoordinate, destinationCoordinate, null) { }

        public Move(ChessGame chessGame,
                         ChessboardCoordinate sourceCoordinate,
                         ChessboardCoordinate destinationCoordinate,
                         Capture capture)
            : base(chessGame)
        {
            this.SourceCoordinate = sourceCoordinate;
            this.DestinationCoordinate = destinationCoordinate;

            this.Capture = null;
        }

        public ChessboardCoordinate SourceCoordinate { get; private set; }
        public new ChessboardCoordinate DestinationCoordinate { get; private set; }

        protected override ChessboardCoordinate GetDestinationCoordinate()
        {
            return this.DestinationCoordinate;
        }

        public Capture Capture;

        private ChessboardCoordinate EnPassantCoordinate;

        public override void Execute()
        {
            IPieceBehavior pieceBehavior = this.Game.CreatePieceBehavior(
                this.Game.Chessboard[this.SourceCoordinate]);

            if (!pieceBehavior.CanDo(this))
            {
                throw new IncorrectCommandException();
            }
            
            this.EnPassantCoordinate = this.Game.EnPassantCoordinate;
            this.Game.EnPassantCoordinate = (pieceBehavior as ChessPieceBehavior).GetEnPassantCoordinate(this);

            this.Game.Chessboard.SetPiece(this.Game.Chessboard[this.SourceCoordinate], this.DestinationCoordinate);
            this.Game.Chessboard.RemovePiece(this.SourceCoordinate);
            this.Game.HalfmoveClock++;

            this.Game.Chessboard[DestinationCoordinate].MovementHistory.Add(DestinationCoordinate);

            base.Execute();
        }

        public override void UnExecute()
        {
            base.UnExecute();

            this.Game.Chessboard.SetPiece(this.Game.Chessboard[this.DestinationCoordinate], this.SourceCoordinate);
            this.Game.Chessboard.RemovePiece(this.DestinationCoordinate);

            this.Game.HalfmoveClock--;
            this.Game.EnPassantCoordinate = this.EnPassantCoordinate;

            this.Game.Chessboard[this.SourceCoordinate].MovementHistory
                .RemoveAt(this.Game.Chessboard[this.SourceCoordinate].MovementHistory.Count - 1);
        }
    }
}
