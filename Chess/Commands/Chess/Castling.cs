﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ChessboardGamesCore.Exceptions;

namespace ChessboardGamesCore.Commands.Chess
{
    public class Castling : ChessCommand
    {
        public Castling(ChessGame game, PieceColor color, CastlingType type)
            :base(game)
        {
            this.Color = color;
            this.Type = type;
            this.rank = (this.Color == PieceColor.White) ? 0 : 7;

            this.sourceKingCoordinate = new ChessboardCoordinate(4 , rank); // File = "e"

            // For Kingside castling rook's file = "h" and for Queenside castling rook's file = "a".
            this.sourceRookCoordinate = new ChessboardCoordinate((this.Type == CastlingType.Kingside) ? 7 : 0, rank);

            this.destinationKingCoordinate = new ChessboardCoordinate((this.Type == CastlingType.Kingside) ? 6 : 2, rank);

            this.destinationRookCoordinate = new ChessboardCoordinate((this.Type == CastlingType.Kingside) ? 5 : 3, rank);
        }

        public PieceColor Color { get; protected set; }
        public CastlingType Type { get; protected set; }

        protected int rank;
        protected ChessboardCoordinate sourceKingCoordinate;
        protected ChessboardCoordinate sourceRookCoordinate;
        protected ChessboardCoordinate destinationKingCoordinate;
        protected ChessboardCoordinate destinationRookCoordinate;

        protected override ChessboardCoordinate GetDestinationCoordinate()
        {
            return destinationKingCoordinate;
        }

        public override void Execute()
        {
            if (!this.IsPossibleCommand())
                throw new IncorrectCommandException();

            this.Game.Chessboard.SetPiece(this.Game.Chessboard[this.sourceKingCoordinate], this.destinationKingCoordinate);
            this.Game.Chessboard[this.destinationKingCoordinate].MovementHistory.Add(this.destinationKingCoordinate);
            this.Game.Chessboard.RemovePiece(this.sourceKingCoordinate);

            this.Game.Chessboard.SetPiece(this.Game.Chessboard[this.sourceRookCoordinate], this.destinationRookCoordinate);
            this.Game.Chessboard[this.destinationRookCoordinate].MovementHistory.Add(this.destinationRookCoordinate);
            this.Game.Chessboard.RemovePiece(this.sourceRookCoordinate);

            base.Execute();
        }

        private bool IsPossibleCommand()
        {
            if (this.Game.Chessboard[this.sourceKingCoordinate] == null
                || this.Game.Chessboard[this.sourceKingCoordinate].Type != PieceType.King
                || this.Game.Chessboard[this.sourceKingCoordinate].Color != this.Color
                || this.Game.Chessboard[this.sourceKingCoordinate].MovementHistory.Count > 1)
            {
                return false;
            }

            if (this.Game.Chessboard[this.sourceRookCoordinate] == null
                || this.Game.Chessboard[this.sourceRookCoordinate].Type != PieceType.Rook
                || this.Game.Chessboard[this.sourceRookCoordinate].Color != this.Color
                || this.Game.Chessboard[this.sourceRookCoordinate].MovementHistory.Count > 1)
            {
                return false;
            }

            int startIndex = Math.Min(this.sourceKingCoordinate.File, this.sourceRookCoordinate.File) + 1;
            int endIndex = Math.Max(this.sourceKingCoordinate.File, this.sourceRookCoordinate.File);
            for (int i = startIndex; i < endIndex; i++)
                if (this.Game.Chessboard[i, this.rank] != null)
                {
                    return false;
                }

            return true;
        }


        public override void UnExecute()
        {
            base.UnExecute();

            this.Game.Chessboard.SetPiece(this.Game.Chessboard[this.destinationKingCoordinate], this.sourceKingCoordinate);
            this.Game.Chessboard[this.sourceKingCoordinate].MovementHistory.RemoveAt(
                    this.Game.Chessboard[this.sourceKingCoordinate].MovementHistory.Count - 1
                );
            this.Game.Chessboard.RemovePiece(this.destinationKingCoordinate);

            this.Game.Chessboard.SetPiece(this.Game.Chessboard[this.destinationRookCoordinate], this.sourceRookCoordinate);
            this.Game.Chessboard[this.sourceRookCoordinate].MovementHistory.RemoveAt(
                    this.Game.Chessboard[this.sourceRookCoordinate].MovementHistory.Count - 1
                );
            this.Game.Chessboard.RemovePiece(this.destinationRookCoordinate);
        }
    }
}
