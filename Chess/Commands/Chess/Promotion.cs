﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ChessboardGamesCore.Exceptions;

namespace ChessboardGamesCore.Commands.Chess
{
    public class Promotion : ChessCommand
    {
        public Promotion(ChessGame game, ChessboardCoordinate pawnCoordinate, PieceType destinationPieceType)
            : base(game)
        {
            this.PawnCoordinate = pawnCoordinate;
            this.destinationPieceType = destinationPieceType;
        }

        public ChessboardCoordinate PawnCoordinate { get; private set; }
        private PieceType destinationPieceType;
        private PieceType oldPieceType;

        protected override ChessboardCoordinate GetDestinationCoordinate()
        {
            return this.PawnCoordinate;
        }

        public override void Execute()
        {
            if (!IsPossibleCommand())
                throw new IncorrectCommandException();

            this.oldPieceType = this.Game.Chessboard[this.PawnCoordinate].Type;

            Piece newPiece = new Piece(this.destinationPieceType,
                this.Game.Chessboard[this.PawnCoordinate].Color,
                this.Game.Chessboard[this.PawnCoordinate].MovementHistory);

            this.Game.Chessboard.RemovePiece(this.PawnCoordinate);
            this.Game.Chessboard.SetPiece(newPiece, this.PawnCoordinate);

            base.Execute();
        }

        private bool IsPossibleCommand()
        {
            if (this.Game.Chessboard[this.PawnCoordinate].Type != PieceType.Pawn)
                return false;

            if (this.destinationPieceType == PieceType.King)
            {
                return false;
            }

            return this.PawnCoordinate.Rank % 7 == 0;
        }

        public override void UnExecute()
        {
            base.UnExecute();

            Piece oldPiece = new Piece(this.oldPieceType,
                this.Game.Chessboard[this.PawnCoordinate].Color,
                this.Game.Chessboard[this.PawnCoordinate].MovementHistory);

            this.Game.Chessboard.RemovePiece(this.PawnCoordinate);
            this.Game.Chessboard.SetPiece(oldPiece, this.PawnCoordinate);
        }
    }
}
