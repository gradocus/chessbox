﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ChessboardGamesCore.Exceptions;

namespace ChessboardGamesCore.Commands.Chess
{
    public class Capture : ChessCommand
    {
        public Capture(Move chessMove,
                            ChessboardCoordinate capturedPieceCoordinate)
            : base(chessMove.Game)
        {
            this.Move = chessMove;

            this.halfmoveClock = 0;
            this.capturedPiece = null;
            this.CapturedPieceCoordinate = capturedPieceCoordinate;

            this.Move.Capture = this;
        }

        public Move Move { get; private set; }

        private int halfmoveClock;
        private Piece capturedPiece;
        public ChessboardCoordinate CapturedPieceCoordinate { get; private set; }

        protected override ChessboardCoordinate GetDestinationCoordinate()
        {
            return this.Move.DestinationCoordinate;
        }

        public override void Execute()
        {
            IPieceBehavior pieceBehavior = this.Game.CreatePieceBehavior(
                this.Game.Chessboard[this.Move.SourceCoordinate]);

            if (!pieceBehavior.CanDo(this))
            {
                throw new IncorrectCommandException();
            }

            this.halfmoveClock = this.Move.Game.HalfmoveClock;
            this.capturedPiece = this.Move.Game.Chessboard[this.CapturedPieceCoordinate];
            this.Move.Game.Chessboard.RemovePiece(this.CapturedPieceCoordinate);
            this.Move.Game.HalfmoveClock = -1;

            this.Move.Execute();

            base.Execute();
        }

        public override void UnExecute()
        {
            base.UnExecute();

            this.Move.UnExecute();

            this.Move.Game.Chessboard.SetPiece(this.capturedPiece, this.CapturedPieceCoordinate);
            this.Move.Game.HalfmoveClock = this.halfmoveClock;
        }
    }
}
