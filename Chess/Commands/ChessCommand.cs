﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChessboardGamesCore.Commands
{
    public abstract class ChessCommand
        : ICommand
    {
        public ChessCommand(ChessGame game)
        {
            this.Game = game;
            this.IsReversible = false;
        }

        public ChessGame Game { get; private set; }

        public ChessboardCoordinate DestinationCoordinate
        {
            get { return GetDestinationCoordinate(); }
        }

        protected abstract ChessboardCoordinate GetDestinationCoordinate();

        public virtual void Execute()
        {
            this.IsReversible = true;
        }

        public bool IsReversible
        {
            get;
            private set;
        }

        public virtual void UnExecute()
        {
            if (!this.IsReversible)
            {
                throw new Exception("Try UnExecute not executed command");
            }

            this.IsReversible = false;
        }
    }
}
