﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChessboardGamesCore
{
    public enum PieceType
    {
        Bishop,
        King,
        Knight,
        Pawn,
        Queen,
        Rook
    }
}
