﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChessboardGamesCore
{
    public class GameCommandHistory
    {
        public GameCommandHistory()
        {
            this.commandList = new List<ICommand>();
            this.Position = 0;
        }

        public int Position { get; private set; }

        public int Count
        {
            get { return this.commandList.Count; }
        }

        private List<ICommand> commandList;

        public ICommand[] Commands
        {
            get { return this.commandList.ToArray(); }
        }

        public void ExecuteCommand(ICommand command)
        {
            command.Execute();
            for (int i = this.commandList.Count - 1; i >= this.Position; i--)
                this.commandList.RemoveAt(i);
            this.commandList.Add(command);
            this.Position++;
        }

        public void Undo()
        {
            if (this.Position > 0)
            {
                this.Position--;
                this.commandList[this.Position].UnExecute();
            }
        }

        public void Redo()
        {
            if (this.Position < this.commandList.Count)
            {
                this.commandList[this.Position].Execute();
                this.Position++;
            }
        }
    }
}
