﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChessboardGamesCore
{
    public interface ICommand
    {
        ChessboardCoordinate DestinationCoordinate { get; }

        void Execute();

        bool IsReversible { get; }

        void UnExecute();
    }
}
