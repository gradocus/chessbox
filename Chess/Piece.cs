﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChessboardGamesCore
{
    public class Piece
    {
        public Piece(PieceType type, PieceColor color)
            : this(type, color, new List<ChessboardCoordinate>()) { }

        public Piece(PieceType type, PieceColor color, List<ChessboardCoordinate> movementHistory)
        {
            this.Type = type;
            this.Color = color;
            this.MovementHistory = movementHistory;
        }

        public PieceType Type { get; private set; }
        public PieceColor Color { get; private set; }

        public List<ChessboardCoordinate> MovementHistory
        {
            get;
            private set;
        }

        #region Equal methods for piece comparisons.
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            Piece otherPiece = obj as Piece;

            return this.Equals(otherPiece);
        }

        public static bool operator ==(Piece firstPiece, Piece secondPiece)
        {
            if (System.Object.ReferenceEquals(firstPiece, secondPiece))
            {
                return true;
            }

            if (((object)firstPiece == null) || ((object)secondPiece == null))
            {
                return false;
            }

            return firstPiece.Equals(secondPiece);
        }

        public static bool operator !=(Piece firstPiece, Piece secondPiece)
        {
            return !(firstPiece == secondPiece);
        }

        public bool Equals(Piece otherPiece)
        {
            if ((object)otherPiece == null)
            {
                return false;
            }

            return otherPiece.Type == this.Type
                && otherPiece.Color == this.Color;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion
    }
}
