﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ChessboardGamesCore.Exceptions;

namespace ChessboardGamesCore
{
    public class ChessboardCoordinate
    {
        public ChessboardCoordinate(string fileAndRank)
            : this(fileAndRank.ToLower()[0] - 'a',
                    Int32.Parse(fileAndRank[1].ToString()) - 1)
        { }

        public ChessboardCoordinate(char file, int rank)
            : this(file.ToString().ToLower()[0] - 'a', rank < 0 ? -1 : rank - 1)
        { }

        public ChessboardCoordinate(int file, int rank)
        {
            if (file > 7 || file < -1)
                throw new ChessboardCoordinateOutOfRangeException("File coordinate is out of chessboard coordinate range.");
            if (rank > 7 || rank < -1)
                throw new ChessboardCoordinateOutOfRangeException("Rank coordinate is out of chessboard coordinate range.");

            this.File = file;
            this.Rank = rank;
        }

        public static readonly int ANY_VALUE = -1;

        public int File { get; private set; }
        public int Rank { get; private set; }

        #region Equal methods for coordinate comparisons.
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            ChessboardCoordinate otherCoordinate = obj as ChessboardCoordinate;

            return this.Equals(otherCoordinate);
        }

        public static bool operator ==(ChessboardCoordinate firstCoordinate, ChessboardCoordinate secondCoordinate)
        {
            if (System.Object.ReferenceEquals(firstCoordinate, secondCoordinate))
            {
                return true;
            }

            if (((object)firstCoordinate == null) || ((object)secondCoordinate == null))
            {
                return false;
            }

            return firstCoordinate.Equals(secondCoordinate);
        }

        public static bool operator !=(ChessboardCoordinate firstCoordinate, ChessboardCoordinate secondCoordinate)
        {
            return !(firstCoordinate == secondCoordinate);
        }

        public bool Equals(ChessboardCoordinate otherCoordinate)
        {
            if ((object)otherCoordinate == null)
            {
                return false;
            }

            return (otherCoordinate.File == ANY_VALUE || this.File == ANY_VALUE || otherCoordinate.File == this.File)
                && (otherCoordinate.Rank == ANY_VALUE || this.Rank == ANY_VALUE || otherCoordinate.Rank == this.Rank);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion
    }
}
