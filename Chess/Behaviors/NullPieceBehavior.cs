﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ChessboardGamesCore.Commands;

namespace ChessboardGamesCore.Behaviors
{
    public class NullPieceBehavior
        : IPieceBehavior
    {
        public bool CanDo(ICommand command)
        {
            return false;
        }
    }
}
