﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ChessboardGamesCore.Commands.Chess;

namespace ChessboardGamesCore.Behaviors.Chess
{
    public class KnightBehavior : ChessPieceBehavior
    {
        public KnightBehavior(PieceColor color)
            : base(color) { }

        protected override bool CorrectWay(Move move)
        {
            int deltaFile = move.DestinationCoordinate.File - move.SourceCoordinate.File;
            int deltaRank = move.DestinationCoordinate.Rank - move.SourceCoordinate.Rank;

            int[] correctDeltaFileOrRank = new int[] { 1, 2 };

            if (!correctDeltaFileOrRank.Contains(Math.Abs(deltaFile))
                    || !correctDeltaFileOrRank.Contains(Math.Abs(deltaRank))
                    || Math.Abs(deltaFile) == Math.Abs(deltaRank))
                return false;

            return move.Game.Chessboard[move.DestinationCoordinate] == null
                || move.Game.Chessboard[move.DestinationCoordinate].Color != this.Color;
        }
    }
}
