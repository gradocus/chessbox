﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ChessboardGamesCore.Commands.Chess;

namespace ChessboardGamesCore.Behaviors.Chess
{
    public class PawnBehavior : ChessPieceBehavior
    {
        public PawnBehavior(PieceColor color)
            : base(color) { }

        protected override bool CorrectWay(Move move)
        {
            int moveDirection = (this.Color == PieceColor.White) ? 1 : -1;

            int deltaFile = move.DestinationCoordinate.File - move.SourceCoordinate.File;
            int deltaRank = move.DestinationCoordinate.Rank - move.SourceCoordinate.Rank;

            if (moveDirection != Math.Sign(deltaRank))
                return false;

            if (Math.Abs(deltaRank) == 1 && Math.Abs(deltaFile) < 2)
            {
                // Simple capture or EnPassant capture.
                if (Math.Abs(deltaFile) == 1)
                {
                    return move.Game.EnPassantCoordinate == move.DestinationCoordinate
                        || (move.Capture != null && move.Capture.CapturedPieceCoordinate == move.DestinationCoordinate);
                }
                else // Standart move.
                {
                    return move.Game.Chessboard[move.DestinationCoordinate] == null;
                }
            }

            // Move it forwqard two squares.
            if (Math.Abs(deltaRank) == 2 && deltaFile == 0 && move.SourceCoordinate.Rank % 5 == 1)
            {
                return move.Game.Chessboard[move.SourceCoordinate.File, move.SourceCoordinate.Rank + moveDirection] == null;
            }

            return false;
        }

        protected override bool CanDo(Capture capture)
        {
            int moveDirection = (this.Color == PieceColor.White) ? 1 : -1;

            int deltaFile = capture.Move.DestinationCoordinate.File - capture.Move.SourceCoordinate.File;
            int deltaRank = capture.Move.DestinationCoordinate.Rank - capture.Move.SourceCoordinate.Rank;

            if (Math.Abs(deltaFile) == 1 && deltaRank == moveDirection
                && (capture.Move.DestinationCoordinate == capture.CapturedPieceCoordinate
                || capture.Move.DestinationCoordinate == capture.Move.Game.EnPassantCoordinate))
                return true;

            return false;
        }
        
        protected bool isEnPassant(Move move)
        {
            int deltaFile = move.DestinationCoordinate.File - move.SourceCoordinate.File;
            int deltaRank = move.DestinationCoordinate.Rank - move.SourceCoordinate.Rank;

            return deltaFile == 0 && move.SourceCoordinate.Rank % 5 == 1 && Math.Abs(deltaRank) == 2;
        }

        protected override bool CanDo(Promotion promotion)
        {
            return promotion.PawnCoordinate.Rank == (this.Color == PieceColor.White ? 7 : 0);
        }

        public override ChessboardCoordinate GetEnPassantCoordinate(Move move)
        {
            if (this.isEnPassant(move))
            {
                int moveDirection = (this.Color == PieceColor.White) ? 1 : -1;
                return new ChessboardCoordinate(move.SourceCoordinate.File,
                                                move.SourceCoordinate.Rank + moveDirection);
            }
            else
                return null;
        }
    }
}
