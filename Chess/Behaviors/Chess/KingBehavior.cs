﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ChessboardGamesCore.Commands.Chess;

namespace ChessboardGamesCore.Behaviors.Chess
{
    public class KingBehavior : ChessPieceBehavior
    {
        public KingBehavior(PieceColor color)
            : base(color) { }

        protected override bool CorrectWay(Move move)
        {
            int deltaFile = move.DestinationCoordinate.File - move.SourceCoordinate.File;
            int deltaRank = move.DestinationCoordinate.Rank - move.SourceCoordinate.Rank;

            if (Math.Abs(deltaFile) > 1 || Math.Abs(deltaRank) > 1)
            {
                return false;
            }

            return move.Game.Chessboard[move.DestinationCoordinate] == null
                || move.Game.Chessboard[move.DestinationCoordinate].Color != this.Color;
        }
    }
}
