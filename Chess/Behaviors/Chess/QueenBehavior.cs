﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ChessboardGamesCore.Commands.Chess;

namespace ChessboardGamesCore.Behaviors.Chess
{
    public class QueenBehavior : ChessPieceBehavior
    {
        public QueenBehavior(PieceColor color)
            : base(color) { }

        protected override bool CorrectWay(Move move)
        {
            int deltaFile = move.DestinationCoordinate.File - move.SourceCoordinate.File;
            int deltaRank = move.DestinationCoordinate.Rank - move.SourceCoordinate.Rank;

            if (Math.Abs(deltaFile) != Math.Abs(deltaRank)
                    && deltaFile != 0 && deltaRank != 0)
                return false;

            int file = move.SourceCoordinate.File + Math.Sign(deltaFile);
            int rank = move.SourceCoordinate.Rank + Math.Sign(deltaRank);

            while (file != move.DestinationCoordinate.File
                 || rank != move.DestinationCoordinate.Rank)
            {
                ChessboardCoordinate middleCoordiname = new ChessboardCoordinate(file, rank);
                if (move.Game.Chessboard[middleCoordiname] != null)
                    return false;

                file += Math.Sign(deltaFile);
                rank += Math.Sign(deltaRank);
            }

            return move.Game.Chessboard[move.DestinationCoordinate] == null
                || move.Game.Chessboard[move.DestinationCoordinate].Color != this.Color;
        }
    }
}
