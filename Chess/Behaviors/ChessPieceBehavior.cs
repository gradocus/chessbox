﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ChessboardGamesCore.Commands;
using ChessboardGamesCore.Commands.Chess;
using ChessboardGamesCore.Exceptions;

namespace ChessboardGamesCore.Behaviors
{
    public abstract class ChessPieceBehavior : IPieceBehavior
    {
        public ChessPieceBehavior(PieceColor color)
        {
            this.Color = color;
        }

        protected PieceColor Color;

        public bool CanDo(ICommand command)
        {
            if (command as Move != null)
                return this.CanDo(command as Move);

            if (command as Capture != null)
                return this.CanDo(command as Capture);

            if (command as Castling != null)
                return this.CanDo(command as Castling);

            if (command as CompositeCommand != null)
                return this.CanDo(command as CompositeCommand);

            if (command as Promotion != null)
                return this.CanDo(command as Promotion);

            return false;
        }

        private bool CanDo(Castling castling)
        {
            try
            {
                castling.Execute();
                castling.UnExecute();
                return this.checkWayChecksForCastling(castling);
            }
            catch (IncorrectCommandException)
            {
                return false;
            }
        }

        private bool checkWayChecksForCastling(Castling castling)
        {
            ChessboardCoordinate kingCoordinate = new ChessboardCoordinate('e', (this.Color == PieceColor.White) ? 1 : 8);

            ChessboardCoordinate rookCoordinate= new ChessboardCoordinate(
                    (castling.Type == CastlingType.Queenside) ? 'a' : 'h',
                    (this.Color == PieceColor.White) ? 1 : 8
                );

            PieceColor enemyColor = castling.Color == PieceColor.White? PieceColor.Black: PieceColor.White;
            int kingDirection = Math.Sign(rookCoordinate.File - kingCoordinate.File);
            for (int i = kingCoordinate.File; i != rookCoordinate.File; i += kingDirection)
            {
                ChessboardCoordinate destinationCoordinate = new ChessboardCoordinate(i + kingDirection, kingCoordinate.Rank);
                if (castling.Game.IsCheckForSquare(destinationCoordinate, enemyColor))
                {
                    return false;
                }
            }

            return true;
        }

        private bool CanDo(Move move)
        {
            return this.Color == move.Game.NextMoveColor && this.CorrectWay(move);
        }

        protected abstract bool CorrectWay(Move move);

        protected virtual bool CanDo(Capture capture)
        {
            return this.CanDo(capture.Move) && capture.CapturedPieceCoordinate == capture.Move.DestinationCoordinate;
        }

        protected virtual bool CanDo(CompositeCommand compositeCommand)
        {
            try
            {
                compositeCommand.Execute();
                compositeCommand.UnExecute();
                return true;
            }
            catch (IncorrectCommandException)
            {
                return false;
            }
        }

        protected virtual bool CanDo(Promotion promotion)
        {
            return false;
        }

        public virtual ChessboardCoordinate GetEnPassantCoordinate(Move move)
        {
            return null;
        }
    }
}
