﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChessboardGamesCore.Exceptions
{
    public class SetPieceOnOtherPieceException
        : Exception
    {
        public SetPieceOnOtherPieceException()
            : base() { }
    }
}
