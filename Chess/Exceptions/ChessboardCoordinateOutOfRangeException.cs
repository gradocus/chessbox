﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChessboardGamesCore.Exceptions
{
    public class ChessboardCoordinateOutOfRangeException
        : Exception
    {
        public ChessboardCoordinateOutOfRangeException()
            : base() { }

        public ChessboardCoordinateOutOfRangeException(string message)
            : base(message) { }
    }
}
