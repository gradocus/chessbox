﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChessboardGamesCore.Exceptions
{
    public class RemoveNullPieceFromChessboardException
        : Exception
    {
        public RemoveNullPieceFromChessboardException()
            : base() { }
    }
}
