﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore.IO.PGN;
using ChessboardGamesCore.IO.PGN.Exceptions;

namespace ChessboardGamesCoreNUnit.IO.PGNTests
{
    [TestFixture]
    class PGNMovetextTests
    {
        [Test]
        public void ComplementMovesWhenAddNumberTheSameLastAddedNumber()
        {
            PGNMovetext movetext = new PGNMovetext();

            PGNMove firtPartOfTheMove = new PGNMove(1, "White", "");
            movetext.Add(firtPartOfTheMove);

            PGNMove secondPartOfTheMove = new PGNMove(1, "", "Black");
            movetext.Add(secondPartOfTheMove);

            Assert.That(movetext.Count, Is.EqualTo(1));
            Assert.That(movetext[0].Number, Is.EqualTo(1));
            Assert.That(movetext[0].WhiteMove, Is.EqualTo("White"));
            Assert.That(movetext[0].BlackMove, Is.EqualTo("Black"));
        }

        [Test]
        public void SortMovesByNumberOnAddingNewMove()
        {
            PGNMovetext movetext = new PGNMovetext();
            movetext.Add(new PGNMove(2));
            movetext.Add(new PGNMove(4));
            movetext.Add(new PGNMove(3));

            Assert.That(movetext[0].Number, Is.EqualTo(2));
            Assert.That(movetext[1].Number, Is.EqualTo(3));
            Assert.That(movetext[2].Number, Is.EqualTo(4));

            movetext.Add(new PGNMove(1));
            Assert.That(movetext[0].Number, Is.EqualTo(1));
        }

        [Test]
        public void AppendTwoMovetext()
        {
            PGNMovetext firstMovetext = new PGNMovetext();
            firstMovetext.Add(new PGNMove(1, "White", "Black"));
            firstMovetext.Add(new PGNMove(2, "White", "Black"));

            PGNMovetext secondMovetext = new PGNMovetext();
            secondMovetext.Add(new PGNMove(3, "White", "Black"));
            secondMovetext.Add(new PGNMove(4, "White", "Black"));

            firstMovetext.Append(secondMovetext);

            Assert.That(firstMovetext.Count, Is.EqualTo(4));
        }

        [Test]
        public void AppendTwoCrossMovetext()
        {
            PGNMovetext firstMovetext = new PGNMovetext();
            firstMovetext.Add(new PGNMove(1, "White", "Black"));
            firstMovetext.Add(new PGNMove(2, "first White", "first Black"));

            PGNMovetext secondMovetext = new PGNMovetext();
            secondMovetext.Add(new PGNMove(2, "second White", "second Black"));
            secondMovetext.Add(new PGNMove(3, "White", "Black"));

            firstMovetext.Append(secondMovetext);

            Assert.That(firstMovetext.Count, Is.EqualTo(3));
            Assert.That(firstMovetext[1].WhiteMove, Is.EqualTo("first White"));
            Assert.That(firstMovetext[1].BlackMove, Is.EqualTo("first Black"));
        }

        [Test]
        [ExpectedException(typeof(IncorrectPGNDataTypeException))]
        public void IncorrectPGNDataTypeExceptionOnAppendingWithNonMovetextPGNData()
        {
            PGNMovetext movetext = new PGNMovetext();
            movetext.Append(new PGNTagPairs());
        }

        [Test]
        public void ConvertToString()
        {
            PGNMovetext movetext = new PGNMovetext();
            movetext.Add(new PGNMove(1, "e4", "e5"));

            Assert.That(movetext.ToString(), Is.EqualTo("1. e4 e5 "));
        }

        [Test]
        public void ConvertToStringWithComments()
        {
            PGNMovetext movetext = new PGNMovetext();
            movetext.Add(new PGNMove(1, "e4", "Comment for white move.", "e5", "Comment for black move."));

            Assert.That(movetext.ToString(), Is.EqualTo("1. e4 {Comment for white move.} 1... e5 {Comment for black move.} "));
        }

        [Test]
        public void ConvertToStringWithOnlyWhiteComment()
        {
            PGNMovetext movetext = new PGNMovetext();
            movetext.Add(new PGNMove(1, "e4", "Comment for white move.", "e5", ""));

            Assert.That(movetext.ToString(), Is.EqualTo("1. e4 {Comment for white move.} 1... e5 "));
        }

        [Test]
        public void ConvertToStringWithOnlyBlackComment()
        {
            PGNMovetext movetext = new PGNMovetext();
            movetext.Add(new PGNMove(1, "e4", "", "e5", "Comment for black move."));

            Assert.That(movetext.ToString(), Is.EqualTo("1. e4 e5 {Comment for black move.} "));
        }

        [Test]
        public void ConvertToStringWith3MovesPerLine()
        {
            PGNMovetext movetext = new PGNMovetext();
            movetext.Add(new PGNMove(1, "e4", "e5"));
            movetext.Add(new PGNMove(2, "e4", "e5"));
            movetext.Add(new PGNMove(3, "e4", "e5"));
            movetext.Add(new PGNMove(4, "e4", "e5"));
            movetext.Add(new PGNMove(5, "e4", "e5"));

            Assert.That(movetext.ToString(3), Is.EqualTo("1. e4 e5 2. e4 e5 3. e4 e5 \n4. e4 e5 5. e4 e5 "));
        }

        [Test]
        public void ConvertToStringWith0MovesPerLineEqualToUnlimitMovesPerLine()
        {
            PGNMovetext movetext = new PGNMovetext();
            movetext.Add(new PGNMove(1, "e4", "e5"));
            movetext.Add(new PGNMove(2, "e4", "e5"));
            movetext.Add(new PGNMove(3, "e4", "e5"));
            movetext.Add(new PGNMove(4, "e4", "e5"));
            movetext.Add(new PGNMove(5, "e4", "e5"));

            Assert.That(movetext.ToString(0), Is.EqualTo("1. e4 e5 2. e4 e5 3. e4 e5 4. e4 e5 5. e4 e5 "));
        }
    }
}
