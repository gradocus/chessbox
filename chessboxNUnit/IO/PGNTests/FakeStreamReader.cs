﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Windows.Forms;

namespace ChessboardGamesCoreNUnit.IO.PGNTests
{
    class FakeStreamReader
        : StreamReader
    {
        public FakeStreamReader()
            : this(new string[] { }) { }

        public FakeStreamReader(string[] fileLines)
            : base(new MemoryStream(new byte[] { 1 }))
        {
            this.Closed = false;
            this.currentLineIndex = -1;
            this.fileLines = fileLines;
        }

        private string[] fileLines;
        private int currentLineIndex;

        public bool Closed { get; private set; }

        public override string ReadLine()
        {
            this.currentLineIndex++;

            if (this.EndOfStream || this.Closed
                    || this.currentLineIndex >= this.fileLines.Length)
                throw new EndOfStreamException();

            if (this.Closed || this.currentLineIndex == this.fileLines.Length - 1)
            {
                this.BaseStream.Position = this.BaseStream.Length;
                return fileLines[currentLineIndex];
            }

            return fileLines[currentLineIndex];
        }

        public override void Close()
        {
            this.Closed = true;
        }
    }
}
