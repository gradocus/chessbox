﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore.IO.PGN;
using ChessboardGamesCore.IO.PGN.Exceptions;

namespace ChessboardGamesCoreNUnit.IO.PGNTests
{
    [TestFixture]
    class PGNMoveTests
    {
        [Test]
        public void ComplementMovesWithTheSameNumbers()
        {
            PGNMove firstPartOfTheMove = new PGNMove(1, "white move", "");
            Assert.That(firstPartOfTheMove.WhiteMove, Is.EqualTo("white move"));
            Assert.That(firstPartOfTheMove.BlackMove, Is.EqualTo(""));
            
            PGNMove secondPartOfTheMove = new PGNMove(1, "", "black move");

            firstPartOfTheMove.Complement(secondPartOfTheMove);
            Assert.That(firstPartOfTheMove.WhiteMove, Is.EqualTo("white move"));
            Assert.That(firstPartOfTheMove.BlackMove, Is.EqualTo("black move"));
        }

        [Test]
        public void AppendCommentsOnPGNMoveComplement()
        {
            PGNMove firstPartOfTheMove = new PGNMove(1, "white move", "Full white move", "black move", "Full black");
            Assert.That(firstPartOfTheMove.WhiteMove, Is.EqualTo("white move"));
            Assert.That(firstPartOfTheMove.WhiteMoveComment, Is.EqualTo("Full white move"));
            Assert.That(firstPartOfTheMove.BlackMove, Is.EqualTo("black move"));
            Assert.That(firstPartOfTheMove.BlackMoveComment, Is.EqualTo("Full black"));

            PGNMove secondPartOfTheMove = new PGNMove(1, "", "comment", "", "move comment");

            firstPartOfTheMove.Complement(secondPartOfTheMove);
            Assert.That(firstPartOfTheMove.WhiteMove, Is.EqualTo("white move"));
            Assert.That(firstPartOfTheMove.WhiteMoveComment, Is.EqualTo("Full white move comment"));
            Assert.That(firstPartOfTheMove.BlackMove, Is.EqualTo("black move"));
            Assert.That(firstPartOfTheMove.BlackMoveComment, Is.EqualTo("Full black move comment"));
        }

        [Test]
        [ExpectedException(typeof(DifferentMovesComplementException))]
        public void GetDifferentMovesComplementException()
        {
            PGNMove firstMove = new PGNMove(1);
            PGNMove secondMove = new PGNMove(2);

            firstMove.Complement(secondMove);
        }


    }
}
