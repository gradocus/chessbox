﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore.IO.PGN;
using ChessboardGamesCore.IO.PGN.Exceptions;

namespace ChessboardGamesCoreNUnit.IO.PGNTests
{
    [TestFixture]
    class PGNTagPairsTests
    {
        [Test]
        public void AddTagPair()
        {
            PGNTagPairs tagPair = new PGNTagPairs();
            Assert.That(tagPair.ContainsTag("Some tag"), Is.False);
            tagPair.AddTagPair("Some tag", "Value of the some tag");
            Assert.That(tagPair.ContainsTag("Some tag"), Is.True);
        }

        [Test]
        public void AdditionOfExistingTagWillRewriteHisValue()
        {
            PGNTagPairs tagPair = new PGNTagPairs();
            tagPair.AddTagPair("Some tag", "Value of the some tag");
            Assert.That(tagPair.GetTagValueByName("Some tag"), Is.EqualTo("Value of the some tag"));

            tagPair.AddTagPair("Some tag", "Another value of the some tag");
            Assert.That(tagPair.GetTagValueByName("Some tag"), Is.EqualTo("Another value of the some tag"));
        }

        [Test]
        public void GetTagPairsCount()
        {
            PGNTagPairs tagPair = new PGNTagPairs();
            int tagPairsCount = tagPair.Count;

            tagPair.AddTagPair("Some tag", "Value of the some tag");
            Assert.That(tagPairsCount + 1, Is.EqualTo(tagPair.Count));

            tagPair.AddTagPair("Another tag", "Value of the another tag");
            Assert.That(tagPairsCount + 2, Is.EqualTo(tagPair.Count));
        }

        [Test]
        public void TagPairsContainsTag()
        {
            PGNTagPairs tagPair = new PGNTagPairs();
            tagPair.AddTagPair("Some tag", "Value of the some tag");
            Assert.That(tagPair.ContainsTag("Some tag"), Is.True);
        }

        [Test]
        public void AppendTwoTagPairs()
        {
            PGNTagPairs firstTagPair = new PGNTagPairs();
            firstTagPair.AddTagPair("First tagPair", "Value of the first tagPair");

            PGNTagPairs secondTagPair = new PGNTagPairs();
            secondTagPair.AddTagPair("Second tagPair", "Value of the second tagPair");

            firstTagPair.Append(secondTagPair);

            Assert.That(firstTagPair.Count, Is.EqualTo(2));
        }

        [Test]
        public void AppendTwoCrossTagPairs()
        {
            PGNTagPairs firstTagPair = new PGNTagPairs();
            firstTagPair.AddTagPair("First tagPair", "Value of the first tagPair");
            firstTagPair.AddTagPair("Second tagPair", "Value of the second tagPair");

            PGNTagPairs secondTagPair = new PGNTagPairs();
            secondTagPair.AddTagPair("Second tagPair", "Value of the second tagPair");
            secondTagPair.AddTagPair("Third tagPair", "Value of the third tagPair");

            firstTagPair.Append(secondTagPair);

            Assert.That(firstTagPair.Count, Is.EqualTo(3));
        }

        [Test]
        [ExpectedException(typeof(IncorrectPGNDataTypeException))]
        public void IncorrectPGNDataTypeExceptionOnAppendingWithNonTagPairsPGNData()
        {
            PGNTagPairs tagPair = new PGNTagPairs();
            tagPair.Append(new PGNMovetext());
        }

        [Test]
        public void TransformTagPairsToString()
        {
            PGNTagPairs tagPairs = new PGNTagPairs();
            tagPairs.AddTagPair("First tagPair", "Value of the first tagPair");
            tagPairs.AddTagPair("Second tagPair", "Value of the second tagPair");
            Assert.That(tagPairs.ToString(),
                        Is.EqualTo("[First tagPair \"Value of the first tagPair\"]\n"
                                 + "[Second tagPair \"Value of the second tagPair\"]\n")
                   );
        }
    }
}
