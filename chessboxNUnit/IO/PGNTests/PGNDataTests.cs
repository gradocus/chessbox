﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore.IO.PGN;

namespace ChessboardGamesCoreNUnit.IO.PGNTests
{
    [TestFixture]
    class PGNDataTests
    {
        [Test]
        public void ToStringReturnTextThatAddsByAddSourceLine()
        {
            string LINE_SEPARATOR = "\r\n";
            string someNonEmptyString = "Some text";
            PGNData testPGNData = new FakePGNData();

            Assert.That(testPGNData.ToString(), Is.EqualTo(String.Empty));
            testPGNData.AddSourceLine(someNonEmptyString);
            Assert.That(testPGNData.ToString(), Is.EqualTo(someNonEmptyString + LINE_SEPARATOR));
        }
    }
}
