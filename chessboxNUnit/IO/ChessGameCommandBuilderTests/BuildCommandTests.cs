﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore;
using ChessboardGamesCore.IO;

namespace ChessboardGamesCoreNUnit.IO.ChessGameCommandBuilderTests
{
    [TestFixture]
    class BuildCommandTests
    {
        [Test]
        public void BuildKingsideCastlingCommands()
        {
            ChessboardCoordinate e1 = new ChessboardCoordinate("e1");
            ChessboardCoordinate f1 = new ChessboardCoordinate("f1");
            ChessboardCoordinate g1 = new ChessboardCoordinate("g1");
            ChessboardCoordinate h1 = new ChessboardCoordinate("h1");

            IGame game = new ChessGame();
            game.Chessboard.RemovePiece(f1);
            game.Chessboard.RemovePiece(g1);

            string kingsideCastlingPGNRecord = "O-O";

            IGameCommandBuilder chessGameCommandBuilder = new ChessGameCommandBuilder(game, kingsideCastlingPGNRecord);
            Assert.That(chessGameCommandBuilder.BuildCommand(), Is.True);

            Piece whiteKing = new Piece(PieceType.King, PieceColor.White);
            Piece whiteRook = new Piece(PieceType.Rook, PieceColor.White);

            Assert.That(game.Chessboard[e1], Is.EqualTo(whiteKing));
            Assert.That(game.Chessboard[f1], Is.EqualTo(null));
            Assert.That(game.Chessboard[g1], Is.EqualTo(null));
            Assert.That(game.Chessboard[h1], Is.EqualTo(whiteRook));
            game.MovementHistory.ExecuteCommand(chessGameCommandBuilder.Command);
            Assert.That(game.Chessboard[e1], Is.EqualTo(null));
            Assert.That(game.Chessboard[f1], Is.EqualTo(whiteRook));
            Assert.That(game.Chessboard[g1], Is.EqualTo(whiteKing));
            Assert.That(game.Chessboard[h1], Is.EqualTo(null));
        }

        [Test]
        public void BuildQueensideCastlingCommands()
        {
            ChessboardCoordinate a1 = new ChessboardCoordinate("a1");
            ChessboardCoordinate c1 = new ChessboardCoordinate("c1");
            ChessboardCoordinate d1 = new ChessboardCoordinate("d1");
            ChessboardCoordinate e1 = new ChessboardCoordinate("e1");

            IGame game = new ChessGame();
            game.Chessboard.RemovePiece(new ChessboardCoordinate("b1"));
            game.Chessboard.RemovePiece(c1);
            game.Chessboard.RemovePiece(d1);

            string queensideCastlingPGNRecord = "O-O-O";

            IGameCommandBuilder chessGameCommandBuilder = new ChessGameCommandBuilder(game, queensideCastlingPGNRecord);
            Assert.That(chessGameCommandBuilder.BuildCommand(), Is.True);

            Piece whiteKing = new Piece(PieceType.King, PieceColor.White);
            Piece whiteRook = new Piece(PieceType.Rook, PieceColor.White);

            Assert.That(game.Chessboard[a1], Is.EqualTo(whiteRook));
            Assert.That(game.Chessboard[c1], Is.EqualTo(null));
            Assert.That(game.Chessboard[d1], Is.EqualTo(null));
            Assert.That(game.Chessboard[e1], Is.EqualTo(whiteKing));
            game.MovementHistory.ExecuteCommand(chessGameCommandBuilder.Command);
            Assert.That(game.Chessboard[a1], Is.EqualTo(null));
            Assert.That(game.Chessboard[c1], Is.EqualTo(whiteKing));
            Assert.That(game.Chessboard[d1], Is.EqualTo(whiteRook));
            Assert.That(game.Chessboard[e1], Is.EqualTo(null));
        }

        [Test]
        public void FailCommandBuildingOnIncorrectCastling()
        {
            IGame game = new ChessGame();

            string kingsideCastlingPGNRecord = "O-O";

            IGameCommandBuilder chessGameCommandBuilder = new ChessGameCommandBuilder(game, kingsideCastlingPGNRecord);
            Assert.That(chessGameCommandBuilder.BuildCommand(), Is.False);
        }

        [Test]
        public void BuildSimpleMoveCommand()
        {
            IGame chessGame = new ChessGame();
            string whiteMoveE2E4PGNRecord = "e4";
            ChessboardCoordinate e4 =
                new ChessboardCoordinate(whiteMoveE2E4PGNRecord);

            IGameCommandBuilder chessGameCommandBuilder = new ChessGameCommandBuilder(chessGame, whiteMoveE2E4PGNRecord);
            Assert.That(chessGameCommandBuilder.BuildCommand(), Is.True);
            Assert.That(chessGame.Chessboard[e4], Is.EqualTo(null));
            chessGame.MovementHistory.ExecuteCommand(chessGameCommandBuilder.Command);
            Assert.That(chessGame.Chessboard[e4], Is.Not.EqualTo(null));
            Assert.That(chessGame.Chessboard[new ChessboardCoordinate("e2")], Is.EqualTo(null));
        }

        [Test]
        public void CantChoisePiecesThatCanDoThisMove()
        {
            IGame chessGame = new ChessGame();
            string whiteMoveToE5PGNRecord = "e5";
            
            IGameCommandBuilder chessGameCommandBuilder = new ChessGameCommandBuilder(chessGame, whiteMoveToE5PGNRecord);
            Assert.That(chessGameCommandBuilder.BuildCommand(), Is.False);
        }

        [Test]
        public void SelectOneOfTwoPiecesForMove()
        {
            IGame chessGame = new ChessGame();
            chessGame.Chessboard.SetPiece(new Piece(PieceType.Rook, PieceColor.White), new ChessboardCoordinate("a4"));
            chessGame.Chessboard.SetPiece(new Piece(PieceType.Rook, PieceColor.White), new ChessboardCoordinate("h4"));
            string whiteRookMoveFromH4ToE4PGNRecord = "Rhe4";

            ChessboardCoordinate e4 = new ChessboardCoordinate("e4");

            IGameCommandBuilder chessGameCommandBuilder = new ChessGameCommandBuilder(chessGame, whiteRookMoveFromH4ToE4PGNRecord);
            Assert.That(chessGameCommandBuilder.BuildCommand(), Is.True);
            Assert.That(chessGame.Chessboard[e4], Is.EqualTo(null));
            chessGame.MovementHistory.ExecuteCommand(chessGameCommandBuilder.Command);
            Assert.That(chessGame.Chessboard[e4], Is.Not.EqualTo(null));
            Assert.That(chessGame.Chessboard[new ChessboardCoordinate("h4")], Is.EqualTo(null));
            Assert.That(chessGame.Chessboard[new ChessboardCoordinate("a4")], Is.Not.EqualTo(null));
        }

        [Test]
        public void CantChoiseOnePieceToDoThisMove()
        {
            IGame chessGame = new ChessGame();
            chessGame.Chessboard.SetPiece(new Piece(PieceType.Rook, PieceColor.White), new ChessboardCoordinate("a4"));
            chessGame.Chessboard.SetPiece(new Piece(PieceType.Rook, PieceColor.White), new ChessboardCoordinate("h4"));
            string whiteRookMoveFromX4ToE4PGNRecord = "Re4";

            ChessboardCoordinate e4 = new ChessboardCoordinate("e4");

            IGameCommandBuilder chessGameCommandBuilder = new ChessGameCommandBuilder(chessGame, whiteRookMoveFromX4ToE4PGNRecord);
            Assert.That(chessGameCommandBuilder.BuildCommand(), Is.False);
        }

        [Test]
        public void BuildCompositeCommandPawnMoveAndPromote()
        {
            Chessboard chessboard = new Chessboard();

            ChessboardCoordinate a7 =  new ChessboardCoordinate("a7");
            ChessboardCoordinate a8 =  new ChessboardCoordinate("a8");

            chessboard.SetPiece(new Piece(PieceType.Pawn, PieceColor.White), a7);
            
            ChessGame game = new ChessGame(chessboard, PieceColor.White, 0, 1);
            string whiteMoveAndPromoteRecord = "a8Q";
            ChessGameCommandBuilder commandBuilder = new ChessGameCommandBuilder(game, whiteMoveAndPromoteRecord);
            Assert.That(commandBuilder.BuildCommand(), Is.True);
            game.MovementHistory.ExecuteCommand(commandBuilder.Command);
            Assert.That(game.Chessboard[a8].Type, Is.EqualTo(PieceType.Queen));
        }

        [Test]
        public void ChoiceForNextMoveWithoutCheckForKing()
        {
            Chessboard chessboard = new Chessboard();

            chessboard.SetPiece(new Piece(PieceType.Rook, PieceColor.Black), new ChessboardCoordinate("a1"));
            chessboard.SetPiece(new Piece(PieceType.Rook, PieceColor.White), new ChessboardCoordinate("e1"));
            chessboard.SetPiece(new Piece(PieceType.King, PieceColor.White), new ChessboardCoordinate("h1"));
            chessboard.SetPiece(new Piece(PieceType.Rook, PieceColor.White), new ChessboardCoordinate("g4"));
            chessboard.SetPiece(new Piece(PieceType.Pawn, PieceColor.Black), new ChessboardCoordinate("e4"));

            ChessGame game = new ChessGame(chessboard, PieceColor.White, 0, 1);
            string whiteMoveRecord = "Rxe4";
            ChessGameCommandBuilder commandBuilder = new ChessGameCommandBuilder(game, whiteMoveRecord);
            Assert.That(commandBuilder.BuildCommand(), Is.True);
            game.MovementHistory.ExecuteCommand(commandBuilder.Command);
            Assert.That(game.Chessboard[new ChessboardCoordinate("e4")].Type, Is.EqualTo(PieceType.Rook));
            Assert.That(game.Chessboard[new ChessboardCoordinate("g4")], Is.EqualTo(null));
        }
    }
}
