﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using NUnit.Framework;
using ChessboardGamesCore.IO;
using ChessboardGamesCore.IO.PGN;
using ChessboardGamesCoreNUnit.IO.PGNTests;

namespace ChessboardGamesCoreNUnit.IO
{
    [TestFixture]
    class ChessGameBuilderTests
    {
        [SetUp]
        public void SetUp()
        {
            this.fakeStreamReader = new FakeStreamReader(
                new string[] {
                    "[Event \"F/S Return Match\"]",
                    "[Site \"Belgrade, Serbia Yugoslavia|JUG\"]",
                    "[Date \"1992.11.04\"]",
                    "[Round \"29\"]",
                    "[White \"Fischer, Robert J.\"]",
                    "[Black \"Spassky, Boris V.\"]",
                    "[Result \"1/2-1/2\"]",
                    "",
                    "1. e4 e5 2. Nf3 Nc6 3. Bb5 {This opening is called the Ruy Lopez.} 3... a6",
                    "4. Ba4 Nf6 5. O-O Be7 6. Re1 b5 7. Bb3 d6 8. c3 O-O 9. h3 Nb8  10. d4 Nbd7",
                    "11. c4 c6 12. cxb5 axb5 13. Nc3 Bb7 14. Bg5 b4 15. Nb1 h6 16. Bh4 c5 17. dxe5",
                    "Nxe4 18. Bxe7 Qxe7 19. exd6 Qf6 20. Nbd2 Nxd6 21. Nc4 Nxc4 22. Bxc4 Nb6",
                    "23. Ne5 Rae8 24. Bxf7+ Rxf7 25. Nxf7 Rxe1+ 26. Qxe1 Kxf7 27. Qe3 Qg5 28. Qxg5",
                    "hxg5 29. b3 Ke6 30. a3 Kd6 31. axb4 cxb4 32. Ra5 Nd5 33. f3 Bc8 34. Kf2 Bf5",
                    "35. Ra7 g6 36. Ra6+ Kc5 37. Ke1 Nf4 38. g3 Nxh3 39. Kd2 Kb5 40. Rd6 Kc5 41. Ra6",
                    "Nf2 42. g4 Bd3 43. Re6 1/2-1/2",
                    "",
                    "[Event \"Cto Juvenil Cataluсa, Hospitalet\"]",
                    "[Site \"?\"]",
                    "[Date \"1991.??.??\"]",
                    "[Round \"?\"]",
                    "[White \"Guerrero Sanmarti, Richard\"]",
                    "[Black \"Perales Galino, Eduard\"]",
                    "[Result \"1-0\"]",
                    "[ECO \"B78\"]",
                    "[PlyCount \"63\"]",
                    "[EventDate \"1991.??.??\"]",
                    "",
                    "1. e4 c5 2. Nf3 d6 3. Nc3 Nc6 4. d4 cxd4 5. Nxd4 g6 6. Be3 Bg7 7. f3 Nf6 8. Qd2",
                    "O-O 9. Bc4 Bd7 10. O-O-O Qb8 11. g4 b5 12. Bd5 b4 13. Nce2 Nxd5 14. Nxc6 Bxc6",
                    "15. exd5 Bb5 16. Bd4 Bxe2 17. Qxe2 Qc7 18. Bxg7 Kxg7 19. Kb1 Rfc8 20. h4 Re8",
                    "21. h5 Rac8 22. Qd2 Rh8 23. hxg6 fxg6 24. Qd4+ Kf7 25. Qe4 Ke8 26. Rde1 a5 27.",
                    "f4 Qd7 28. f5 gxf5 29. gxf5 Kd8 30. Qd4 Qe8 31. Qb6+ Rc7 32. f6Q 1-0",
                    ""
                });
        }

        private StreamReader fakeStreamReader;

        [Test]
        public void BuildGameTest()
        {
            PGNGameReader fakeReader = new PGNGameReader(fakeStreamReader);
            Assert.That(fakeReader.ReadNextGame(), Is.True);

            ChessGameBuilder gameBuilder = new ChessGameBuilder(fakeReader.GameRecord);
            Assert.That(gameBuilder.BuildGame(), Is.True);
            Assert.That(gameBuilder.Game.MovementHistory.Count, Is.EqualTo(85)); // 42 * 2 + 1 = 85.
        }

        [Test]
        public void CantBuildGameThatContainsIncorrectMoves()
        {
            PGNGameReader fakeReader = new PGNGameReader(fakeStreamReader);
            Assert.That(fakeReader.ReadNextGame(), Is.True);
            Assert.That(fakeReader.ReadNextGame(), Is.True);
            // Incorrect move is "32. f6Q 1-0".
            ChessGameBuilder gameBuilder = new ChessGameBuilder(fakeReader.GameRecord);
            Assert.That(gameBuilder.BuildGame(), Is.False);
        }
    }
}
