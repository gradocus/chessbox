﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore.IO.FEN;

namespace ChessboardGamesCoreNUnit.IO.FENTests
{
    [TestFixture]
    public class FENPiecePlacementTests
    {
        FENPiecePlacement piecePlacement;

        [SetUp]
        public void SetUpPiecePlacementTests()
        {
            this.piecePlacement = new FENPiecePlacement();
        }

        [Test]
        public void CheckFENPiecePlacementCreationFromString()
        {
            this.piecePlacement = new FENPiecePlacement("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR");

            Assert.That(this.piecePlacement[0, 0], Is.EqualTo('R')); // A1
            Assert.That(this.piecePlacement[0, 1], Is.EqualTo('P')); // A2
            Assert.That(this.piecePlacement[0, 3], Is.EqualTo(FENPiecePlacement.EMPTY_PLACE)); // A4
            Assert.That(this.piecePlacement[0, 6], Is.EqualTo('p')); // A7
            Assert.That(this.piecePlacement[0, 7], Is.EqualTo('r')); // A8
        }

        [Test]
        public void CheckGettingFENPiecePlacementString()
        {
            this.piecePlacement = new FENPiecePlacement("rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR");
            Assert.That(this.piecePlacement.ToString(), Is.EqualTo("rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR"));
        }

        [Test]
        public void CheckChangingFENPiecePlacementString()
        {
            Assert.That(this.piecePlacement.ToString(), Is.EqualTo("8/8/8/8/8/8/8/8"));

            this.piecePlacement[0, 0] = 'p'; // A1
            this.piecePlacement[3, 0] = 'p'; // D1
            Assert.That(this.piecePlacement.ToString(), Is.EqualTo("8/8/8/8/8/8/8/p2p4"));
        }
    }
}
