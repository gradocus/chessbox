﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore;
using ChessboardGamesCore.IO.FEN;

namespace ChessboardGamesCoreNUnit.IO.FENTests
{
    [TestFixture]
    public class FENParserTests
    {
        [Test]
        public void CheckParsingDefaultFENString()
        {
            FENParser FENParser = new FENParser();
            FENData fenData = FENParser.GetFENDataFromFENString("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");

            Assert.That(fenData.PiecePlacement.ToString(), Is.EqualTo("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR"));
            Assert.That(fenData.ActiveColor, Is.EqualTo(PieceColor.White));
            Assert.That(fenData.CastlingAvailability.ToString(), Is.EqualTo("KQkq"));
            Assert.That(fenData.EnPassantTargetSquare, Is.EqualTo("-"));
            Assert.That(fenData.HalfmoveClock, Is.EqualTo(0));
            Assert.That(fenData.FullmoveClock, Is.EqualTo(1));
        }

        [Test]
        public void CheckParsingNonDefaultFENString()
        {
            FENParser FENParser = new FENParser();
            FENData fenData = FENParser.GetFENDataFromFENString("rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq c6 0 2");

            Assert.That(fenData.PiecePlacement.ToString(), Is.EqualTo("rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR"));
            Assert.That(fenData.ActiveColor, Is.EqualTo(PieceColor.White));
            Assert.That(fenData.CastlingAvailability.ToString(), Is.EqualTo("KQkq"));
            Assert.That(fenData.EnPassantTargetSquare, Is.EqualTo("c6"));
            Assert.That(fenData.HalfmoveClock, Is.EqualTo(0));
            Assert.That(fenData.FullmoveClock, Is.EqualTo(2));
        }
    }
}
