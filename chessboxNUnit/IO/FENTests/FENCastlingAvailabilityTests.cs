﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore.IO.FEN;

namespace ChessboardGamesCoreNUnit.IO.FENTests
{
    [TestFixture]
    public class FENCastlingAvailabilityTests
    {
        FENCastlingAvailability castlingAvailability;

        [SetUp]
        public void SetUpCastlingAvailabilityTests()
        {
            this.castlingAvailability = new FENCastlingAvailability();
        }

        [Test]
        public void CheckCreationAllCastlingAvailabilityAsNonAvailable()
        {
            this.castlingAvailability = new FENCastlingAvailability("-");

            Assert.That(
                this.castlingAvailability.BlackCanCastleKingside
                    && this.castlingAvailability.BlackCanCastleQueenside
                    && this.castlingAvailability.WhiteCanCastleKingside
                    && this.castlingAvailability.WhiteCanCastleQueenside,
                Is.False);
        }

        [Test]
        public void CheckCreationDefaultCastlingAvailability()
        {
            this.castlingAvailability = new FENCastlingAvailability("KQkq");

            Assert.That(
                this.castlingAvailability.BlackCanCastleKingside
                    && this.castlingAvailability.BlackCanCastleQueenside
                    && this.castlingAvailability.WhiteCanCastleKingside
                    && this.castlingAvailability.WhiteCanCastleQueenside,
                Is.True);
        }

        [Test]
        public void CheckCreationNonDefaultCastlingAvailability()
        {
            this.castlingAvailability = new FENCastlingAvailability("Kq");

            Assert.That(
                !this.castlingAvailability.BlackCanCastleKingside
                    && this.castlingAvailability.BlackCanCastleQueenside
                    && this.castlingAvailability.WhiteCanCastleKingside
                    && !this.castlingAvailability.WhiteCanCastleQueenside,
                Is.True);
        }

        [Test]
        public void CheckEmptyCastlingAvailabilityToString()
        {
            this.castlingAvailability = new FENCastlingAvailability("-");

            Assert.That(this.castlingAvailability.ToString(), Is.EqualTo("-"));
        }

        [Test]
        public void CheckDefaultCastlingAvailabilityToString()
        {
            this.castlingAvailability = new FENCastlingAvailability("KQkq");

            Assert.That(this.castlingAvailability.ToString(), Is.EqualTo("KQkq"));
        }

        [Test]
        public void CheckChangingCastlingAvailability()
        {
            this.castlingAvailability.WhiteCanCastleKingside = false;
            this.castlingAvailability.WhiteCanCastleQueenside = true;
            this.castlingAvailability.BlackCanCastleKingside = false;
            this.castlingAvailability.BlackCanCastleQueenside = true;

            Assert.That(this.castlingAvailability.ToString(), Is.EqualTo("Qq"));
        }
    }
}
