﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;

using ChessboardGamesCore;
using ChessboardGamesCore.Exceptions;

namespace ChessboardGamesCoreNUnit.ChessboardTests
{
    [TestFixture]
    class ComparisonTests
    {
        [SetUp]
        public void SetUp()
        {
            this.firstEmptyChessboard = new Chessboard();
            this.secondEmptyChessboard = new Chessboard();
        }

        protected Chessboard firstEmptyChessboard;
        protected Chessboard secondEmptyChessboard;

        [Test]
        public void EmptyChessboardsAreEquals()
        {
            Assert.That(this.firstEmptyChessboard, Is.EqualTo(this.secondEmptyChessboard));
            Assert.That(this.firstEmptyChessboard == this.secondEmptyChessboard, Is.True);
        }

        [Test]
        public void ChessboardsWithTheSamePiecesOnTheSameSquaresAreEquals()
        {
            Piece blackPawn = new Piece(PieceType.Pawn, PieceColor.Black);
            ChessboardCoordinate blackPawnCoordinate = new ChessboardCoordinate("a2");
            this.firstEmptyChessboard.SetPiece(blackPawn, blackPawnCoordinate);

            Piece otherBlackPawn = new Piece(PieceType.Pawn, PieceColor.Black);
            this.secondEmptyChessboard.SetPiece(otherBlackPawn, blackPawnCoordinate);

            Assert.That(this.firstEmptyChessboard == this.secondEmptyChessboard, Is.True);
        }

        [Test]
        public void ChessboardsWithTheSamePiecesOnAnotherSquaresAreNotEquals()
        {
            Piece blackPawn = new Piece(PieceType.Pawn, PieceColor.Black);
            ChessboardCoordinate blackPawnCoordinate = new ChessboardCoordinate("a2");
            this.firstEmptyChessboard.SetPiece(blackPawn, blackPawnCoordinate);

            Piece otherBlackPawn = new Piece(PieceType.Pawn, PieceColor.Black);
            ChessboardCoordinate otherBlackPawnCoordinate = new ChessboardCoordinate("b2");
            this.secondEmptyChessboard.SetPiece(otherBlackPawn, otherBlackPawnCoordinate);

            Assert.That(this.firstEmptyChessboard != this.secondEmptyChessboard, Is.True);
        }

        [Test]
        public void ChessboardsWithAnotherPiecesOnTheSameSquaresAreNotEquals()
        {
            Piece blackPawn = new Piece(PieceType.Pawn, PieceColor.Black);
            ChessboardCoordinate blackPawnCoordinate = new ChessboardCoordinate("a2");
            this.firstEmptyChessboard.SetPiece(blackPawn, blackPawnCoordinate);

            Piece blackBishop = new Piece(PieceType.Bishop, PieceColor.Black);
            this.secondEmptyChessboard.SetPiece(blackBishop, blackPawnCoordinate);

            Assert.That(this.firstEmptyChessboard != this.secondEmptyChessboard, Is.True);
        }
    }
}
