﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore;
using ChessboardGamesCore.Exceptions;

namespace ChessboardGamesCoreNUnit.ChessboardTests
{
    [TestFixture]
    class AnyValueAccessTests
    {
        [Test]
        [ExpectedException(typeof(ChessboardCoordinateOutOfRangeException))]
        public void OutOfRangeExceptionOnUseAnyValueCoordinate()
        {
            Chessboard board = new Chessboard();

            ChessboardCoordinate anyFileCoordinate
                = new ChessboardCoordinate(ChessboardCoordinate.ANY_VALUE, 4);

            if (board[anyFileCoordinate] != null)
            {
                // Do something...
            }
        }
    }
}
