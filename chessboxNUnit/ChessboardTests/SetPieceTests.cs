﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;

using ChessboardGamesCore;
using ChessboardGamesCore.Exceptions;

namespace ChessboardGamesCoreNUnit.ChessboardTests
{
    [TestFixture]
    class SetPieceTests
    {
        [Test]
        public void SetPiece()
        {
            Chessboard chessboard = new Chessboard();
            ChessboardCoordinate a1 = new ChessboardCoordinate('a', 1);

            Assert.That(chessboard[a1], Is.EqualTo(null));
            chessboard.SetPiece(new Piece(PieceType.Pawn, PieceColor.White), a1);
            Assert.That(chessboard[a1], Is.Not.EqualTo(null));
        }

        [Test]
        [ExpectedException(typeof(SetPieceOnOtherPieceException))]
        public void SetPieceOnOtherPieceException()
        {
            Chessboard chessboard = new Chessboard();
            ChessboardCoordinate a1 = new ChessboardCoordinate('a', 1);

            chessboard.SetPiece(new Piece(PieceType.Pawn, PieceColor.White), a1);
            chessboard.SetPiece(new Piece(PieceType.Pawn, PieceColor.Black), a1);
        }

        [Test]
        [ExpectedException(typeof(SetNullPieceOnChessboardException))]
        public void SetNullPieceException()
        {
            Chessboard chessboard = new Chessboard();
            ChessboardCoordinate a1 = new ChessboardCoordinate('a', 1);

            chessboard.SetPiece(null, a1);
        }

        [Test]
        public void AddPieceCoordinateToPieceCoordinateArray()
        {
            Chessboard board = new Chessboard();
            ChessboardCoordinate a2 = new ChessboardCoordinate("a2");
            Piece whitePawn = new Piece(PieceType.Pawn, PieceColor.White);

            Assert.That(board.GetPiecesCoordinateArray(whitePawn).Length, Is.EqualTo(0));
            board.SetPiece(whitePawn, a2);
            Assert.That(board.GetPiecesCoordinateArray(whitePawn).Length, Is.EqualTo(1));
            Assert.That(board.GetPiecesCoordinateArray(whitePawn).Contains(a2), Is.True);
        }
    }
}
