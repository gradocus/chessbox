﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;

using ChessboardGamesCore;
using ChessboardGamesCore.Exceptions;

namespace ChessboardGamesCoreNUnit.ChessboardTests
{
    [TestFixture]
    class RemovePieceTests
    {
        [Test]
        public void RemovePiece()
        {
            Chessboard chessboard = new Chessboard();

            ChessboardCoordinate a1 = new ChessboardCoordinate('a', 1);
            chessboard.SetPiece(new Piece(PieceType.Pawn, PieceColor.White), a1);

            Assert.That(chessboard[a1], Is.Not.EqualTo(null));
            chessboard.RemovePiece(a1);
            Assert.That(chessboard[a1], Is.EqualTo(null));
        }

        [Test]
        [ExpectedException(typeof(RemoveNullPieceFromChessboardException))]
        public void RemoveNullPieceException()
        {
            Chessboard chessboard = new Chessboard();
            ChessboardCoordinate a1 = new ChessboardCoordinate('a', 1);

            Assert.That(chessboard[a1], Is.EqualTo(null));
            chessboard.RemovePiece(a1);
        }

        [Test]
        public void RemovePieceCoordinateFromPieceCoordinateArray()
        {
            Chessboard board = new Chessboard();
            ChessboardCoordinate a2 = new ChessboardCoordinate("a2");
            Piece whitePawn = new Piece(PieceType.Pawn, PieceColor.White);

            board.SetPiece(whitePawn, a2);
            Assert.That(board.GetPiecesCoordinateArray(whitePawn).Length, Is.EqualTo(1));
            board.RemovePiece(a2);
            Assert.That(board.GetPiecesCoordinateArray(whitePawn).Length, Is.EqualTo(0));
        }
    }
}
