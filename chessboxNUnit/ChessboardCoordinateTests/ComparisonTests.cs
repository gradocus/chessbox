﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore;

namespace ChessboardGamesCoreNUnit.ChessboardCoordinateTests
{
    class ComparisonTests
    {
        [Test]
        public void CordinateIsEqualToTheSameCoordinate()
        {
            ChessboardCoordinate firstCoordinate = new ChessboardCoordinate("a1");
            ChessboardCoordinate secondCoordinate = new ChessboardCoordinate("a1");

            Assert.That(firstCoordinate, Is.EqualTo(secondCoordinate));
            Assert.That(firstCoordinate == secondCoordinate, Is.True);
            Assert.That(firstCoordinate != secondCoordinate, Is.False);
        }

        [Test]
        public void CoordinatesWithDifferentRanksAreNotEquals()
        {
            ChessboardCoordinate firstCoordinate = new ChessboardCoordinate("a1");
            ChessboardCoordinate secondCoordinate = new ChessboardCoordinate("a2");

            Assert.That(firstCoordinate == secondCoordinate, Is.False);
        }

        [Test]
        public void CoordinatesWithDifferentFilesAreNotEquals()
        {
            ChessboardCoordinate firstCoordinate = new ChessboardCoordinate("a1");
            ChessboardCoordinate secondCoordinate = new ChessboardCoordinate("b1");

            Assert.That(firstCoordinate == secondCoordinate, Is.False);
        }

        [Test]
        public void CoordinatesWithDifferentFilesAndRanksAreNotEquals()
        {
            ChessboardCoordinate firstCoordinate = new ChessboardCoordinate("a1");
            ChessboardCoordinate secondCoordinate = new ChessboardCoordinate("b2");

            Assert.That(firstCoordinate == secondCoordinate, Is.False);
        }

        [Test]
        public void CompareWithAnyValues()
        {
            ChessboardCoordinate firstCoordinate = new ChessboardCoordinate('a', 1);
            ChessboardCoordinate secondCoordinate
                = new ChessboardCoordinate(ChessboardCoordinate.ANY_VALUE, 0);
            ChessboardCoordinate thirdCoordinate = new ChessboardCoordinate('b', 1);
            ChessboardCoordinate fourthCoordinate
                = new ChessboardCoordinate('a', ChessboardCoordinate.ANY_VALUE);

            Assert.That(firstCoordinate == secondCoordinate, Is.True);
            Assert.That(secondCoordinate == thirdCoordinate, Is.True);
            Assert.That(firstCoordinate == secondCoordinate, Is.True);
            Assert.That(firstCoordinate == fourthCoordinate, Is.True);
        }
    }
}
