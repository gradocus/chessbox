﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore;
using ChessboardGamesCore.Exceptions;

namespace ChessboardGamesCoreNUnit.ChessboardCoordinateTests
{
    [TestFixture]
    class CreationTests
    {
        [Test]
        public void FromCharAndInt()
        {
            ChessboardCoordinate coordinate = new ChessboardCoordinate('b', 2);

            Assert.That(coordinate.File, Is.EqualTo(1));
            Assert.That(coordinate.Rank, Is.EqualTo(1));
        }

        [Test]
        public void FromIntAndInt()
        {
            ChessboardCoordinate coordinate = new ChessboardCoordinate(1, 1);

            Assert.That(coordinate.File, Is.EqualTo(1));
            Assert.That(coordinate.Rank, Is.EqualTo(1));
        }

        [Test]
        [ExpectedException(typeof(ChessboardCoordinateOutOfRangeException))]
        public void UsingFileValueMoreThan7()
        {
            ChessboardCoordinate coordinate = new ChessboardCoordinate(8, 0);
        }

        [Test]
        [ExpectedException(typeof(ChessboardCoordinateOutOfRangeException))]
        public void UsingRankValueMoreThan7()
        {
            ChessboardCoordinate coordinate = new ChessboardCoordinate(0, 8);
        }

        [Test]
        [ExpectedException(typeof(ChessboardCoordinateOutOfRangeException))]
        public void UsingFileAndRankValuesMoreThan7()
        {
            ChessboardCoordinate coordinate = new ChessboardCoordinate(9, 10);
        }
    }
}
