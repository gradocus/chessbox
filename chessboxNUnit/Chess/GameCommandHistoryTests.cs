﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore;
using ChessboardGamesCoreNUnit.Chess.Commands;

namespace ChessboardGamesCoreNUnit.Chess
{
    [TestFixture]
    class GameCommandHistoryTests
    {
        [SetUp]
        protected void SetUp()
        {
            this.game = new ChessGame();
        }

        private ChessGame game;

        [Test]
        public void ExecuteCommand()
        {
            FakeCommand fakeCommand = new FakeCommand();

            Assert.That(fakeCommand.IsApplied, Is.False);
            game.MovementHistory.ExecuteCommand(fakeCommand);
            Assert.That(fakeCommand.IsApplied, Is.True);
        }

        [Test]
        public void UndoCommand()
        {
            FakeCommand fakeCommand = new FakeCommand();
            game.MovementHistory.ExecuteCommand(fakeCommand);

            Assert.That(fakeCommand.IsApplied, Is.True);
            game.MovementHistory.Undo();
            Assert.That(fakeCommand.IsApplied, Is.False);
        }

        [Test]
        public void RedoCommand()
        {
            FakeCommand fakeCommand = new FakeCommand();
            game.MovementHistory.ExecuteCommand(fakeCommand);
            game.MovementHistory.Undo();

            Assert.That(fakeCommand.IsApplied, Is.False);
            game.MovementHistory.Redo();
            Assert.That(fakeCommand.IsApplied, Is.True);
        }

        [Test]
        public void IncreasePositionAndCommandCountOnExecuteCommand()
        {
            int count = game.MovementHistory.Count;
            int position = game.MovementHistory.Position;

            FakeCommand fakeCommand = new FakeCommand();
            game.MovementHistory.ExecuteCommand(fakeCommand);

            Assert.That(game.MovementHistory.Count, Is.EqualTo(count + 1));
            Assert.That(game.MovementHistory.Position, Is.EqualTo(position + 1));
        }

        [Test]
        public void DecreasePositionAndSaveCommandCountOnUndo()
        {
            FakeCommand fakeCommand = new FakeCommand();
            game.MovementHistory.ExecuteCommand(fakeCommand);

            int count = game.MovementHistory.Count;
            int position = game.MovementHistory.Position;

            game.MovementHistory.Undo();

            Assert.That(game.MovementHistory.Count, Is.EqualTo(count));
            Assert.That(game.MovementHistory.Position, Is.EqualTo(position - 1));
        }

        [Test]
        public void IncreasePositionAndSaveCommandCountOnRedo()
        {
            FakeCommand fakeCommand = new FakeCommand();
            game.MovementHistory.ExecuteCommand(fakeCommand);
            game.MovementHistory.Undo();

            int count = game.MovementHistory.Count;
            int position = game.MovementHistory.Position;

            game.MovementHistory.Redo();

            Assert.That(game.MovementHistory.Count, Is.EqualTo(count));
            Assert.That(game.MovementHistory.Position, Is.EqualTo(position + 1));
        }

        [Test]
        public void RemoveAllCancelledCommandsOnExecuteCommand()
        {
            FakeCommand fakeCommand = new FakeCommand();
            game.MovementHistory.ExecuteCommand(fakeCommand);
            game.MovementHistory.ExecuteCommand(fakeCommand);
            game.MovementHistory.ExecuteCommand(fakeCommand);
            game.MovementHistory.ExecuteCommand(fakeCommand);
            game.MovementHistory.Undo();
            game.MovementHistory.Undo();

            Assert.That(game.MovementHistory.Count, Is.EqualTo(4));
            Assert.That(game.MovementHistory.Position, Is.EqualTo(2));

            game.MovementHistory.ExecuteCommand(fakeCommand);

            Assert.That(game.MovementHistory.Count, Is.EqualTo(3));
            Assert.That(game.MovementHistory.Position, Is.EqualTo(3));
        }
    }
}
