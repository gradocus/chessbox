﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore;

namespace ChessboardGamesCoreNUnit.Chess
{
    [TestFixture]
    class ChessGameTests
    {
        [Test]
        public void CreateChessGameWithNewGameState()
        {
            ChessGame chessGame = new ChessGame();

            Assert.That(chessGame.HalfmoveClock, Is.EqualTo(0));
            Assert.That(chessGame.FullmoveClock, Is.EqualTo(1));
            Assert.That(chessGame.NextMoveColor, Is.EqualTo(PieceColor.White));

            Chessboard startBoardState = new Chessboard();
            startBoardState.SetPiece(new Piece(PieceType.Rook,    PieceColor.White), new ChessboardCoordinate("A1"));
            startBoardState.SetPiece(new Piece(PieceType.Knight,  PieceColor.White), new ChessboardCoordinate("B1"));
            startBoardState.SetPiece(new Piece(PieceType.Bishop,  PieceColor.White), new ChessboardCoordinate("C1"));
            startBoardState.SetPiece(new Piece(PieceType.Queen,   PieceColor.White), new ChessboardCoordinate("D1"));
            startBoardState.SetPiece(new Piece(PieceType.King,    PieceColor.White), new ChessboardCoordinate("E1"));
            startBoardState.SetPiece(new Piece(PieceType.Bishop,  PieceColor.White), new ChessboardCoordinate("F1"));
            startBoardState.SetPiece(new Piece(PieceType.Knight,  PieceColor.White), new ChessboardCoordinate("G1"));
            startBoardState.SetPiece(new Piece(PieceType.Rook,    PieceColor.White), new ChessboardCoordinate("H1"));

            for (int file = 0; file < 8; file++)
            {
                startBoardState.SetPiece(new Piece(PieceType.Pawn, PieceColor.White), new ChessboardCoordinate(file, 1));
                startBoardState.SetPiece(new Piece(PieceType.Pawn, PieceColor.Black), new ChessboardCoordinate(file, 6));
            }

            startBoardState.SetPiece(new Piece(PieceType.Rook,    PieceColor.Black), new ChessboardCoordinate("A8"));
            startBoardState.SetPiece(new Piece(PieceType.Knight,  PieceColor.Black), new ChessboardCoordinate("B8"));
            startBoardState.SetPiece(new Piece(PieceType.Bishop,  PieceColor.Black), new ChessboardCoordinate("C8"));
            startBoardState.SetPiece(new Piece(PieceType.Queen,   PieceColor.Black), new ChessboardCoordinate("D8"));
            startBoardState.SetPiece(new Piece(PieceType.King,    PieceColor.Black), new ChessboardCoordinate("E8"));
            startBoardState.SetPiece(new Piece(PieceType.Bishop,  PieceColor.Black), new ChessboardCoordinate("F8"));
            startBoardState.SetPiece(new Piece(PieceType.Knight,  PieceColor.Black), new ChessboardCoordinate("G8"));
            startBoardState.SetPiece(new Piece(PieceType.Rook,    PieceColor.Black), new ChessboardCoordinate("H8"));

            Assert.That(chessGame.Chessboard, Is.EqualTo(startBoardState));
            Assert.That(chessGame.MovementHistory.Count, Is.EqualTo(0));
        }

        [Test]
        public void AllPiecesOnNewChessGameMustHaveStartingPositionCoordinateIntoMovementHistoryList()
        {
            ChessGame chessGame = new ChessGame();

            for (int file = 0; file < 8; file++)
                for (int rank = 0; rank < 8; rank++)
                {
                    ChessboardCoordinate currentCoordinate = new ChessboardCoordinate(file, rank);
                    if (chessGame.Chessboard[currentCoordinate] != null)
                    {
                        Assert.That(chessGame.Chessboard[currentCoordinate].MovementHistory.Count, Is.EqualTo(1));
                        Assert.That(chessGame.Chessboard[currentCoordinate].MovementHistory.Last(), Is.EqualTo(currentCoordinate));
                    }
                }
        }

        [Test]
        public void CreateChessGameForSomeChessboardState()
        {
            Chessboard chessboard = new Chessboard();
            ChessboardCoordinate a1 = new ChessboardCoordinate('a', 1);
            Piece whitePawn = new Piece(PieceType.Pawn, PieceColor.White);
            chessboard.SetPiece(whitePawn, a1);

            ChessGame chessGame = new ChessGame(chessboard, PieceColor.White, 0, 1);

            Assert.That(chessGame.Chessboard, Is.EqualTo(chessboard));
        }

        [Test]
        public void FalseCheckForAnyKingFromInitialPosition()
        {
            ChessGame chessGame = new ChessGame();

            Assert.That(chessGame.IsCheckFor(new Piece(PieceType.King, PieceColor.White)), Is.False);
            Assert.That(chessGame.IsCheckFor(new Piece(PieceType.King, PieceColor.Black)), Is.False);
        }

        [Test]
        public void CheckForAnyKing()
        {
            Chessboard board = new Chessboard();
            board.SetPiece(new Piece(PieceType.King, PieceColor.White), new ChessboardCoordinate("e1"));
            board.SetPiece(new Piece(PieceType.Rook, PieceColor.Black), new ChessboardCoordinate("e3"));
            board.SetPiece(new Piece(PieceType.Rook, PieceColor.White), new ChessboardCoordinate("e6"));
            board.SetPiece(new Piece(PieceType.King, PieceColor.Black), new ChessboardCoordinate("e8"));

            ChessGame chessGame = new ChessGame(board, PieceColor.White, 0, 1);

            Assert.That(chessGame.IsCheckFor(new Piece(PieceType.King, PieceColor.Black)), Is.True);
            Assert.That(chessGame.IsCheckFor(new Piece(PieceType.King, PieceColor.White)), Is.True);
        }
    }
}
