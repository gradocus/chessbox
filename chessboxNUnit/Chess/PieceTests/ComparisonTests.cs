﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore;

namespace ChessboardGamesCoreNUnit.Chess.PieceTests
{
    [TestFixture]
    class ComparisonTests
    {
        [Test]
        public void IsEqualToTheSamePiece()
        {
            Piece firstBlackPawn = new Piece(PieceType.Pawn, PieceColor.Black);
            Piece secondBlackPawn = new Piece(PieceType.Pawn, PieceColor.Black);

            Assert.That(firstBlackPawn, Is.EqualTo(secondBlackPawn));
            Assert.That(firstBlackPawn == secondBlackPawn, Is.True);
            Assert.That(firstBlackPawn != secondBlackPawn, Is.False);

            Piece whitePawn = new Piece(PieceType.Pawn, PieceColor.White);
            Piece blackBishop = new Piece(PieceType.Bishop, PieceColor.Black);

            Assert.That(firstBlackPawn, Is.Not.EqualTo(whitePawn));
            Assert.That(firstBlackPawn, Is.Not.EqualTo(blackBishop));
        }

        [Test]
        public void IsNotEqualToAnotherColorPiece()
        {
            Piece blackPawn = new Piece(PieceType.Pawn, PieceColor.Black);
            Piece whitePawn = new Piece(PieceType.Pawn, PieceColor.White);

            Assert.That(blackPawn, Is.Not.EqualTo(whitePawn));
            Assert.That(blackPawn == whitePawn, Is.False);
            Assert.That(blackPawn != whitePawn, Is.True);
        }

        [Test]
        public void IsNotEqualToAnotherTypePiece()
        {
            Piece blackPawn = new Piece(PieceType.Pawn, PieceColor.Black);
            Piece blackBishop = new Piece(PieceType.Bishop, PieceColor.Black);

            Assert.That(blackPawn, Is.Not.EqualTo(blackBishop));
            Assert.That(blackPawn == blackBishop, Is.False);
            Assert.That(blackPawn != blackBishop, Is.True);
        }

        [Test]
        public void IsNotEqualToAnotherTypeAndColorPiece()
        {
            Piece blackPawn = new Piece(PieceType.Pawn, PieceColor.Black);
            Piece whiteBishop = new Piece(PieceType.Bishop, PieceColor.White);

            Assert.That(blackPawn, Is.Not.EqualTo(whiteBishop));
            Assert.That(blackPawn == whiteBishop, Is.False);
            Assert.That(blackPawn != whiteBishop, Is.True);
        }
    }
}
