﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore;
using ChessboardGamesCore.Commands.Chess;
using ChessboardGamesCore.Behaviors;
using ChessboardGamesCore.Behaviors.Chess;

namespace ChessboardGamesCoreNUnit.Chess.PieceBehaviorTests.BishopBehaviorTests
{
    [TestFixture]
    class GetEnPassantChessboardCoordinate
    {
        [SetUp]
        public void SetUp()
        {
            this.Game = new ChessGame();
        }

        ChessGame Game;

        [Test]
        public void ItsNullForAnyCorrectMove()
        {
            ChessPieceBehavior blackBishopBehavior = new BishopBehavior(PieceColor.Black);
            ChessboardCoordinate b2 = new ChessboardCoordinate("b2");
            ChessboardCoordinate b4 = new ChessboardCoordinate("b4");
            Move whitePawnMove = new Move(this.Game, b2, b4);

            this.Game.MovementHistory.ExecuteCommand(whitePawnMove);
            this.Game.MovementHistory.ExecuteCommand(
                new Move(
                    this.Game, new ChessboardCoordinate("a7"),
                    new ChessboardCoordinate("a5")
                ));
            Assert.That(this.Game.EnPassantCoordinate, Is.Not.EqualTo(null));

            ChessboardCoordinate c1 = new ChessboardCoordinate("c1");
            ChessboardCoordinate a3 = new ChessboardCoordinate("a3");
            Move blackBishopMove = new Move(this.Game, c1, a3);
            this.Game.MovementHistory.ExecuteCommand(blackBishopMove);
            Assert.That(this.Game.EnPassantCoordinate, Is.EqualTo(null));
        }
    }
}
