﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore;
using ChessboardGamesCore.Commands.Chess;

namespace ChessboardGamesCoreNUnit.Chess.PieceBehaviorTests.BishopBehaviorTests
{
    [TestFixture]
    class CanDoCapture
    {
        [SetUp]
        public void SetUp()
        {
            this.Game = new ChessGame();
            this.whiteBishop = new Piece(PieceType.Bishop, PieceColor.White);
            this.blackBishopBehavior = this.Game.CreatePieceBehavior(whiteBishop);
        }

        ChessGame Game;
        Piece whiteBishop;
        IPieceBehavior blackBishopBehavior;

        [Test]
        public void DoBishopCapture()
        {
            ChessboardCoordinate e5 = new ChessboardCoordinate("e5");
            this.Game.Chessboard.SetPiece(this.whiteBishop, e5);

            ChessboardCoordinate c7 = new ChessboardCoordinate("c7");
            Move move = new Move(this.Game, e5, c7);
            Capture capture = new Capture(move, c7);

            Assert.That(this.blackBishopBehavior.CanDo(move), Is.True);
            Assert.That(this.blackBishopBehavior.CanDo(capture), Is.True);
        }
    }
}
