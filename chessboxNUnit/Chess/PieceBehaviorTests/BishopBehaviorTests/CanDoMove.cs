﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore;
using ChessboardGamesCore.Commands.Chess;

namespace ChessboardGamesCoreNUnit.Chess.PieceBehaviorTests.BishopBehaviorTests
{
    [TestFixture]
    class CanDoMove
    {
        [SetUp]
        public void SetUp()
        {
            this.Game = new ChessGame();
            this.whiteBishop = new Piece(PieceType.Bishop, PieceColor.White);
            this.whiteBishopBehavior = this.Game.CreatePieceBehavior(this.whiteBishop);
        }

        ChessGame Game;
        Piece whiteBishop;
        IPieceBehavior whiteBishopBehavior;

        [Test]
        public void MoveOnDiagonal()
        {
            this.Game.Chessboard.SetPiece(this.whiteBishop, new ChessboardCoordinate("d3"));

            ICommand whiteBishopFirstMove = new Move(this.Game,
                new ChessboardCoordinate("d3"),
                new ChessboardCoordinate("g6"));
            Assert.That(whiteBishopBehavior.CanDo(whiteBishopFirstMove), Is.True);

            ICommand whiteBishopSecondMove = new Move(this.Game,
                new ChessboardCoordinate("d3"),
                new ChessboardCoordinate("a6"));
            Assert.That(whiteBishopBehavior.CanDo(whiteBishopSecondMove), Is.True);
        }

        [Test]
        public void CantMoveOnDiagonalViaOtherPieces()
        {
            this.Game.Chessboard.SetPiece(this.whiteBishop, new ChessboardCoordinate("d3"));

            ICommand whiteBishopFirstMove = new Move(this.Game,
                new ChessboardCoordinate("d3"),
                new ChessboardCoordinate("g6"));
            this.Game.Chessboard.SetPiece(
                new Piece(PieceType.Pawn, PieceColor.Black),
                new ChessboardCoordinate("e4"));
            Assert.That(whiteBishopBehavior.CanDo(whiteBishopFirstMove), Is.False);

            ICommand whiteBishopSecondMove = new Move(this.Game,
                new ChessboardCoordinate("d3"),
                new ChessboardCoordinate("a6"));
            this.Game.Chessboard.SetPiece(
                new Piece(PieceType.Pawn, PieceColor.White),
                new ChessboardCoordinate("c4"));
            Assert.That(whiteBishopBehavior.CanDo(whiteBishopSecondMove), Is.False);
        }

        [Test]
        public void DoCaptureMoveOnOtherColorPiece()
        {
            ChessboardCoordinate d3 = new ChessboardCoordinate("d3");
            ChessboardCoordinate f5 = new ChessboardCoordinate("f5");

            this.Game.Chessboard.SetPiece(this.whiteBishop, d3);

            Piece blackPawn = new Piece(PieceType.Pawn, PieceColor.Black);
            this.Game.Chessboard.SetPiece(blackPawn, f5);
            Move whiteBishopMove = new Move(this.Game, d3, f5);
            ICommand whiteBishopCapture = new Capture(whiteBishopMove, f5);
            Assert.That(whiteBishopBehavior.CanDo(whiteBishopMove), Is.True);
        }

        [Test]
        public void CantDoCaptureMoveOnTheSameColorPiece()
        {
            ChessboardCoordinate d3 = new ChessboardCoordinate("d3");
            ChessboardCoordinate f5 = new ChessboardCoordinate("f5");

            this.Game.Chessboard.SetPiece(this.whiteBishop, d3);

            Piece whitePawn = new Piece(PieceType.Pawn, PieceColor.White);
            this.Game.Chessboard.SetPiece(whitePawn, f5);
            Move blackBishopMove = new Move(this.Game, d3, f5);
            ICommand blackBishopCapture = new Capture(blackBishopMove, f5);
            Assert.That(whiteBishopBehavior.CanDo(blackBishopMove), Is.False);
        }
    }
}
