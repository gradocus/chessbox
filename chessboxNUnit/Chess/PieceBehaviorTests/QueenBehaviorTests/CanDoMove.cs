﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore;
using ChessboardGamesCore.Commands.Chess;

namespace ChessboardGamesCoreNUnit.Chess.PieceBehaviorTests.QueenBehaviorTests
{
    [TestFixture]
    class CanDoMove
    {
        [SetUp]
        public void SetUp()
        {
            this.Game = new ChessGame();
            this.blackQueen = new Piece(PieceType.Queen, PieceColor.Black);
            this.blackQueenBehavior = this.Game.CreatePieceBehavior(this.blackQueen);

            this.Game.MovementHistory.ExecuteCommand(
                new Move(
                    this.Game, new ChessboardCoordinate("h2"),
                    new ChessboardCoordinate("h3")
                ));
        }

        ChessGame Game;
        Piece blackQueen;
        IPieceBehavior blackQueenBehavior;

        [Test]
        public void MoveOnDiagonal()
        {
            this.Game.Chessboard.SetPiece(this.blackQueen, new ChessboardCoordinate("d3"));

            ICommand blackQueenFirstMove = new Move(this.Game,
                new ChessboardCoordinate("d3"),
                new ChessboardCoordinate("g6"));
            Assert.That(blackQueenBehavior.CanDo(blackQueenFirstMove), Is.True);

            ICommand blackQueenSecondMove = new Move(this.Game,
                new ChessboardCoordinate("d3"),
                new ChessboardCoordinate("a6"));
            Assert.That(blackQueenBehavior.CanDo(blackQueenSecondMove), Is.True);
        }


        [Test]
        public void MoveOnHorizontalAndVertical()
        {
            ChessboardCoordinate e4 = new ChessboardCoordinate("e4");
            this.Game.Chessboard.SetPiece(this.blackQueen, e4);

            ICommand blackQueenFirstMove = new Move(this.Game, e4,
                new ChessboardCoordinate("a4"));
            Assert.That(blackQueenBehavior.CanDo(blackQueenFirstMove), Is.True);

            ICommand blackQueenSecondMove = new Move(this.Game, e4,
                new ChessboardCoordinate("h4"));
            Assert.That(blackQueenBehavior.CanDo(blackQueenSecondMove), Is.True);

            ICommand blackQueenThirdMove = new Move(this.Game, e4,
                new ChessboardCoordinate("e3"));
            Assert.That(blackQueenBehavior.CanDo(blackQueenThirdMove), Is.True);

            ICommand blackQueenFourthMove = new Move(this.Game, e4,
                new ChessboardCoordinate("e6"));
            Assert.That(blackQueenBehavior.CanDo(blackQueenFourthMove), Is.True);
        }

        [Test]
        public void CantMoveOnDiagonalViaOtherPieces()
        {
            this.Game.Chessboard.SetPiece(this.blackQueen, new ChessboardCoordinate("d3"));

            ICommand blackQueenFirstMove = new Move(this.Game,
                new ChessboardCoordinate("d3"),
                new ChessboardCoordinate("g6"));
            this.Game.Chessboard.SetPiece(
                new Piece(PieceType.Pawn, PieceColor.Black),
                new ChessboardCoordinate("e4"));
            Assert.That(blackQueenBehavior.CanDo(blackQueenFirstMove), Is.False);

            ICommand blackQueenSecondMove = new Move(this.Game,
                new ChessboardCoordinate("d3"),
                new ChessboardCoordinate("a6"));
            this.Game.Chessboard.SetPiece(
                new Piece(PieceType.Pawn, PieceColor.White),
                new ChessboardCoordinate("c4"));
            Assert.That(blackQueenBehavior.CanDo(blackQueenSecondMove), Is.False);
        }

        [Test]
        public void DoCaptureMoveOnOtherColorPiece()
        {
            ChessboardCoordinate d3 = new ChessboardCoordinate("d3");
            ChessboardCoordinate f5 = new ChessboardCoordinate("f5");

            this.Game.Chessboard.SetPiece(this.blackQueen, d3);

            Piece whitePawn = new Piece(PieceType.Pawn, PieceColor.White);
            this.Game.Chessboard.SetPiece(whitePawn, f5);
            Move blackQueenMove = new Move(this.Game, d3, f5);
            ICommand blackQueenCapture = new Capture(blackQueenMove, f5);
            Assert.That(blackQueenBehavior.CanDo(blackQueenMove), Is.True);
        }

        [Test]
        public void CantDoCaptureMoveOnTheSameColorPiece()
        {
            ChessboardCoordinate d3 = new ChessboardCoordinate("d3");
            ChessboardCoordinate f5 = new ChessboardCoordinate("f5");

            this.Game.Chessboard.SetPiece(this.blackQueen, d3);

            Piece blackPawn = new Piece(PieceType.Pawn, PieceColor.Black);
            this.Game.Chessboard.SetPiece(blackPawn, f5);
            Move blackQueenMove = new Move(this.Game, d3, f5);
            ICommand blackQueenCapture = new Capture(blackQueenMove, f5);
            Assert.That(blackQueenBehavior.CanDo(blackQueenMove), Is.False);
        }
    }
}
