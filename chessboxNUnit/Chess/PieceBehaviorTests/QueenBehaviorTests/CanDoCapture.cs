﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore;
using ChessboardGamesCore.Commands.Chess;

namespace ChessboardGamesCoreNUnit.Chess.PieceBehaviorTests.QueenBehaviorTests
{
    [TestFixture]
    class CanDoCapture
    {
        [SetUp]
        public void SetUp()
        {
            this.Game = new ChessGame();
            this.whiteQueen = new Piece(PieceType.Queen, PieceColor.White);
            this.whiteQueenBehavior = this.Game.CreatePieceBehavior(whiteQueen);
        }

        ChessGame Game;
        Piece whiteQueen;
        IPieceBehavior whiteQueenBehavior;

        [Test]
        public void DoQueenCapture()
        {
            ChessboardCoordinate e5 = new ChessboardCoordinate("e5");
            this.Game.Chessboard.SetPiece(this.whiteQueen, e5);

            ChessboardCoordinate c7 = new ChessboardCoordinate("c7");
            Move move = new Move(this.Game, e5, c7);
            Capture capture = new Capture(move, c7);

            Assert.That(this.whiteQueenBehavior.CanDo(move), Is.True);
            Assert.That(this.whiteQueenBehavior.CanDo(capture), Is.True);
        }
    }
}
