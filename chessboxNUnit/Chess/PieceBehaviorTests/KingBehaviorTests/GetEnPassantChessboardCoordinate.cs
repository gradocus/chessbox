﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore;
using ChessboardGamesCore.Commands.Chess;
using ChessboardGamesCore.Behaviors;
using ChessboardGamesCore.Behaviors.Chess;

namespace ChessboardGamesCoreNUnit.Chess.PieceBehaviorTests.KingBehaviorTests
{
    [TestFixture]
    class GetEnPassantChessboardCoordinate
    {
        [SetUp]
        public void SetUp()
        {
            this.Game = new ChessGame();
        }

        ChessGame Game;

        [Test]
        public void ItsNullForAnyCorrectMove()
        {
            ChessPieceBehavior whiteKingBehavior = new KingBehavior(PieceColor.White);
            ChessboardCoordinate e2 = new ChessboardCoordinate("e2");
            ChessboardCoordinate e4 = new ChessboardCoordinate("e4");
            Move whitePawnMove = new Move(this.Game, e2, e4);

            this.Game.MovementHistory.ExecuteCommand(whitePawnMove);
            this.Game.MovementHistory.ExecuteCommand(
                new Move(
                    this.Game, new ChessboardCoordinate("h7"),
                    new ChessboardCoordinate("h5")
                ));
            Assert.That(this.Game.EnPassantCoordinate, Is.Not.EqualTo(null));

            ChessboardCoordinate e1 = new ChessboardCoordinate("e1");
            Move whiteKingMove = new Move(this.Game, e1, e2);
            this.Game.MovementHistory.ExecuteCommand(whiteKingMove);
            Assert.That(this.Game.EnPassantCoordinate, Is.EqualTo(null));
        }
    }
}
