﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore;
using ChessboardGamesCore.Commands.Chess;

namespace ChessboardGamesCoreNUnit.Chess.PieceBehaviorTests.KingBehaviorTests
{
    [TestFixture]
    class CanDoMove
    {
        [SetUp]
        public void SetUp()
        {
            this.Game = new ChessGame();
            this.blackKing = new Piece(PieceType.King, PieceColor.Black);
            this.blackKingBehavior = this.Game.CreatePieceBehavior(this.blackKing);

            this.Game.MovementHistory.ExecuteCommand(
                new Move(
                    this.Game, new ChessboardCoordinate("h2"),
                    new ChessboardCoordinate("h3")
                ));
        }

        ChessGame Game;
        Piece blackKing;
        IPieceBehavior blackKingBehavior;

        [Test]
        public void AllCorrectMoves()
        {
            ChessboardCoordinate e4 = new ChessboardCoordinate("e4");
            this.Game.Chessboard.SetPiece(this.blackKing, e4);

            for (int file = e4.File - 1; file <= e4.File + 1; file++)
                for (int rank = e4.Rank - 1; rank <= e4.Rank + 1; rank++)
                {
                    ChessboardCoordinate moveCoordinate = new ChessboardCoordinate(file, rank);
                    if (moveCoordinate == e4)
                        continue;
                    ICommand blackKingMove = new Move(this.Game, e4, moveCoordinate);
                    Assert.That(blackKingBehavior.CanDo(blackKingMove), Is.True);
                }
        }
        
        [Test]
        public void DoCaptureMoveOnOtherColorPiece()
        {
            ChessboardCoordinate e4 = new ChessboardCoordinate("e4");
            ChessboardCoordinate d4 = new ChessboardCoordinate("d4");

            this.Game.Chessboard.SetPiece(this.blackKing, e4);

            Piece whitePawn = new Piece(PieceType.Pawn, PieceColor.White);
            this.Game.Chessboard.SetPiece(whitePawn, d4);
            Move blackKingMove = new Move(this.Game, e4, d4);
            ICommand blackKingCapture = new Capture(blackKingMove, d4);
            Assert.That(blackKingBehavior.CanDo(blackKingMove), Is.True);
        }

        [Test]
        public void CantDoCaptureMoveOnTheSameColorPiece()
        {
            ChessboardCoordinate e4 = new ChessboardCoordinate("e4");
            ChessboardCoordinate d4 = new ChessboardCoordinate("d4");

            this.Game.Chessboard.SetPiece(this.blackKing, e4);

            Piece blackPawn = new Piece(PieceType.Pawn, PieceColor.Black);
            this.Game.Chessboard.SetPiece(blackPawn, d4);
            Move blackKingMove = new Move(this.Game, e4, d4);
            ICommand blackKingCapture = new Capture(blackKingMove, d4);
            Assert.That(blackKingBehavior.CanDo(blackKingMove), Is.False);
        }
    }
}
