﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore;
using ChessboardGamesCore.Commands.Chess;

namespace ChessboardGamesCoreNUnit.Chess.PieceBehaviorTests.KingBehaviorTests
{
    [TestFixture]
    class CanDoCapture
    {
        [SetUp]
        public void SetUp()
        {
            this.Game = new ChessGame();
            this.blackKing = new Piece(PieceType.King, PieceColor.Black);
            this.blackKingBehavior = this.Game.CreatePieceBehavior(blackKing);

            this.Game.MovementHistory.ExecuteCommand(
                new Move(
                    this.Game, new ChessboardCoordinate("h2"),
                    new ChessboardCoordinate("h3")
                ));
        }

        ChessGame Game;
        Piece blackKing;
        IPieceBehavior blackKingBehavior;

        [Test]
        public void DoKingCapture()
        {
            ChessboardCoordinate e3 = new ChessboardCoordinate("e3");
            this.Game.Chessboard.SetPiece(this.blackKing, e3);

            ChessboardCoordinate e2 = new ChessboardCoordinate("e2");
            Move move = new Move(this.Game, e3, e2);
            Capture capture = new Capture(move, e2);

            Assert.That(this.blackKingBehavior.CanDo(move), Is.True);
            Assert.That(this.blackKingBehavior.CanDo(capture), Is.True);
        }
    }
}
