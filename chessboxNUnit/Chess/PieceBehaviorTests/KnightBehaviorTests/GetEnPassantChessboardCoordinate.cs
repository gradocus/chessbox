﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore;
using ChessboardGamesCore.Commands.Chess;
using ChessboardGamesCore.Behaviors;
using ChessboardGamesCore.Behaviors.Chess;

namespace ChessboardGamesCoreNUnit.Chess.PieceBehaviorTests.KnightBehaviorTests
{
    [TestFixture]
    class GetEnPassantChessboardCoordinate
    {
        [SetUp]
        public void SetUp()
        {
            this.Game = new ChessGame();
        }

        ChessGame Game;

        [Test]
        public void ItsNullForAnyCorrectMove()
        {
            ChessPieceBehavior blackKnightBehavior = new KnightBehavior(PieceColor.Black);
            ChessboardCoordinate a2 = new ChessboardCoordinate("a2");
            ChessboardCoordinate a4 = new ChessboardCoordinate("a4");
            Move whitePawnMove = new Move(this.Game, a2, a4);

            this.Game.MovementHistory.ExecuteCommand(whitePawnMove);
            Assert.That(this.Game.EnPassantCoordinate, Is.Not.EqualTo(null));

            ChessboardCoordinate g8 = new ChessboardCoordinate("g8");
            ChessboardCoordinate f6 = new ChessboardCoordinate("f6");
            Move blackKnightMove = new Move(this.Game, g8, f6);
            this.Game.MovementHistory.ExecuteCommand(blackKnightMove);
            Assert.That(this.Game.EnPassantCoordinate, Is.EqualTo(null));
        }
    }
}
