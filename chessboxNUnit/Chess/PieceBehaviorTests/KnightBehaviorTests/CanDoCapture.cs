﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore;
using ChessboardGamesCore.Commands.Chess;

namespace ChessboardGamesCoreNUnit.Chess.PieceBehaviorTests.KnightBehaviorTests
{
    [TestFixture]
    class CanDoCapture
    {
        [SetUp]
        public void SetUp()
        {
            this.Game = new ChessGame();
            this.blackKnight = new Piece(PieceType.Knight, PieceColor.Black);
            this.blackKnightBehavior = this.Game.CreatePieceBehavior(blackKnight);

            this.Game.MovementHistory.ExecuteCommand(
                new Move(
                    this.Game, new ChessboardCoordinate("h2"),
                    new ChessboardCoordinate("h3")
                ));
        }

        ChessGame Game;
        Piece blackKnight;
        IPieceBehavior blackKnightBehavior;

        [Test]
        public void DoKnightCapture()
        {
            ChessboardCoordinate e4 = new ChessboardCoordinate("e4");
            this.Game.Chessboard.SetPiece(this.blackKnight, e4);

            ChessboardCoordinate f2 = new ChessboardCoordinate("f2");
            Move move = new Move(this.Game, e4, f2);
            Capture capture = new Capture(move, f2);

            Assert.That(this.blackKnightBehavior.CanDo(move), Is.True);
            Assert.That(this.blackKnightBehavior.CanDo(capture), Is.True);
        }
    }
}
