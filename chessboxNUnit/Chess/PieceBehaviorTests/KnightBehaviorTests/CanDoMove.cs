﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore;
using ChessboardGamesCore.Commands.Chess;

namespace ChessboardGamesCoreNUnit.Chess.PieceBehaviorTests.KnightBehaviorTests
{
    [TestFixture]
    class CanDoMove
    {
        [SetUp]
        public void SetUp()
        {
            this.Game = new ChessGame();
            this.blackKnight = new Piece(PieceType.Knight, PieceColor.Black);
            this.blackKnightBehavior = this.Game.CreatePieceBehavior(this.blackKnight);

            this.Game.MovementHistory.ExecuteCommand(
                new Move(
                    this.Game, new ChessboardCoordinate("h2"),
                    new ChessboardCoordinate("h3")
                ));
        }

        ChessGame Game;
        Piece blackKnight;
        IPieceBehavior blackKnightBehavior;

        [Test]
        public void AllCorrectMoves()
        {
            ChessboardCoordinate e4 = new ChessboardCoordinate("e4");
            this.Game.Chessboard.SetPiece(this.blackKnight, e4);

            for (int i = 0; i < 7; i++) // Remove pieces from "x2" coordinates.
                this.Game.Chessboard.RemovePiece(new ChessboardCoordinate(i, 1));

            int[] correctDeltaFileOrRank = new int[] { 1, 2 };
            for (int file = e4.File - 2; file <= e4.File + 2; file++)
                for (int rank = e4.Rank - 2; rank <= e4.Rank + 2; rank++)
                {
                    if (!correctDeltaFileOrRank.Contains(Math.Abs(file - e4.File))
                            || !correctDeltaFileOrRank.Contains(Math.Abs(rank - e4.Rank))
                        || Math.Abs(file - e4.File) == Math.Abs(rank - e4.Rank))
                        continue;
                    
                    ChessboardCoordinate moveCoordinate = new ChessboardCoordinate(file, rank);
                    ICommand blackKnightMove = new Move(this.Game, e4, moveCoordinate);
                    Assert.That(blackKnightBehavior.CanDo(blackKnightMove), Is.True);
                }
        }
        
        [Test]
        public void DoCaptureMoveOnOtherColorPiece()
        {
            ChessboardCoordinate e4 = new ChessboardCoordinate("e4");
            ChessboardCoordinate f2 = new ChessboardCoordinate("f2");

            this.Game.Chessboard.SetPiece(this.blackKnight, e4);

            Move blackKnightMove = new Move(this.Game, e4, f2);
            ICommand blackKnightCapture = new Capture(blackKnightMove, f2);
            Assert.That(blackKnightBehavior.CanDo(blackKnightMove), Is.True);
        }

        [Test]
        public void CantDoCaptureMoveOnTheSameColorPiece()
        {
            ChessboardCoordinate g8 = new ChessboardCoordinate("g8");
            ChessboardCoordinate e7 = new ChessboardCoordinate("e7");

            Move blackKnightMove = new Move(this.Game, g8, e7);
            ICommand blackKnightCapture = new Capture(blackKnightMove, e7);
            Assert.That(blackKnightBehavior.CanDo(blackKnightMove), Is.False);
        }
    }
}
