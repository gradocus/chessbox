﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore;
using ChessboardGamesCore.Commands.Chess;
using ChessboardGamesCore.Behaviors;
using ChessboardGamesCore.Behaviors.Chess;

namespace ChessboardGamesCoreNUnit.Chess.PieceBehaviorTests.PawnBehaviorTests
{
    [TestFixture]
    class GetEnPassantChessboardCoordinate
    {
        [SetUp]
        public void SetUp()
        {
            this.Game = new ChessGame();
        }

        ChessGame Game;

        [Test]
        public void ForCorrectTwoSquareMove()
        {
            ChessPieceBehavior whitePawnBehavior = new PawnBehavior(PieceColor.White);
            ChessboardCoordinate b2 = new ChessboardCoordinate("b2");
            ChessboardCoordinate b4 = new ChessboardCoordinate("b4");
            Move whitePawnMove = new Move(this.Game, b2, b4);

            Assert.That(whitePawnBehavior.GetEnPassantCoordinate(whitePawnMove),
                        Is.EqualTo(new ChessboardCoordinate("b3")));

            ChessPieceBehavior blackPawnBehavior = new PawnBehavior(PieceColor.Black);
            ChessboardCoordinate b7 = new ChessboardCoordinate("b7");
            ChessboardCoordinate b5 = new ChessboardCoordinate("b5");
            Move blackPawnMove = new Move(this.Game, b7, b5);

            Assert.That(blackPawnBehavior.GetEnPassantCoordinate(blackPawnMove),
                        Is.EqualTo(new ChessboardCoordinate("b6")));
        }

        [Test]
        public void ForCorrectNonTwoSquareMove()
        {
            ChessPieceBehavior whitePawnBehavior = new PawnBehavior(PieceColor.White);
            ChessboardCoordinate b2 = new ChessboardCoordinate("b2");
            ChessboardCoordinate b3 = new ChessboardCoordinate("b3");
            Move whitePawnMove = new Move(this.Game, b2, b3);

            Assert.That(whitePawnBehavior.GetEnPassantCoordinate(whitePawnMove), Is.EqualTo(null));

            ChessPieceBehavior blackPawnBehavior = new PawnBehavior(PieceColor.Black);
            ChessboardCoordinate b7 = new ChessboardCoordinate("b7");
            ChessboardCoordinate b6 = new ChessboardCoordinate("b6");
            Move blackPawnMove = new Move(this.Game, b7, b6);

            Assert.That(blackPawnBehavior.GetEnPassantCoordinate(blackPawnMove), Is.EqualTo(null));
        }
    }
}
