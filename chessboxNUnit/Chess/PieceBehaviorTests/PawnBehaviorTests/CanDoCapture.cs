﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore;
using ChessboardGamesCore.Commands.Chess;

namespace ChessboardGamesCoreNUnit.Chess.PieceBehaviorTests.PawnBehaviorTests
{
    [TestFixture]
    class CanDoCapture
    {
        [SetUp]
        public void SetUp()
        {
            this.Game = new ChessGame();

            this.blackPawnBehavior = this.Game.CreatePieceBehavior(
                new Piece(PieceType.Pawn, PieceColor.Black));

            this.whitePawnBehavior = this.Game.CreatePieceBehavior(
                new Piece(PieceType.Pawn, PieceColor.White));
        }

        ChessGame Game;
        IPieceBehavior blackPawnBehavior;
        IPieceBehavior whitePawnBehavior;

        [Test]
        public void DoDefaultPawnCapture()
        {
            Piece blackPawn = new Piece(PieceType.Pawn, PieceColor.Black);
            ChessboardCoordinate c3 = new ChessboardCoordinate("c3");
            this.Game.Chessboard.SetPiece(blackPawn, c3);
            ChessboardCoordinate b2 = new ChessboardCoordinate("b2");
            
            Move move = new Move(this.Game, b2, c3);
            Capture capture = new Capture(move, c3);

            Assert.That(whitePawnBehavior.CanDo(move), Is.True);
            Assert.That(whitePawnBehavior.CanDo(capture), Is.True);
        }

        [Test]
        public void DoEnPassantPawnCapture()
        {
            this.Game.MovementHistory.ExecuteCommand(
                new Move(
                    this.Game, new ChessboardCoordinate("h2"),
                    new ChessboardCoordinate("h3")
                ));

            ChessboardCoordinate c5 = new ChessboardCoordinate("c5");
            Piece whitePawn = new Piece(PieceType.Pawn, PieceColor.White);
            this.Game.Chessboard.SetPiece(whitePawn, c5);

            ChessboardCoordinate d7 = new ChessboardCoordinate("d7");
            ChessboardCoordinate d5 = new ChessboardCoordinate("d5");
            ICommand blackPawnMove = new Move(this.Game, d7, d5);
            this.Game.MovementHistory.ExecuteCommand(blackPawnMove);

            ChessboardCoordinate enPassantCoopdinate = new ChessboardCoordinate("d6");
            Move enPassantMove = new Move(this.Game, c5, enPassantCoopdinate);
            ICommand enPassantCapture = new Capture(enPassantMove, d5);
            Assert.That(whitePawnBehavior.CanDo(enPassantMove), Is.True);
            Assert.That(whitePawnBehavior.CanDo(enPassantCapture), Is.True);
        }
    }
}
