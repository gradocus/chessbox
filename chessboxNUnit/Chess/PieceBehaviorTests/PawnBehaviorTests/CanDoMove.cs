﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore;
using ChessboardGamesCore.Commands.Chess;

namespace ChessboardGamesCoreNUnit.Chess.PieceBehaviorTests.PawnBehaviorTests
{
    [TestFixture]
    class CanDoMove
    {
        [SetUp]
        public void SetUp()
        {
            this.Game = new ChessGame();

            this.blackPawnBehavior = this.Game.CreatePieceBehavior(
                new Piece(PieceType.Pawn, PieceColor.Black));

            this.whitePawnBehavior = this.Game.CreatePieceBehavior(
                new Piece(PieceType.Pawn, PieceColor.White));
        }

        ChessGame Game;
        IPieceBehavior blackPawnBehavior;
        IPieceBehavior whitePawnBehavior;

        [Test]
        public void MoveToTheNextSquare()
        {
            ICommand whitePawnMove = new Move(this.Game,
                new ChessboardCoordinate("b2"),
                new ChessboardCoordinate("b3"));
            Assert.That(whitePawnBehavior.CanDo(whitePawnMove), Is.True);

            this.Game.MovementHistory.ExecuteCommand(whitePawnMove);

            ICommand blackPawnMove = new Move(this.Game,
                new ChessboardCoordinate("b7"),
                new ChessboardCoordinate("b6"));
            Assert.That(blackPawnBehavior.CanDo(blackPawnMove), Is.True);
        }

        [Test]
        public void CantMoveBack()
        {
            ChessboardCoordinate b2 = new ChessboardCoordinate("b2");
            ChessboardCoordinate b3 = new ChessboardCoordinate("b3");
            ICommand whitePawnMove = new Move(this.Game, b2, b3);
            this.Game.MovementHistory.ExecuteCommand(whitePawnMove);

            ChessboardCoordinate b7 = new ChessboardCoordinate("b7");
            ChessboardCoordinate b6 = new ChessboardCoordinate("b6");
            ICommand blackPawnMove = new Move(this.Game, b7, b6);
            this.Game.MovementHistory.ExecuteCommand(blackPawnMove);

            ICommand whitePawnBackMove = new Move(this.Game, b3, b2);
            Assert.That(whitePawnBehavior.CanDo(whitePawnBackMove), Is.False);
        }

        [Test]
        public void DoTwoSquareMoveFromInitialPosition()
        {
            ChessboardCoordinate b2 = new ChessboardCoordinate("b2");
            ChessboardCoordinate b4 = new ChessboardCoordinate("b4");
            ICommand whitePawnTwoSquareMove = new Move(this.Game, b2, b4);
            Assert.That(whitePawnBehavior.CanDo(whitePawnTwoSquareMove), Is.True);

            this.Game.MovementHistory.ExecuteCommand(whitePawnTwoSquareMove);

            ChessboardCoordinate b7 = new ChessboardCoordinate("b7");
            ChessboardCoordinate b5 = new ChessboardCoordinate("b5");
            ICommand blackPawnTwoSquareMove = new Move(this.Game, b7, b5);
            Assert.That(blackPawnBehavior.CanDo(blackPawnTwoSquareMove), Is.True);
        }

        [Test]
        public void CantDoTwoSquareMoveFromNotInitialPosition()
        {
            ChessboardCoordinate b2 = new ChessboardCoordinate("b2");
            ChessboardCoordinate b3 = new ChessboardCoordinate("b3");
            ChessboardCoordinate b5 = new ChessboardCoordinate("b5");
            ICommand whitePawnMove = new Move(this.Game, b2, b3);
            this.Game.MovementHistory.ExecuteCommand(whitePawnMove);

            ChessboardCoordinate a7 = new ChessboardCoordinate("a7");
            ChessboardCoordinate a6 = new ChessboardCoordinate("a6");
            ChessboardCoordinate a4 = new ChessboardCoordinate("a4");

            ICommand blackPawnTwoSquareMove = new Move(this.Game, a6, a4);
            Assert.That(blackPawnBehavior.CanDo(blackPawnTwoSquareMove), Is.False);

            ICommand blackPawnMove = new Move(this.Game, a7, a6);
            this.Game.MovementHistory.ExecuteCommand(blackPawnMove);

            ICommand whitePawnTwoSquareMove = new Move(this.Game, b3, b5);
            Assert.That(whitePawnBehavior.CanDo(whitePawnTwoSquareMove), Is.False);
        }

        [Test]
        public void CantTwoSquareMoveViaOtherPieces()
        {
            this.Game.Chessboard.SetPiece(
                    new Piece(PieceType.Pawn, PieceColor.White),
                    new ChessboardCoordinate("b3")
                );
            ChessboardCoordinate b2 = new ChessboardCoordinate("b2");
            ChessboardCoordinate b4 = new ChessboardCoordinate("b4");
            ICommand whitePawnTwoSquareMove = new Move(this.Game, b2, b4);
            Assert.That(whitePawnBehavior.CanDo(whitePawnTwoSquareMove), Is.False);
        }

        [Test]
        public void UpdateEnPassantOnTwoSquareMove()
        {
            ChessboardCoordinate c4 = new ChessboardCoordinate("c4");
            Piece blackPawn = new Piece(PieceType.Pawn, PieceColor.Black);
            this.Game.Chessboard.SetPiece(blackPawn, c4);

            ChessboardCoordinate d2 = new ChessboardCoordinate("d2");
            ChessboardCoordinate d4 = new ChessboardCoordinate("d4");
            ICommand whitePawnMove = new Move(this.Game, d2, d4);
            this.Game.MovementHistory.ExecuteCommand(whitePawnMove);

            ChessboardCoordinate enPassantCoopdinate = new ChessboardCoordinate("d3");
            ICommand pawnEnPassantMove = new Move(this.Game, c4, enPassantCoopdinate);
            Assert.That(blackPawnBehavior.CanDo(pawnEnPassantMove), Is.True);
        }

        [Test]
        public void CantMoveLeftOrRight()
        {
            ChessboardCoordinate b4 = new ChessboardCoordinate("b4");
            ChessboardCoordinate c4 = new ChessboardCoordinate("c4");
            ChessboardCoordinate d4 = new ChessboardCoordinate("d4");

            Piece whitePawn = new Piece(PieceType.Pawn, PieceColor.White);
            this.Game.Chessboard.SetPiece(whitePawn, c4);
            ICommand whitePawnMoveLeft = new Move(this.Game, c4, b4);
            Assert.That(whitePawnBehavior.CanDo(whitePawnMoveLeft), Is.False);
            ICommand whitePawnMoveRight = new Move(this.Game, c4, d4);
            Assert.That(whitePawnBehavior.CanDo(whitePawnMoveRight), Is.False);
        }

        [Test]
        public void DoCaptureMoveOnOtherColorPiece()
        {
            ChessboardCoordinate b2 = new ChessboardCoordinate("b2");
            ChessboardCoordinate c3 = new ChessboardCoordinate("c3");

            Piece blackPawn = new Piece(PieceType.Pawn, PieceColor.Black);
            this.Game.Chessboard.SetPiece(blackPawn, c3);
            Move whitePawnCaptureMove = new Move(this.Game, b2, c3);
            ICommand whitePawnCapture = new Capture(whitePawnCaptureMove, c3);
            Assert.That(whitePawnBehavior.CanDo(whitePawnCaptureMove), Is.True);
        }

        [Test]
        public void CantDoCaptureMoveOnEmptySquare()
        {
            ChessboardCoordinate b2 = new ChessboardCoordinate("b2");
            ChessboardCoordinate c3 = new ChessboardCoordinate("c3");

            ICommand whitePawnCaptureMove = new Move(this.Game, b2, c3);
            Assert.That(whitePawnBehavior.CanDo(whitePawnCaptureMove), Is.False);
        }
    }
}
