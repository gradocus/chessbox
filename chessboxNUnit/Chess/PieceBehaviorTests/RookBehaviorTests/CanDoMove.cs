﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore;
using ChessboardGamesCore.Commands.Chess;

namespace ChessboardGamesCoreNUnit.Chess.PieceBehaviorTests.RookBehaviorTests
{
    [TestFixture]
    class CanDoMove
    {
        [SetUp]
        public void SetUp()
        {
            this.Game = new ChessGame();
            this.blackRook = new Piece(PieceType.Rook, PieceColor.Black);
            this.blackRookBehavior = this.Game.CreatePieceBehavior(this.blackRook);

            this.Game.MovementHistory.ExecuteCommand(
                new Move(
                    this.Game, new ChessboardCoordinate("h2"),
                    new ChessboardCoordinate("h3")
                ));
        }

        ChessGame Game;
        Piece blackRook;
        IPieceBehavior blackRookBehavior;

        [Test]
        public void MoveOnHorizontalAndVertical()
        {
            ChessboardCoordinate e4 = new ChessboardCoordinate("e4");
            this.Game.Chessboard.SetPiece(this.blackRook, e4);

            ICommand blackRookFirstMove = new Move(this.Game, e4,
                new ChessboardCoordinate("a4"));
            Assert.That(blackRookBehavior.CanDo(blackRookFirstMove), Is.True);

            ICommand blackRookSecondMove = new Move(this.Game, e4,
                new ChessboardCoordinate("h4"));
            Assert.That(blackRookBehavior.CanDo(blackRookSecondMove), Is.True);

            ICommand blackRookThirdMove = new Move(this.Game, e4,
                new ChessboardCoordinate("e3"));
            Assert.That(blackRookBehavior.CanDo(blackRookThirdMove), Is.True);

            ICommand blackRookFourthMove = new Move(this.Game, e4,
                new ChessboardCoordinate("e6"));
            Assert.That(blackRookBehavior.CanDo(blackRookFourthMove), Is.True);
        }

        [Test]
        public void CantMoveViaOtherPieces()
        {
            ChessboardCoordinate e4 = new ChessboardCoordinate("e4");
            this.Game.Chessboard.SetPiece(this.blackRook, e4);

            ICommand blackRookFirstMove = new Move(this.Game, e4,
                new ChessboardCoordinate("a4"));
            this.Game.Chessboard.SetPiece(
                new Piece(PieceType.Pawn, PieceColor.Black),
                new ChessboardCoordinate("c4"));
            Assert.That(blackRookBehavior.CanDo(blackRookFirstMove), Is.False);

            ICommand blackRookSecondMove = new Move(this.Game, e4,
                new ChessboardCoordinate("e6"));
            this.Game.Chessboard.SetPiece(
                new Piece(PieceType.Pawn, PieceColor.White),
                new ChessboardCoordinate("e5"));
            Assert.That(blackRookBehavior.CanDo(blackRookSecondMove), Is.False);
        }

        [Test]
        public void DoCaptureMoveOnOtherColorPiece()
        {
            ChessboardCoordinate e4 = new ChessboardCoordinate("e4");
            ChessboardCoordinate a4 = new ChessboardCoordinate("a4");

            this.Game.Chessboard.SetPiece(this.blackRook, e4);

            Piece whitePawn = new Piece(PieceType.Pawn, PieceColor.White);
            this.Game.Chessboard.SetPiece(whitePawn, a4);
            Move blackRookMove = new Move(this.Game, e4, a4);
            ICommand blackRookCapture = new Capture(blackRookMove, a4);
            Assert.That(blackRookBehavior.CanDo(blackRookMove), Is.True);
        }

        [Test]
        public void CantDoCaptureMoveOnTheSameColorPiece()
        {
            ChessboardCoordinate e4 = new ChessboardCoordinate("e4");
            ChessboardCoordinate a4 = new ChessboardCoordinate("a4");

            this.Game.Chessboard.SetPiece(this.blackRook, e4);

            Piece blackPawn = new Piece(PieceType.Pawn, PieceColor.Black);
            this.Game.Chessboard.SetPiece(blackPawn, a4);
            Move blackRookMove = new Move(this.Game, e4, a4);
            ICommand blackRookCapture = new Capture(blackRookMove, a4);
            Assert.That(blackRookBehavior.CanDo(blackRookMove), Is.False);
        }
    }
}
