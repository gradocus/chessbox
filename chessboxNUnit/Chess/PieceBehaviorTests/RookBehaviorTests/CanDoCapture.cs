﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore;
using ChessboardGamesCore.Commands.Chess;

namespace ChessboardGamesCoreNUnit.Chess.PieceBehaviorTests.RookBehaviorTests
{
    [TestFixture]
    class CanDoCapture
    {
        [SetUp]
        public void SetUp()
        {
            this.Game = new ChessGame();
            this.whiteRook = new Piece(PieceType.Rook, PieceColor.White);
            this.whiteRookBehavior = this.Game.CreatePieceBehavior(whiteRook);
        }

        ChessGame Game;
        Piece whiteRook;
        IPieceBehavior whiteRookBehavior;

        [Test]
        public void DoRookCapture()
        {
            ChessboardCoordinate e4 = new ChessboardCoordinate("e4");
            this.Game.Chessboard.SetPiece(this.whiteRook, e4);

            ChessboardCoordinate e7 = new ChessboardCoordinate("e7");
            Move move = new Move(this.Game, e4, e7);
            Capture capture = new Capture(move, e7);

            Assert.That(this.whiteRookBehavior.CanDo(move), Is.True);
            Assert.That(this.whiteRookBehavior.CanDo(capture), Is.True);
        }
    }
}
