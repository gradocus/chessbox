﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore;
using ChessboardGamesCore.Commands.Chess;
using ChessboardGamesCore.Behaviors;
using ChessboardGamesCore.Behaviors.Chess;

namespace ChessboardGamesCoreNUnit.Chess.PieceBehaviorTests.RookBehaviorTests
{
    [TestFixture]
    class GetEnPassantChessboardCoordinate
    {
        [SetUp]
        public void SetUp()
        {
            this.Game = new ChessGame();
        }

        ChessGame Game;

        [Test]
        public void ItsNullForAnyCorrectMove()
        {
            ChessPieceBehavior whiteRookBehavior = new RookBehavior(PieceColor.White);
            ChessboardCoordinate a2 = new ChessboardCoordinate("a2");
            ChessboardCoordinate a4 = new ChessboardCoordinate("a4");
            Move whitePawnMove = new Move(this.Game, a2, a4);

            this.Game.MovementHistory.ExecuteCommand(whitePawnMove);
            this.Game.MovementHistory.ExecuteCommand(
                new Move(
                    this.Game, new ChessboardCoordinate("a7"),
                    new ChessboardCoordinate("a5")
                ));
            Assert.That(this.Game.EnPassantCoordinate, Is.Not.EqualTo(null));

            ChessboardCoordinate a1 = new ChessboardCoordinate("a1");
            ChessboardCoordinate a3 = new ChessboardCoordinate("a3");
            Move whiteRookMove = new Move(this.Game, a1, a3);
            this.Game.MovementHistory.ExecuteCommand(whiteRookMove);
            Assert.That(this.Game.EnPassantCoordinate, Is.EqualTo(null));
        }
    }
}
