﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore;
using ChessboardGamesCore.Commands;
using ChessboardGamesCore.Commands.Chess;

namespace ChessboardGamesCoreNUnit.Chess.Commands
{
    [TestFixture]
    class CompositeCommandTests
    {
        [Test]
        public void AddCommand()
        {
            CompositeCommand compositeCommand = new CompositeCommand();
            FakeCommand fakeCommand = new FakeCommand();

            int count = compositeCommand.Count;
            compositeCommand.AddCommand(fakeCommand);

            Assert.That(compositeCommand.Count, Is.EqualTo(count + 1));
        }

        [Test]
        public void ApplyCompositeCommand()
        {
            CompositeCommand compositeCommand = new CompositeCommand();
            FakeCommand firstFakeCommand = new FakeCommand();
            FakeCommand secondFakeCommand = new FakeCommand();

            compositeCommand.AddCommand(firstFakeCommand);
            compositeCommand.AddCommand(secondFakeCommand);

            compositeCommand.Execute();
            Assert.That(firstFakeCommand.IsApplied && secondFakeCommand.IsApplied, Is.True);
        }

        [Test]
        public void CancelCompositeCommand()
        {
            FakeCommand firstFakeCommand = new FakeCommand();
            FakeCommand secondFakeCommand = new FakeCommand();

            CompositeCommand compositeCommand =
                new CompositeCommand(firstFakeCommand, secondFakeCommand);

            compositeCommand.Execute();
            compositeCommand.UnExecute();
            Assert.That(firstFakeCommand.IsApplied || secondFakeCommand.IsApplied, Is.False);
        }

        [Test]
        public void RemoveCommand()
        {
            CompositeCommand compositeCommand = new CompositeCommand();
            FakeCommand fakeCommand = new FakeCommand();

            compositeCommand.AddCommand(fakeCommand);
            int count = compositeCommand.Count;
            compositeCommand.RemoveCommand(fakeCommand);

            Assert.That(compositeCommand.Count, Is.EqualTo(count - 1));
        }

        [Test]
        public void TryRemoveNotAddedCommand()
        {
            CompositeCommand compositeCommand = new CompositeCommand();
            FakeCommand fakeCommand = new FakeCommand();
            FakeCommand anotherFakeCommand = new FakeCommand();

            compositeCommand.AddCommand(fakeCommand);
            int count = compositeCommand.Count;
            compositeCommand.RemoveCommand(anotherFakeCommand);

            Assert.That(compositeCommand.Count, Is.EqualTo(count));
        }

        [Test]
        public void CompositeCommandMoveAndPromotePawn()
        {
            Chessboard board = new Chessboard();
            ChessGame game = new ChessGame(board, PieceColor.White, 0, 1);

            ChessboardCoordinate a7 = new ChessboardCoordinate("a7");
            game.Chessboard.SetPiece(new Piece(PieceType.Pawn, PieceColor.White), a7);
            ChessboardCoordinate a8 = new ChessboardCoordinate("a8");
            Move move = new Move(game, a7, a8);
            CompositeCommand command = new CompositeCommand();
            command.AddCommand(move);
            ICommand whitePawnPromotion = new Promotion(game, a8, PieceType.Queen);
            command.AddCommand(whitePawnPromotion);
            game.MovementHistory.ExecuteCommand(command);
            Assert.That(game.Chessboard[a8].Type, Is.EqualTo(PieceType.Queen));
        }
    }
}
