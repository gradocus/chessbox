﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ChessboardGamesCore;

namespace ChessboardGamesCoreNUnit.Chess.Commands
{
    class FakeCommand : ICommand
    {
        public FakeCommand()
        {
            this.IsApplied = false;
            this.IsReversible = false;
        }

        public bool IsApplied
        {
            get;
            private set;
        }

        public void Execute()
        {
            this.IsApplied = true;
            this.IsReversible = true;
        }

        public ChessboardCoordinate DestinationCoordinate
        {
            get { return new ChessboardCoordinate(""); }
        }

        public bool IsReversible
        {
            get;
            private set;
        }

        public void UnExecute()
        {
            this.IsApplied = false;
            this.IsReversible = false;
        }
    }
}
