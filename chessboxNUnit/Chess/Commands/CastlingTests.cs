﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore;
using ChessboardGamesCore.Exceptions;
using ChessboardGamesCore.Commands.Chess;

namespace ChessboardGamesCoreNUnit.Chess.Commands
{
    [TestFixture]
    class CastlingTests
    {
        [SetUp]
        public void SetUp()
        {
            this.Game = new ChessGame();
        }

        private ChessGame Game;

        [Test]
        [ExpectedException(typeof(IncorrectCommandException))]
        public void IncorrectCommandExceptionIfKingHasMoved()
        {
            this.Game.Chessboard.RemovePiece(new ChessboardCoordinate("f1"));
            this.Game.Chessboard.RemovePiece(new ChessboardCoordinate("g1"));

            // Add moves to white king movement history.
            ChessboardCoordinate kingCoordinate = new ChessboardCoordinate("e1");
            this.Game.Chessboard[kingCoordinate].MovementHistory.Add(new ChessboardCoordinate("f1"));
            this.Game.Chessboard[kingCoordinate].MovementHistory.Add(new ChessboardCoordinate("e1"));

            Castling castling = new Castling(this.Game, PieceColor.White, CastlingType.Kingside);
            this.Game.MovementHistory.ExecuteCommand(castling);
        }

        [Test]
        [ExpectedException(typeof(IncorrectCommandException))]
        public void IncorrectCommandExceptionIfRookHasMoved()
        {
            this.Game.Chessboard.RemovePiece(new ChessboardCoordinate("f1"));
            this.Game.Chessboard.RemovePiece(new ChessboardCoordinate("g1"));

            // Add moves to white king movement history.
            ChessboardCoordinate rookCoordinate = new ChessboardCoordinate("h1");
            this.Game.Chessboard[rookCoordinate].MovementHistory.Add(new ChessboardCoordinate("g1"));
            this.Game.Chessboard[rookCoordinate].MovementHistory.Add(new ChessboardCoordinate("h1"));

            Castling castling = new Castling(this.Game, PieceColor.White, CastlingType.Kingside);
            this.Game.MovementHistory.ExecuteCommand(castling);
        }

        [Test]
        [ExpectedException(typeof(IncorrectCommandException))]
        public void IncorrectCommandExceptionIfHasNoEmptySquaresBetweenTheKingAndTheRook()
        {
            Castling castling = new Castling(this.Game, PieceColor.White, CastlingType.Kingside);
            this.Game.MovementHistory.ExecuteCommand(castling);
        }

        [Test]
        public void MovePiecesOnExecuteCommandForWhiteKingsideCastling()
        {
            this.Game.Chessboard.RemovePiece(new ChessboardCoordinate("f1"));
            this.Game.Chessboard.RemovePiece(new ChessboardCoordinate("g1"));

            Castling castling = new Castling(this.Game, PieceColor.White, CastlingType.Kingside);
            this.Game.MovementHistory.ExecuteCommand(castling);

            Assert.That(this.Game.Chessboard[new ChessboardCoordinate("e1")], Is.EqualTo(null));
            Assert.That(this.Game.Chessboard[new ChessboardCoordinate("g1")].Type, Is.EqualTo(PieceType.King));
            Assert.That(this.Game.Chessboard[new ChessboardCoordinate("h1")], Is.EqualTo(null));
            Assert.That(this.Game.Chessboard[new ChessboardCoordinate("f1")].Type, Is.EqualTo(PieceType.Rook));
        }

        [Test]
        public void MoveBlackPiecesOnExecuteCommandForBlackKingsideCastling()
        {
            this.Game.Chessboard.RemovePiece(new ChessboardCoordinate("f8"));
            this.Game.Chessboard.RemovePiece(new ChessboardCoordinate("g8"));

            Castling castling = new Castling(this.Game, PieceColor.Black, CastlingType.Kingside);
            this.Game.MovementHistory.ExecuteCommand(castling);

            Assert.That(this.Game.Chessboard[new ChessboardCoordinate("e8")], Is.EqualTo(null));
            Assert.That(this.Game.Chessboard[new ChessboardCoordinate("g8")].Type, Is.EqualTo(PieceType.King));
            Assert.That(this.Game.Chessboard[new ChessboardCoordinate("h8")], Is.EqualTo(null));
            Assert.That(this.Game.Chessboard[new ChessboardCoordinate("f8")].Type, Is.EqualTo(PieceType.Rook));
        }

        [Test]
        public void MovePiecesOnExecuteCommandForWhiteQueensideCastling()
        {
            this.Game.Chessboard.RemovePiece(new ChessboardCoordinate("b1"));
            this.Game.Chessboard.RemovePiece(new ChessboardCoordinate("c1"));
            this.Game.Chessboard.RemovePiece(new ChessboardCoordinate("d1"));

            Castling castling = new Castling(this.Game, PieceColor.White, CastlingType.Queenside);
            this.Game.MovementHistory.ExecuteCommand(castling);

            Assert.That(this.Game.Chessboard[new ChessboardCoordinate("e1")], Is.EqualTo(null));
            Assert.That(this.Game.Chessboard[new ChessboardCoordinate("c1")].Type, Is.EqualTo(PieceType.King));
            Assert.That(this.Game.Chessboard[new ChessboardCoordinate("a1")], Is.EqualTo(null));
            Assert.That(this.Game.Chessboard[new ChessboardCoordinate("d1")].Type, Is.EqualTo(PieceType.Rook));
        }

        [Test]
        public void MoveBlackPiecesOnExecuteCommandForBlackQueensideCastling()
        {
            this.Game.Chessboard.RemovePiece(new ChessboardCoordinate("b8"));
            this.Game.Chessboard.RemovePiece(new ChessboardCoordinate("c8"));
            this.Game.Chessboard.RemovePiece(new ChessboardCoordinate("d8"));

            Castling castling = new Castling(this.Game, PieceColor.Black, CastlingType.Queenside);
            this.Game.MovementHistory.ExecuteCommand(castling);

            Assert.That(this.Game.Chessboard[new ChessboardCoordinate("e8")], Is.EqualTo(null));
            Assert.That(this.Game.Chessboard[new ChessboardCoordinate("c8")].Type, Is.EqualTo(PieceType.King));
            Assert.That(this.Game.Chessboard[new ChessboardCoordinate("a8")], Is.EqualTo(null));
            Assert.That(this.Game.Chessboard[new ChessboardCoordinate("d8")].Type, Is.EqualTo(PieceType.Rook));
        }

        [Test]
        public void CantCastlingViaCheckSquares()
        {
            Chessboard chessboard = new Chessboard();

            ChessboardCoordinate a1 = new ChessboardCoordinate("a1");
            ChessboardCoordinate e1 = new ChessboardCoordinate("e1");
            ChessboardCoordinate c3 = new ChessboardCoordinate("c3");

            Piece whiteKing = new Piece(PieceType.King, PieceColor.White);
            chessboard.SetPiece(new Piece(PieceType.Rook, PieceColor.White), a1);
            chessboard.SetPiece(whiteKing, e1);
            chessboard.SetPiece(new Piece(PieceType.Rook, PieceColor.Black), c3);

            ChessGame game = new ChessGame(chessboard, PieceColor.White, 0, 1);
            IPieceBehavior whiteKingBehavior = game.CreatePieceBehavior(whiteKing);

            ICommand castling = new Castling(game, PieceColor.White, CastlingType.Queenside);

            Assert.That(whiteKingBehavior.CanDo(castling), Is.False);
        }
    }
}
