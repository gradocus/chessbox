﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore;
using ChessboardGamesCore.Commands.Chess;
using ChessboardGamesCore.Exceptions;

namespace ChessboardGamesCoreNUnit.Chess.Commands
{
    [TestFixture]
    class ChessCaptureTests
    {
        [SetUp]
        public void SetUp()
        {
            this.Game = new ChessGame();
        }

        private ChessGame Game;

        [Test]
        public void ExecuteCaptureCommand()
        {
            ChessboardCoordinate c3 = new ChessboardCoordinate("c3");
            ChessboardCoordinate b2 = new ChessboardCoordinate("b2");

            Piece blackPawn = new Piece(PieceType.Pawn, PieceColor.Black);
            this.Game.Chessboard.SetPiece(blackPawn, c3);

            Move move = new Move(this.Game, b2, c3);
            Capture capture = new Capture(move, c3);

            this.Game.MovementHistory.ExecuteCommand(capture);
            Assert.That(this.Game.Chessboard[c3], Is.Not.EqualTo(blackPawn));
        }

        [Test]
        public void ResetHalfmoveClockOnExecute()
        {
            ChessboardCoordinate c3 = new ChessboardCoordinate("c3");
            ChessboardCoordinate b2 = new ChessboardCoordinate("b2");

            Piece blackPawn = new Piece(PieceType.Pawn, PieceColor.Black);
            this.Game.Chessboard.SetPiece(blackPawn, c3);

            this.Game.MovementHistory.ExecuteCommand(new Move(this.Game,
                    new ChessboardCoordinate("a2"),
                    new ChessboardCoordinate("a3")
                ));
            this.Game.MovementHistory.ExecuteCommand(new Move(this.Game,
                    new ChessboardCoordinate("b7"),
                    new ChessboardCoordinate("b6")
                ));
            Assert.That(this.Game.HalfmoveClock, Is.Not.EqualTo(0));

            Move move = new Move(this.Game, b2, c3);
            Capture capture = new Capture(move, c3);
            this.Game.MovementHistory.ExecuteCommand(capture);
            Assert.That(this.Game.HalfmoveClock, Is.EqualTo(0));
        }

        [Test]
        public void UnExecuteCaptureCommand()
        {
            ChessboardCoordinate c3 = new ChessboardCoordinate("c3");
            ChessboardCoordinate b2 = new ChessboardCoordinate("b2");

            Piece blackPawn = new Piece(PieceType.Pawn, PieceColor.Black);
            this.Game.Chessboard.SetPiece(blackPawn, c3);

            Move move = new Move(this.Game, b2, c3);
            Capture capture = new Capture(move, c3);

            this.Game.MovementHistory.ExecuteCommand(capture);
            this.Game.MovementHistory.Undo();
            Assert.That(this.Game.Chessboard[c3], Is.EqualTo(blackPawn));
        }

        [Test]
        public void RestoreHalfmoveClockOnUnExecute()
        {
            ChessboardCoordinate c3 = new ChessboardCoordinate("c3");
            ChessboardCoordinate b2 = new ChessboardCoordinate("b2");

            Piece blackPawn = new Piece(PieceType.Pawn, PieceColor.Black);
            this.Game.Chessboard.SetPiece(blackPawn, c3);

            this.Game.MovementHistory.ExecuteCommand(new Move(this.Game,
                    new ChessboardCoordinate("a2"),
                    new ChessboardCoordinate("a3")
                ));
            this.Game.MovementHistory.ExecuteCommand(new Move(this.Game,
                    new ChessboardCoordinate("b7"),
                    new ChessboardCoordinate("b6")
                ));
            int oldHalfmoveClock = this.Game.HalfmoveClock;

            Move move = new Move(this.Game, b2, c3);
            Capture capture = new Capture(move, c3);
            this.Game.MovementHistory.ExecuteCommand(capture);
            Assert.That(this.Game.HalfmoveClock, Is.EqualTo(0));
            this.Game.MovementHistory.Undo();
            Assert.That(this.Game.HalfmoveClock, Is.EqualTo(oldHalfmoveClock));
        }

        [Test]
        [ExpectedException(typeof(IncorrectCommandException))]
        public void GetIncorrectCommandExceptionOnIncorrectCaptureCommand()
        {
            ChessboardCoordinate b2 = new ChessboardCoordinate("b2");
            ChessboardCoordinate b4 = new ChessboardCoordinate("b4");
            Move b2b4move = new Move(this.Game, b2, b4);
            ICommand incorrectCapture = new Capture(b2b4move, new ChessboardCoordinate("b3"));

            this.Game.MovementHistory.ExecuteCommand(incorrectCapture);
        }
    }
}
