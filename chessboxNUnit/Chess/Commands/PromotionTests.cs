﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore;
using ChessboardGamesCore.Exceptions;
using ChessboardGamesCore.Commands.Chess;

namespace ChessboardGamesCoreNUnit.Chess.Commands
{
    [TestFixture]
    class PromotionTests
    {
        [SetUp]
        public void SetUp()
        {
            Chessboard emptyChessboard = new Chessboard();
            this.Game = new ChessGame(emptyChessboard, PieceColor.White, 0, 1);
        }

        ChessGame Game;

        [Test]
        public void TransformPawnThatReachesItsEighthRank()
        {
            ChessboardCoordinate a8 = new ChessboardCoordinate("a8");
            this.Game.Chessboard.SetPiece(new Piece(PieceType.Pawn, PieceColor.White), a8);

            ICommand whitePawnPromotion = new Promotion(this.Game, a8, PieceType.Queen);
            Assert.That(this.Game.Chessboard[a8].Type, Is.EqualTo(PieceType.Pawn));
            this.Game.MovementHistory.ExecuteCommand(whitePawnPromotion);
            Assert.That(this.Game.Chessboard[a8].Type, Is.EqualTo(PieceType.Queen));

            ChessboardCoordinate a1 = new ChessboardCoordinate("a1");
            this.Game.Chessboard.SetPiece(new Piece(PieceType.Pawn, PieceColor.Black), a1);

            ICommand blackPawnPromotion = new Promotion(this.Game, a1, PieceType.Bishop);
            Assert.That(this.Game.Chessboard[a1].Type, Is.EqualTo(PieceType.Pawn));
            this.Game.MovementHistory.ExecuteCommand(blackPawnPromotion);
            Assert.That(this.Game.Chessboard[a1].Type, Is.EqualTo(PieceType.Bishop));
        }

        [Test]
        [ExpectedException(typeof(IncorrectCommandException))]
        public void IncorrectCommandExceptionOnTransformNonPawnPieces()
        {
            this.Game.Chessboard.SetPiece(new Piece(PieceType.Bishop, PieceColor.White), new ChessboardCoordinate("a8"));
            ICommand incorrectWhitePawnPromotion = new Promotion(this.Game, new ChessboardCoordinate("a8"), PieceType.Queen);
            this.Game.MovementHistory.ExecuteCommand(incorrectWhitePawnPromotion);
        }

        [Test]
        [ExpectedException(typeof(IncorrectCommandException))]
        public void IncorrectCommandExceptionOnTransformPawnOnIncorrectRank()
        {
            this.Game.Chessboard.SetPiece(new Piece(PieceType.Pawn, PieceColor.White), new ChessboardCoordinate("a4"));
            ICommand incorrectWhitePawnPromotion = new Promotion(this.Game, new ChessboardCoordinate("a4"), PieceType.Queen);
            this.Game.MovementHistory.ExecuteCommand(incorrectWhitePawnPromotion);
        }

        [Test]
        [ExpectedException(typeof(IncorrectCommandException))]
        public void IncorrectCommandExceptionOnTransformPawnIntoKing()
        {
            this.Game.Chessboard.SetPiece(new Piece(PieceType.Pawn, PieceColor.White), new ChessboardCoordinate("a8"));
            ICommand incorrectWhitePawnPromotion = new Promotion(this.Game, new ChessboardCoordinate("a8"), PieceType.King);
            this.Game.MovementHistory.ExecuteCommand(incorrectWhitePawnPromotion);
        }

        [Test]
        public void TransformPawnIntoTheSameColorPiece()
        {
            ChessboardCoordinate a8 = new ChessboardCoordinate("a8");
            this.Game.Chessboard.SetPiece(new Piece(PieceType.Pawn, PieceColor.White), a8);
            ICommand whitePawnPromotion = new Promotion(this.Game, a8, PieceType.Queen);
            this.Game.MovementHistory.ExecuteCommand(whitePawnPromotion);
            Assert.That(this.Game.Chessboard[a8].Color, Is.EqualTo(PieceColor.White));
        }
    }
}
