﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ChessboardGamesCore;
using ChessboardGamesCore.Exceptions;
using ChessboardGamesCore.Commands.Chess;

namespace ChessboardGamesCoreNUnit.Chess.Commands
{
    [TestFixture]
    class ChessMoveTests
    {
        [SetUp]
        public void SetUp()
        {
            this.Game = new ChessGame();
        }

        private ChessGame Game;

        [Test]
        public void MovePieceOnExecute()
        {
            Piece whitePawn = new Piece(PieceType.Pawn, PieceColor.White);
            ChessboardCoordinate b2 = new ChessboardCoordinate("b2");
            ChessboardCoordinate b3 = new ChessboardCoordinate("b3");
            Move move = new Move(this.Game, b2, b3);

            Assert.That(this.Game.Chessboard[b2], Is.EqualTo(whitePawn));
            this.Game.MovementHistory.ExecuteCommand(move);
            Assert.That(this.Game.Chessboard[b2], Is.Not.EqualTo(whitePawn));
            Assert.That(this.Game.Chessboard[b3], Is.EqualTo(whitePawn));
        }

        [Test]
        public void UpdateEnPassantSquareCoordinateOnExecute()
        {
            ChessboardCoordinate b2 = new ChessboardCoordinate("b2");
            ChessboardCoordinate b4 = new ChessboardCoordinate("b4");
            Move b2b4move = new Move(this.Game, b2, b4);

            this.Game.MovementHistory.ExecuteCommand(b2b4move);
            Assert.That(this.Game.EnPassantCoordinate, Is.EqualTo(new ChessboardCoordinate("b3")));

            ChessboardCoordinate b7 = new ChessboardCoordinate("b7");
            ChessboardCoordinate b5 = new ChessboardCoordinate("b5");
            Move b7b5move = new Move(this.Game, b7, b5);

            this.Game.MovementHistory.ExecuteCommand(b7b5move);
            Assert.That(this.Game.EnPassantCoordinate, Is.EqualTo(new ChessboardCoordinate("b6")));

            ChessboardCoordinate a2 = new ChessboardCoordinate("a2");
            ChessboardCoordinate a3 = new ChessboardCoordinate("a3");
            Move a2a3move = new Move(this.Game, a2, a3);
            this.Game.MovementHistory.ExecuteCommand(a2a3move);
            Assert.That(this.Game.EnPassantCoordinate, Is.EqualTo(null));
        }

        [Test]
        public void MovePieceBackOnUnExecute()
        {
            Piece whitePawn = new Piece(PieceType.Pawn, PieceColor.White);
            ChessboardCoordinate b2 = new ChessboardCoordinate("b2");
            ChessboardCoordinate b3 = new ChessboardCoordinate("b3");
            Move move = new Move(this.Game, b2, b3);

            this.Game.MovementHistory.ExecuteCommand(move);
            this.Game.MovementHistory.Undo();
            Assert.That(this.Game.Chessboard[b3], Is.EqualTo(null));
            Assert.That(this.Game.Chessboard[b2], Is.EqualTo(whitePawn));
        }

        [Test]
        public void RestoreEnPassantSquareCoordinateOnUnExecute()
        {
            ChessboardCoordinate b2 = new ChessboardCoordinate("b2");
            ChessboardCoordinate b4 = new ChessboardCoordinate("b4");
            Move b2b4move = new Move(this.Game, b2, b4);

            Assert.That(this.Game.EnPassantCoordinate, Is.EqualTo(null));
            this.Game.MovementHistory.ExecuteCommand(b2b4move);
            Assert.That(this.Game.EnPassantCoordinate, Is.EqualTo(new ChessboardCoordinate("b3")));
            this.Game.MovementHistory.Undo();
            Assert.That(this.Game.EnPassantCoordinate, Is.EqualTo(null));
        }

        [Test]
        [ExpectedException(typeof(IncorrectCommandException))]
        public void ExceptionOnIncorrectMove()
        {
            ChessboardCoordinate b2 = new ChessboardCoordinate("b2");
            ChessboardCoordinate b5 = new ChessboardCoordinate("b5");
            Move b2b5move = new Move(this.Game, b2, b5);

            this.Game.MovementHistory.ExecuteCommand(b2b5move);
        }

        [Test]
        public void UpdatePieceMovementHistoryOnExecute()
        {
            ChessboardCoordinate b2 = new ChessboardCoordinate("b2");
            int pieceMoveCount = this.Game.Chessboard[b2].MovementHistory.Count;
            ChessboardCoordinate b3 = new ChessboardCoordinate("b3");

            Move b2b3move = new Move(this.Game, b2, b3);
            this.Game.MovementHistory.ExecuteCommand(b2b3move);
            Assert.That(this.Game.Chessboard[b3].MovementHistory.Count, Is.EqualTo(pieceMoveCount + 1));
            Assert.That(this.Game.Chessboard[b3]
                .MovementHistory[this.Game.Chessboard[b3].MovementHistory.Count - 1], Is.EqualTo(b3));
        }

        [Test]
        public void UpdatePieceMovementHistoryOnUnExecute()
        {
            ChessboardCoordinate b2 = new ChessboardCoordinate("b2");
            int pieceMoveCount = this.Game.Chessboard[b2].MovementHistory.Count;
            ChessboardCoordinate b3 = new ChessboardCoordinate("b3");

            Move b2b3move = new Move(this.Game, b2, b3);
            this.Game.MovementHistory.ExecuteCommand(b2b3move);
            this.Game.MovementHistory.Undo();
            Assert.That(this.Game.Chessboard[b2].MovementHistory.Count, Is.EqualTo(pieceMoveCount));
        }

        [Test]
        public void WhiteMoveTheFirst()
        {
            bool isMoved = false;

            try
            {
                this.Game.MovementHistory.ExecuteCommand(
                    new Move(
                        this.Game, new ChessboardCoordinate("a2"),
                        new ChessboardCoordinate("a3")
                    ));
                isMoved = true;
            }
            catch
            {
                isMoved = false;
            }

            Assert.That(isMoved, Is.True);
        }

        [Test]
        public void BlackCantMoveTheFirst()
        {
            bool isMoved = false;

            try
            {
                this.Game.MovementHistory.ExecuteCommand(
                    new Move(
                        this.Game, new ChessboardCoordinate("b7"),
                        new ChessboardCoordinate("b6")
                    ));
                isMoved = true;
            }
            catch
            {
                isMoved = false;
            }

            Assert.That(isMoved, Is.False);
        }

        [Test]
        public void MovesAlternation()
        {
            // White move
            this.Game.MovementHistory.ExecuteCommand(
                    new Move(
                        this.Game, new ChessboardCoordinate("a2"),
                        new ChessboardCoordinate("a3")
                    ));

            // White can't move twice.
            bool isMoved = false;
            try
            {
                this.Game.MovementHistory.ExecuteCommand(
                    new Move(
                        this.Game, new ChessboardCoordinate("b2"),
                        new ChessboardCoordinate("b3")
                    ));
                isMoved = true;
            }
            catch
            {
                isMoved = false;
            }
            Assert.That(isMoved, Is.False);

            // Black move
            this.Game.MovementHistory.ExecuteCommand(
                    new Move(
                        this.Game, new ChessboardCoordinate("b7"),
                        new ChessboardCoordinate("b6")
                    ));

            // Black can't move twice.
            isMoved = false;
            try
            {
                this.Game.MovementHistory.ExecuteCommand(
                    new Move(
                        this.Game, new ChessboardCoordinate("a7"),
                        new ChessboardCoordinate("a6")
                    ));
                isMoved = true;
            }
            catch
            {
                isMoved = false;
            }
            Assert.That(isMoved, Is.False);
        }
    }
}
